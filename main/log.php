<?php
class log
{
	public $db_method;

	function __construct(){
		$this->db_method=new db_method();
	}

	public function get_data(){
		$data=array();
		if(!isset($_SESSION))session_start();
		if(isset($_SESSION['id_user'])){
			$user=$this->db_method->db_select('user',array('user_id'=>$_SESSION['id_user']),'user_photo,user_name,user_surname');
			if($user!==false){
				foreach($user[0] as $key=>$value){
					if($key=='user_photo' AND !empty($value)){
						$data['photo']="/userphoto/".$_SESSION['id_user']."/$value";
					}
					if(($key=='user_name' OR $key=='user_surname') AND !empty($value)){
						@$data['main_user_name'].=" $value";
					}
				}
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
			$status_news=$this->db_method->db_select('user_status',array('user_id'=>$_SESSION['id_user']),'status_news');
			if($status_news!==false){
				$messadge=$this->db_method->db_news($_SESSION['id_user'],$status_news,1);
				if($messadge!==false){
					if(!empty($messadge))$data['count_news']=$messadge;
				}
				else{
					$data['messadge']=$this->db_method->db_error();
				}
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
			$messadge=$this->db_method->db_select('note',array('user_in_id'=>$_SESSION['id_user'],'note_status'=>'0'),'note_id');
			if($messadge!==false){
				if(!empty($messadge))$data['count_note']=count($messadge);
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
			$messadge=$this->db_method->db_select('messadge',array('user_in_id'=>$_SESSION['id_user'],'mes_status'=>'0'),'mes_id');
			if($messadge!==false){
				if(!empty($messadge))$data['count_mes']=count($messadge);
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
			$rank=$this->db_method->db_select('user_rank',array('user_id'=>$_SESSION['id_user']),'user_rank,user_rank_point');
			if($rank!==false){
				if(!empty($rank)){
					foreach($rank[0] as $key=>$value){
						if($key=='user_rank_point') $data['user_rank_point']=$value;
						if($key=='user_rank'){
							$data['user_rank']=$value;
							$data['user_power']=$this->db_method->db_select('levels',array('levels'=>$value),'levels_power');
						}
					}
				}
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}			
			$data['log']='<li><a href="/" style="position:relative"><div class="title" data-title="Сфера"><img src="../images/Сфера.PNG" class="main_nav_photo"></div></a></li>'.
							'<li id="go_to_room" style="position:relative"><a><div class="title" data-title="Дом"><img src="../images/Дом.PNG" class="main_nav_photo"></div></a></li>'.
							'<li style="margin-top: 3px"><a href="/logout.html">Выход</a></li>';
		}
		else{
			$data['log']="<div id='reg' class='reg_log'><div class='title4' data-title='Регистрация'><a href='/registration.html'>sign up</a></div></div>
						<div id='login' class='reg_log'><div class='title4' data-title='Вход'><a href='/login.html'>log in</a></div></div>";
 
		}
		return $data;
	}
}
