<?php
/**
 * The template for user progress
 *
 *
 */



?>

<div class="reg_body4 container_2">
<div class="reg_body7 container cont">
	<div class="row news_block2">
		<legend>Достижения</legend>
		<div class="span11">
			<? if(isset($data['messadge'])) echo $data['messadge']; ?>
			<table class="table">
				<thead>
					<tr>
						<th>Достижение</th>
						<th>Описание достижения</th>
						<th>Дата свершения</th>
						<th>Подтверждающий документ</th>
					</tr>
				</thead>
				<tbody> <!----tbody для обозначения содержимого таблици, если мешает можно удалить------>
					<? if(isset($data['table'])) echo $data['table']; ?>
				</tbody>
			</table>
		</div>
	</div>
 </div>
</div>
 <div class="ikosayder">

<!-------------------------------------------------ИКОСАЙДЕР---------------------------------------->
</div>
