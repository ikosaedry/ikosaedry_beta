<? if(!isset($close_rofile_id)) {$close_rofile_id = '';}?>
<script type='text/javascript' src='/js/sh_personal.js'></script>
<div class="reg_body5 cont container_2" id="user_profile_view">
	<div class="row news_block2 margin_l main_user_info">
        <?if(isset($data['messadge'])) echo $data['messadge'];?>
		<div class="span6">
			<div class="span6" style="margin-bottom: 20px !important; margin-top: 20px !important;">
				<div class="span3" style='margin-right: 20px; margin-left:0px'>Профиль пользователя</div>
				<div class="span3" style="margin: 0px"><a href="/" style='position: relative'><div class="title5" data-title="Закрыть"><img <?=$close_rofile_id?> src='../images/Закрыть.png' style='height: 15px;' class='pull-right'></div></a></div>
			</div>
		</div>
		<div class="span6">
			<div class="span6">
				<div class="photo">
					<img src="<?if(isset($data['user_photo'])) echo $data['user_photo']; else echo "/images/no_photo.png"; ?>"/>
				</div>
			</div>
			<div class="span6" style='margin-top: 10px; margin-left:0px'>
			<?if($data['level']>=0){?>
				<div class="span3" style="color: grey; position: relative;">
					<div class="tick0"></div>				
					<div class="tick1"></div>
					<div class="tick2"></div>
					<div class="tick3"></div>
					<div class="tick4"></div>
					<div class="tick5"></div>
					<div class="tick6"></div>
					<div class="tick7"></div>
					<div class="tick8"></div>
					<div class="tick9"></div>
					<div class="tick10"></div>
					<div class="tick11"></div>
					<div class="tick12"></div>
					<div class="tick13"></div>
					<div class="tick14"></div>
					<div class="tick15"></div>
					<div class="tick16"></div>
					<div class="tick17"></div>
					<div class="tick18"></div>
					<div class="tick19"></div>
					<div class="tick20"></div>
					<input type=range min=-10 max=10 value=<?if(isset($data['vote'])) echo $data['vote']; else echo 0;?> step=1 list=volsettings class="vote">
						<datalist id=volsettings>
							<option>-10</option>
							<option>-9</option>
							<option>-8</option>
							<option>-7</option>
							<option>-6</option>
							<option>-5</option>
							<option>-4</option>
							<option>-3</option>
							<option>-2</option>
							<option>-1</option>
							<option>0</option>
							<option>1</option>	
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>										
						</datalist>
					<div class="pull-left" style="width: 20px">-</div>
					<div class="pull-left" style="text-align: center; width: 170px">0</div>
					<div class="pull-right"style="width: 20px; text-align: right;">+</div>
				</div>	
			<?}?>
			</div>
			<div class="span6">
				<div class="lg_inpt pull-right">
					<div class="friend" style="display: inline" data="<?if(isset($data['friends_value'])) echo $data['friends_value']; else echo '3';?>">
						<?if(isset($data['friends'])) echo $data['friends']; else echo "<div style='float: right; position: relative'><div class='title3' data-title='Дружить' style='float: right'><img src='../images/Добавить.PNG' style='height: 25px;margin-left: 10px; '></div></div>"; ?>
					</div>
					<?if(!isset($data['friends_value']) OR $data['friends_value']<2){?>
						<div class="sub" style="display: inline" data="<?if(isset($data['sub_value'])) echo $data['sub_value']; else echo '0';?>">
							<?if(isset($data['sub_value'])) echo "<div style='float: right; position: relative'><div class='title3' data-title='Отписаться' style='float: right'><img src='../images/Отписаться.png' style='height: 25px;margin-left: 10px;'></div></div>"; else echo "<div style='float: right; position: relative'><div class='title3' data-title='Подписаться'><img src='../images/Подписаться.png' style='height: 25px;margin-left: 10px;'></div></div>"; ?>
						</div>
					<?}?>
					<div class="send_messadge pull-right" style='margin-left:15px; position:relative'><div class="title3" data-title="Написать сообщение" style='float: right'><img src='../images/Сообщение.PNG' style='height: 25px;margin-left: 10px;' class="pull-right open_messege_block"></div></div>
				</div>
			</div>
		</div>
		<div class="span6 messege_personal_block">
			<form method="post">
				<textarea class='messege_personal' name='message'></textarea>
				<button class='messege_personal_btn pull-right' name='send'>Отправить</button>
			</form>
		</div>
		<div class="span6">
			<div class="span2 lg_inpt">имя</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_name'])) echo $data['user_name'];?></div><br>
			<div class="span2 lg_inpt">фамилия</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_surname'])) echo $data['user_surname'];?></div><br>
			<div class="span2 lg_inpt">пол</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_sex'])) echo $data['user_sex'];?></div><br>
			<div class="span2 lg_inpt">страна</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_country'])) echo $data['user_country'];?></div><br>
			<div class="span2 lg_inpt">город</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_sity'])) echo $data['user_sity'];?></div><br>
			<div class="span2 lg_inpt">адрес</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_adres'])) echo $data['user_adres'];?></div><br>
			<div class="span2 lg_inpt">профессия</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_profession'])) echo $data['user_profession'];?></div><br>
			<div class="span2 lg_inpt">телефон</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_tel_number'])) echo $data['user_tel_number'];?></div><br>
			<div class="span2 lg_inpt">дата рождения</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_birthday'])) echo $data['user_birthday'];?></div><br>
			<div class="span2 lg_inpt">email</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_e_mail'])) echo $data['user_e_mail'];?></div><br>
			<div class="span2 lg_inpt">skype</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_skype'])) echo $data['user_skype'];?></div><br>
			<div class="span2 lg_inpt">viber</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_viber'])) echo $data['user_viber'];?></div><br>
			<div class="span2 lg_inpt">whatsapp</div>
			<div class="span3 lg_inpt about_user"><?if(isset($data['user_votsap'])) echo $data['user_votsap'];?></div><br>
		</div>
	</div>
</div>
