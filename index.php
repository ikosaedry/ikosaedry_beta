<?php
//header("Cache-control: public");
//header("Expires: " . gmdate("D, d M Y H:i:s", time() + 60*10) . " GMT");
// подключаем файлы ядра
require_once $_SERVER['DOCUMENT_ROOT'].'/conf/defines.php';
require_once BASE_PATH.'core/lib.php';
require_once BASE_PATH.'core/model.php';
require_once BASE_PATH.'core/controller.php';
require_once BASE_PATH.'core/route.php';

if(isset($_POST['request'])){
	new ajax();
}
else{
// запускаем маршрутизатор
Route::start();
}
