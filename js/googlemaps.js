var searchEnable = true;

var map, infoWindow;
var lat,logn;
var markers = [];

var lastQ = false;
var showMap = false;

$(document).ready(function(){
    $('body').on('click','#close_map',function(){
        $("#map_canvas").attr('style','display:none;');
    });

    $('#search_input').click(function(){
        if(!showMap) {
            showMap = true;
            $('#map_canvas').attr('style', 'height:' + (window.innerHeight - 70) + 'px;opacity:0;');
            $('#map_canvas').animate({opacity: 1}, 300);
            if (!map) {
                initMap();
            }
        }else{
            $("#map_canvas").removeAttr('style');
        }
    });

    $('#search_input').keyup(function(e){
        if((e.keyCode == 13)&&(searchEnable)&&($(this).val()!='')&&(lastQ!=$(this).val())) {
            lastQ = $(this).val();
            $('#map_canvas').append('<div class="loadingMapInfo"><span>Загрузка ...</span></div>');
            $.ajax({
                type: "POST",
                url: '/ajax/map_info.php',
                data: 'lat='+lat+'&long='+long+'&q='+lastQ,
                dataType: "json",
                success: function (json) {
                    $('#map_canvas .loadingMapInfo').remove();
                    deleteMarkers();
                    json.results.forEach(function(item){
                        createMarker(item);
                    });
                },
                error: function() {
                    $('#map_canvas .loadingMapInfo span').html('Ошибка получения данных');
                    setTimeout(function(){$('#map_canvas .loadingMapInfo').remove();},3000);
                }
            });
        }
    });
});
function initMap() {
    var myLatlng = new google.maps.LatLng(43.2223861,76.9386329);
    var myOptions = {
        zoom: 10,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    infoWindow = new google.maps.InfoWindow({map: map});

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            lat = position.coords.latitude;
            long = position.coords.longitude;

            infoWindow.setPosition(pos);
            infoWindow.setContent('Lat: '+lat.toFixed(3)+', Long: '+long.toFixed(3));
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
    $('#map_canvas').append('<span id="close_map"></span>');
}
function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });
    markers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(place.name);
        infoWindow.open(map, this);
    });
}
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
function clearMarkers() {
    setMapOnAll(null);
}
function deleteMarkers() {
    clearMarkers();
    markers = [];
}