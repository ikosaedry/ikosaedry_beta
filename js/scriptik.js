	$(function(){
				$( ".datepicker" ).datepicker({			//датипикер
					  changeMonth: true,
					  changeYear: true,
					  yearRange: "1935:2025"
					});

				$('.clon_btn').click(function(){ //работа кнопки Добавить
					$(".cont_for_clone").find("input.datepicker").each(function ()
						{
						$(this).datepicker('destroy').removeAttr('id') //удаляем датапикер
						});

					var clone = $(".cont_for_clone:first");
					clone.clone(true).appendTo(".form");

					$(".cont_for_clone").removeClass("cont_for_clone").addClass("cont_remuve");		
					$(".form").find(':first-child').addClass("cont_for_clone").removeClass("cont_remuve");
					$(".form").find(".cont_remuve span.remuve_bnt").show();

					$("input.datepicker").each(function () //запускаем датапикер снова
						{
							$(this).datepicker({
							  changeMonth: true,
							  changeYear: true,
							  yearRange: "1935:2025"});
							});
					return false;
				});
				
				$("span.remuve_bnt").click(function(){
					$(this).parents(".cont_remuve").remove();
				})
				
				$("#country").change(function() { //меняет подсказку для ввода номера при выборе страны
					var phoneNum = $(".phoneNum");
					if(($( this ).val()) == "uk")
						{
						phoneNum.attr('placeholder','+38 (000) 000-00-00');
						}
					else if(($( this ).val()) == "rf")
						{
						phoneNum.attr('placeholder','+7 000 000 00 00');
						}
					else if(($( this ).val()) == "kz")
						{
						phoneNum.attr('placeholder','+8 000 00 00000');
						}
					else if(($( this ).val()) == "usa")
						{
						phoneNum.attr('placeholder','1-000-000-0000 ');
						}
				});
	});
