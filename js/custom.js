var player;
var mdown = false;
var showConnectionsAct = false;
var mainElPosY =  minY =  maxY = onePers = currentPos = false;
var myId = -1;

$(document).ready(function(){

    $('.main_scroll').attr('style','display: none;');

    $('body').on('click','#close_profile',function(){
        resetLocation();
        if ($('#user_profile_view')) {
            $('#user_profile_view').remove();
            player.controlSet(true);
        }
    });

    $('#ModalWindow, #dialog, .container, .container_2, #map_canvas, .main_search').hover(function(){
        if(player){player.controlSet(false);}
    });


    $('#ModalWindow, #dialog, .container, .container_2, #map_canvas, .main_search').mouseleave(function(){
        if(player){player.controlSet(true);}
        mdown = false;
    });

    $('#zoom_control').mousedown(function(){
        mdown = true;
    });

    $('#zoom_control').mouseup(function(){
        mdown = false;
    });

    $('#zoom_control').mouseleave(function(){
        mdown = false;
    });

    $('body').on('mouseover','#user_profile_view',function(){
        if(player){player.controlSet(false);}
    });

    $('body').on('mouseleave','#user_profile_view',function(){
        if(player){player.controlSet(true);}
    });

    $('#go_to_room').click(function() {
        $('.messager').parent().remove();
        player.goToRoom();
    });

    $('#showConnections').click(function(){
        if(showConnectionsAct){
            showConnectionsAct = false;
            player.showConnections(showConnectionsAct);
            $(this).attr('style','opacity: 1 !important;');
        }else{
            showConnectionsAct = true;
            player.showConnections(showConnectionsAct);
            $(this).removeAttr('style');
        }

    });
    $('ul li .form-control').change(function () {
        var id = $(this).val();
        if((id!='')&&(id>-1)) {
            player.selectProf(id);
        }
    });
    $(window).resize(function(){
        if($('#user_profile_view')) {
            var w = window.innerWidth - 40;
            $('#user_profile_view').attr('style', 'width:' + w + 'px !important;');
        }
        if((document.location.href.indexOf('.html')==-1)&&(document.location.href.indexOf('.php')==-1)){updateScroolParams();}
    });
    $('body').on('click','#back',function(){
        if($('#user_profile_view')) {
            $('#user_profile_view').attr('style', 'display: none !important;');
            if(player){player.controlSet(true);}
        }
    });

    $.ajax({
        type: "POST",
        url: "/ajax/myid.php",
        success: function (html) {
            if(html!=-1){
                if(($('#zoom_control')&&((document.location.href.indexOf('.html')==-1)&&(document.location.href.indexOf('.php')==-1)))) {
                    $('#zoom_control').attr('style','height:'+(window.innerHeight-100)+'px;');

                    mainElPosY = $('#zoom_control').offset().top;

                    minY = -($('#zoom_point').height() / 2).toFixed(8) + 3;
                    maxY = ($('#maxy').offset().top - $('#miny').offset().top) - ($('#zoom_point').height() / 2).toFixed(8) + 3;
                    $('#zoom_point').attr('style', 'top:' + maxY + 'px;');
                    onePers = (minY - maxY) / 100;
                    currentPos = 0;

                    $('#zoom_point, #zoom_bg').mousedown(function () {
                        mdown = true;
                    });
                    $('#zoom_point, #zoom_bg').mouseup(function () {
                        mdown = false;
                    });

                    $('#zoom_bg, #zoom_point').mousemove(function (e) {
                        if (mdown == true) {
                            var newY = +e.clientY - mainElPosY - (+$('#zoom_point').height()/2) ;
                            var pers = (minY - newY) / onePers;
                            if (pers < 0) {
                                pers = 0;
                            }
                            currentPos = (100 - pers).toFixed(1);
                            if(currentPos<0){currentPos = 0;}
                            if ((newY >= minY) && (newY <= maxY)) {
                                $('#zoom_point').attr('style', 'top:' + newY + 'px;');
                                player.updateDistance(currentPos);
                            }
                        }
                    });
                } else {
                    $('#zoom_control').attr('style','display: none !important;');
                }
            }
        }
    });


    var updateScroolParams = function () {
        $('#zoom_control').attr('style','height:'+(window.innerHeight-100)+'px;');
        minY = -($('#zoom_point').height() / 2).toFixed(8) + 3;
        maxY = ($('#maxy').offset().top - $('#miny').offset().top) - ($('#zoom_point').height() / 2).toFixed(8) + 3;
        onePers = (minY - maxY) / 100;
        setScroolPos(null,true);
    }
});

var setScroolPos = function(pos,reload){
    reload = reload || false;
    pos = pos || currentPos;
    if(((currentPos!==false)&&(currentPos!=pos))||(reload == true)) {
        currentPos = pos;
        var newY = maxY - (maxY - minY) / 100 * pos;
        $('#zoom_point').attr('style', 'top:' + newY + 'px;');
    }
}

