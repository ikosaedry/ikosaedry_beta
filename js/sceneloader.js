var scripts = [];
scripts.push("/js/app.js");
scripts.push("/js/trackballcontrols.js");
scripts.push("/js/fresnelshader.js");
scripts.push("/js/controltexts.js");
scripts.push("/js/custom.js");
scripts.push("/js/pointerlockontros.js");
scripts.push("/js/skyshader.js");
scripts.push("/js/colladaloader.js");
scripts.push("/js/mirror.js");
scripts.push("/js/watershader.js");
scripts.push("/js/ddsloader.js");
scripts.push("/js/mtlloader.js");
scripts.push("/js/objmtlloader.js");
scripts.push("/js/updatelocation.js");
scripts.push("/js/googlemaps.js");

$(window).load(function() {
    $('#sceneloading').html('<img style="position:relative;left: 50%; top: 50%;margin-top: -25px;margin-left: -25px;" src="/images/loading.gif" width="50px" />');
    checkTHREE();
});

$(document).ready(function() {
    $('#zoom_control').attr('style','display:none !important;');;
    /*
    $('#sceneloading').click(function () {
        initScene();
        $(this).html('<img style="position:relative;left: 50%; top: 50%;margin-top: -25px;margin-left: -25px;" src="/images/loading.gif" width="50px" />');

    });
    */
});

function checkTHREE(){
    if (THREE) {
        loading();
    } else {
        setTimeout(function(){checkTHREE();},500);
    }
}

var loading = function(n) {
    n = n || 0;
    if((scripts[n])&&(scripts[n]!='')) {
        var script = document.createElement("script")
        script.type = "text/javascript";
        script.src = scripts[n];
        $('body').append(script);
        setTimeout(function(){loading(n+1);},200);
    } else {
        //$('#go_to_room').attr('style','display:none;');
        setTimeout(function(){$('#sceneloading').html('');},1000);
        initScene();

        /*
        $.ajax({
            type: "POST",
            url: "/ajax/myid.php",
            success: function (html) {
                myId = html;
                if(html==-1){
                    $('#zoom_control').html('');
                    initScene();
                    $('#sceneloading').html('<img style="position:relative;left: 50%; top: 50%;margin-top: -25px;margin-left: -25px;" src="/images/loading.gif" width="50px" />');
                }
            }
        });
        */
    }
}

var initScene = function() {
    var loader = new THREE.XHRLoader();
    loader.load( '/js/app.json', function ( text ) {
        player = new APP.Player();
        player.load( JSON.parse( text ) );
        player.setSize( window.innerWidth, window.innerHeight );
        player.play();
        document.body.appendChild( player.dom );
        window.addEventListener( 'resize', function () {
            player.setSize( window.innerWidth, window.innerHeight );
        } );
        $('#sceneloading').remove();
        //$('#go_to_room').attr('style','display:block;');
    } );
}