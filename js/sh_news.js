function news(){
	$.ajax({
		type: "POST",
		data: {request:'update_news'},
		url: "index.php",
		dataType: "json",
		success: function (data) {
			if(data!=null){
				if(data.comment){
					var id = [];
					var body = [];
					$.each(data.comment, function(index,value){
						$.each(value, function(index,value){
							if(index=='id'){
								id.push(value);
							}
							if(index=='body'){
								body.push(value);
							}
						});
					});
					for(var i=0;i<id.length;i++){
						$('.news_id').each(function(){
							var news_id=$(this).closest('ul').attr('data');
							if(news_id==id[i]){
								var target=$(this);
								var count=$(target).find('.number_comment').text();
								count++;
								$(target).find('.number_comment').text(count);
								$(target).closest('.navbar').after(body[i]);
							}
						});
					}
				}
				if(data.like){
					var id = [];
					var body = [];
					var type = [];
					$.each(data.like, function(index,value){
						$.each(value,function(index,value){
							if(index=='id'){
								id.push(value);
							}
							if(index=='body'){
								body.push(value);
							}
							if(index=='type'){
								type.push(value);
							}							
						});
						for(var i=0;i<id.length;i++){
							$('.news_id').each(function(){
								var target =$(this);
								var news_id=$(target).closest('ul').attr('data');
								if(news_id==id[i]){
									if(type[i]==1){
										$(target).closest('ul').find('.number_likes').text(body[i]);
									}
									if(type[i]==2){
										$(target).closest('ul').find('.number_dislikes').text(body[i]);
									}
									$(target).closest('ul').find('.like').addClass('disabled');
									$(target).closest('ul').find('.dislike').addClass('disabled');
								}
							});
						}
					});
				}
				if(data.news){
					$('div.transparent_box').first().after(data.news);
				}
			}
		}
	});
}
//Обновление новостей
$(document).ready(function(){
	setTimeout(function run() {
		news();
		setTimeout(run, 20000);
	}, 20000);
//лайк новостей
	$('body').on('click','.like', function(){
		var target=$(this);
		var id=$(target).closest('.news_id').attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id,param2:'1',request:'like_news'},
			url: "index.php",
			success: function (html) {
				news();
			}
		});
	});
//дизлайк новостей
	$('body').on('click','.dislike', function(){
		var target=$(this);
		var id=$(target).closest('.news_id').attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id,param2:'2',request:'like_news'},
			url: "index.php",
			success: function (html) {
				news();
			}
		});
	});
//сохранение коментария
	$('body').on('click','.saveComment', function(){
		var target=$(this);
		var id=$(target).attr('data');
		var text=$(target).closest('.comment_box').find('.textarea_news').val();
		if(text){
			$.ajax({
				type: "POST",
				data: {save_comment: id, user_comment:text, request:'comment_news'},
				url: "index.php",
				success: function (html) {
					$(target).closest('.comment_box').find('.textarea_news').val('');
					news();
				}
			});
		}
	});
//прошаривание новости
	$('body').on('click','.share', function(){
		var target=$(this);
		var id=$(target).closest('.news_id').attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id, request:'share_news'},
			url: "index.php",
			success: function () {
				$(target).addClass('disabled');
				news();
			}
		});
	});
//переход в профиль пользователя
	$(document).on('click','.uid',function(){
		var id=$(this).attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id, request:'save_personal_id'},
			url: "index.php"
		});
    });
	$(document).on('click','.mini_user_photo2',function(){
		var id=$(this).attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id, request:'save_personal_id'},
			url: "index.php"
		});
    });
	$(document).on('click','.user_name_on_feed2',function(){
		var id=$(this).closest('.mini_user_photo2').attr('data');
		$.ajax({
			type: "POST",
			data: {param1: id, request:'save_personal_id'},
			url: "index.php"
		});
    });
//работа take in
	$(document).on('click','.takein',function(){
		var id=$(this).closest('.transparent_box').find('.news_id').attr('data');
		$(this).addClass('disabled');
		$.ajax({
			type: "POST",
			data: {param1: id, request:'take_in'},
			url: "index.php"
		});		
	});
});
