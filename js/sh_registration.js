$(document).ready(function(){
	$("input[name='user_pass2']").on('keyup',function(){
		var pass1=$("input[name='user_pass']").val();
		var pass2=$("input[name='user_pass2']").val();
		if(pass1!=pass2){
			$("input[name='user_pass2']").css('border', '1px solid #B22222');
		}
		else{
			$("input[name='user_pass']").css('border', '1px solid #00EE76');
			$("input[name='user_pass2']").css('border', '1px solid #00EE76');
		}
	});
	$("input[name='org_pass2']").on('keyup',function(){
		var pass1=$("input[name='org_pass']").val();
		var pass2=$("input[name='org_pass2']").val();
		if(pass1!=pass2){
			$("input[name='org_pass2']").css('border', '1px solid #B22222');
		}
		else{
			$("input[name='org_pass']").css('border', '1px solid #00EE76');
			$("input[name='org_pass2']").css('border', '1px solid #00EE76');
		}
	});
	$('body').on('change', '.selector',function(){
		var select=$(".selector :selected").val();
		if(select==1){
			$("#organisation").show();
			$("#user").css("display", "none");
		}
		else{
			$("#organisation").css("display", "none");
			$("#user").show();
		}
	});
});
