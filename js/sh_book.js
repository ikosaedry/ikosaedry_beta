//предварительная загрузка фото выбранной пользователем для последующей ее загрузки на сервер
$(document).ready(function(){
	$('body').on('change','.file', function(){
		var input=$(this);
		if (input[0].files && input[0].files[0]) {
			var reader = new FileReader();
			$(input).closest('div').find('.bg_input_file').append('<img id="image" src="#" alt="" class="book_cover"/>');
			reader.onload = function (e) {
				$(input).parent().find('#image').attr('src', e.target.result);
			};
			reader.readAsDataURL(input[0].files[0]);
		}
	});
	$('body').on('click','.v_close_btn', function(){
		var target=$(this);
		var id=$(target).closest('tr').attr('data');
		$.ajax({
			type: "POST",
			data: {datas: id, request:'delete_book'},
			url: "index.php",
			success: function () {
				$(target).closest('tr').empty();
			}
		});
	});
});