(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-68715039-1', 'auto');
ga('send', 'pageview');

/**
 * @author serhy@bk.ru
 */

var APP = {

    Player: function () {

        var scope = this;

        var loader = new THREE.ObjectLoader();
        var camera, scene, PControl = false, renderer, defMaterial = false, clock;

        var particleSystem, icosaedronsCount, line = false, inRoom = bookReadingMode = false, activeBook;

        var moveInRoom = false; roomPos = [], canLoadProfiles = true, velocity = new THREE.Vector3();

        var raycaster, prevTime, bgmesh;

        var wasRememberedE = customCursor = false;

        var currentUserId;

        var pos0 = [], pos1 = [];

        var exitRoom = false;

        var bookYDown = false, bookYUp = false, bookXUp = false, bookXDown = false, bookZUp = false, bookZDown = false;

        var TweenPosition = TweenTarget = false, tween, verticalMirror, groundMirror;

        var showConnectionsAct = selectionAnimated = false, allBooks = [], cubeCamera = false;

        var userBox, lastMovedElement, videoTexture, video, videoplay = false;

        var roomFurnitureMoving = false, canPlayVideo = false, bookMoving = false;

        var inRoomTemp = onceClick = false;

        var savedControlBody, currentshinedLevel = -1, shinedLevel = false, profileSelection = false;

        var icosaednrons = texts = [], textN = -1;

        var light, light_1, light_2, light_3;

        var connectionLines = false, usersConections = [], conectionIds = [];

        var spheresArray = [];

        var myBoxPos = [];

        var selectedMaterial;

        var proFaces = [];
        proFaces.a1 = [];
        proFaces.b1 = [];
        proFaces.c1 = [];

        var selectedProf = false;

        var colors = [0x00599e,0x9e5900,0x009e0f,0x009e86,0x55009e,0x9e0091];

        var myid = selectedUser = userRoomId = -1, users = [], countUsers = 0, spheres =[];

        var mybox = false;

        var moveForward = moveLeft = moveBackward = moveRight = false, ctrlDown = controlDialogShow = false;

        var vr, controls;

        var activeFurniture = activeFurnitureClone = false;

        var desk;

        var elements =[];

        var events = {};

        var insideSphere, gsphere;

        var retCoords;

        var currentDistance = 0, currentScroolPos = 0;

        var scene1Container = new THREE.Object3D();

        var scene2Container = false;

        var TextTurn = [], turnStart = turnLength = 0,turnStarted = false;

        var roomTextures = [];

        var professionsList = [];

        var tempPhotos = [], tempPhotosCount = -1, myBook;

        var facesToShow = [];

        this.dom = undefined;

        this.width = 500;
        this.height = 500;

        this.load = function ( json ) {

            vr = json.project.vr;

            renderer = new THREE.WebGLRenderer( { antialias: true } );
            renderer.setClearColor( 0x002035 );
            renderer.setPixelRatio( window.devicePixelRatio );
            this.dom = renderer.domElement;

            this.setScene( loader.parse( json.scene ) );
            camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1700 );
            //camera.position.z = 500;
            this.setCamera( camera );

            controls = new THREE.TrackballControls( camera );

            controls.rotateSpeed = 0.3;
            controls.zoomSpeed = 0.2;
            controls.panSpeed = 1.8;

            controls.noZoom = false;
            controls.noPan = false;

            controls.staticMoving = false;
            controls.dynamicDampingFactor = 0.07;

            controls.zoomSpeed = 0.01;

            controls.noPan = true;

            controls.maxDistance = 800;

            events = {
                keydown: [],
                keyup: [],
                mousedown: [],
                mouseup: [],
                mousemove: [],
                touchstart: [],
                touchend: [],
                touchmove: [],
                update: [],
                wheel: []
            };

            for ( var uuid in json.scripts ) {

                var object = scene.getObjectByProperty( 'uuid', uuid, true );

                var scripts = json.scripts[ uuid ];

                for ( var i = 0; i < scripts.length; i ++ ) {

                    var script = scripts[ i ];

                    var functions = ( new Function( 'player, scene, keydown, keyup, mousedown, mouseup, mousemove, touchstart, touchend, touchmove, update', script.source + '\nreturn { keydown: keydown, keyup: keyup, mousedown: mousedown, mouseup: mouseup, mousemove: mousemove, touchstart: touchstart, touchend: touchend, touchmove: touchmove, update: update };' ).bind( object ) )( this, scene );

                    for ( var name in functions ) {

                        if ( functions[ name ] === undefined ) continue;

                        if ( events[ name ] === undefined ) {

                            console.warn( 'APP.Player: event type not supported (', name, ')' );
                            continue;

                        }

                        events[ name ].push( functions[ name ].bind( object ) );

                    }

                }

            }

            scene.add(scene1Container);

            /*
            // Втутриние елементы (профили) сцены ______
            var particles = 15000;

            var geometry = new THREE.BufferGeometry();

            var positions = new Float32Array( particles );
            var colors = new Float32Array( particles );

            var color = new THREE.Color();

            var maxRadius = 1200, minRadius = 800;

            for ( var i = 0; i < positions.length; i += 3 ) {

                var randRadius = getRandomInt(minRadius,maxRadius);
                var pc = randomSpherePoint(0,0,0,randRadius);

                positions[ i ]     = pc[0];
                positions[ i + 1 ] = pc[1];
                positions[ i + 2 ] = pc[2];

                var vx = 189;
                var vy = 229;
                var vz = 255;

                color.setRGB( vx, vy, vz );

                colors[ i ]     = color.r;
                colors[ i + 1 ] = color.g;
                colors[ i + 2 ] = color.b;

            }

            geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
            geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );

            geometry.attributes.position.needsUpdate = true;
            geometry.attributes.color.needsUpdate = true;

            var material = new THREE.PointCloudMaterial( { size: 0.1, vertexColors: THREE.VertexColors,transparent: true,opacity: 0.2 } );

            particleSystem = new THREE.PointCloud( geometry, material );

            scene1Container.add( particleSystem );
            //___профили добавлены________________
            */


            //Фон

            //Pesok


            function randomSpherePoint(x0,y0,z0,radius){
                var u = Math.random();
                var v = Math.random();
                var theta = 2 * Math.PI * u;
                var phi = Math.acos(2 * v - 1);
                var x = x0 + (radius * Math.sin(phi) * Math.cos(theta));
                var y = y0 + (radius * Math.sin(phi) * Math.sin(theta));
                var z = z0 + (radius * Math.cos(phi));
                return [x,y,z];
            }

            //icosaedrons start

            var darkMaterial = new THREE.MeshBasicMaterial( { color: 0xd4eeff, transparent: true, opacity: 0.2 } );
            var wireframeMaterial = new THREE.MeshBasicMaterial( { color: 0x88d0ff, wireframe: true, transparent: true, opacity: 0} );
            var multiMaterial = [ darkMaterial, wireframeMaterial ];
            var wireframeMaterial1 = new THREE.MeshBasicMaterial( { color: 0x88d0ff, wireframe: true, transparent: true, opacity: 0.03} );

            var center = new THREE.Object3D();

            dynamicTexture = [];
            materials = [];


            var geometry = new THREE.IcosahedronGeometry( 100, 2 );
            var material = new THREE.MeshLambertMaterial( {color: 0x0061a1,transparent:true,opacity: 0.2, wireframe:true} );
            //var material = new THREE.MeshFaceMaterial( materials ) ;
            var outsideSphere = new THREE.Mesh( geometry, material );

            icosaednrons[10] = outsideSphere;
            icosaednrons[10].name = 'Level 0';
            center.add( icosaednrons[10] );
            /*
            var material = new THREE.MeshLambertMaterial( {color: 0xc9e9ff,transparent:true,opacity: 0.15, wireframe:true, emissive: 0xbee5ff} );
            var geometry = new THREE.IcosahedronGeometry( 99.8, 2 );
            insideSphere = new THREE.Mesh( geometry, material );
            center.add( insideSphere );
            */

            var glassGeometry = new THREE.IcosahedronGeometry( 99.9, 2 ); ///FlatShading
            var glassMaterial = new THREE.MeshPhongMaterial( {color: 0x00548b,transparent:true,opacity: 0.4, emissive: 0x003543, specular: 0xffffff, shininess: 100, shading: THREE.FlatShading, side: THREE.DoubleSide} );
            gsphere = new THREE.Mesh( glassGeometry, glassMaterial );
            center.add( gsphere );
            scene1Container.add(center);

            var n=10;
            i = 100;
            for (var z=10;z>0;z--) {
                i+=30;
                n--;
                var geom = new THREE.IcosahedronGeometry(i, 2);

                if((n!=0)&&(n!=7)) {
                    icosaednrons[n] = new THREE.Mesh( geom, wireframeMaterial );
                    icosaednrons[n].visible = false;
                } else {
                    icosaednrons[n] = new THREE.Mesh( geom, wireframeMaterial1 );
                }
                icosaednrons[n].name = 'Level '+n;
                icosaednrons[n].geometry.colorsNeedUpdate = true;
                scene1Container.add(icosaednrons[n]);
            }

            icosaedronsCount = n;

            for (i=0;i<icosaednrons.length;i++){
                spheresArray[i] = [];
                for (var z=0;z<icosaednrons[1].geometry.faces.length;z++) {
                    if(!spheresArray[z]) {spheresArray[z]=[];}
                    spheresArray[z][i] = false;
                }
            }
            //icosaedrons end
            /*
            if ((getCookie('backX')!='')&&(getCookie('backY')!='')&&(getCookie('backZ')!='')) {
                camera.position.x = getCookie('backX');
                camera.position.y = getCookie('backY');
                camera.position.z = getCookie('backZ');
                camera.lookAt(scene.position);
            }
            */

            var material = new THREE.MeshPhongMaterial( {color: 0xffffff} );

            var i, x = y = z = 57;
            var min = 57, max = 190;

            var geometry = new THREE.SphereGeometry( 1, 32, 32 );

            //Получение своего айди
            var material;
            var geometry = new THREE.BoxGeometry(0.8, 0.8, 0.8 );

            $.ajax({
                type: "POST",
                url: "/ajax/uinfo.php",
                data: "id=my",
                dataType: "json",
                success: function (json) {
                    if(json.id!=-1){
                        myid = json.id;
                        loadAllUsers(json.prof,json.level,myid);
                        /* Демонстрация комнаты */
                        if(myid==75) {
                            setTimeout(function () {
                                player.goToRoom();
                            },1000);
                        }
                        /*===================== */
                        scene1Container.add(spheres[myid]);
                    } else {
                        controls.enabled = false;
                        controls = false;
                        objRotate(scene1Container,60000);
                        professionsPresentation();
                    }
                    //Ajax получение информации о пользователе
                    if(myid!=-1) {
                        loadAllUsers();
                    }
                }
            });

            $.ajax({
                type: "POST",
                url: "/index.php",
                dataType: "json",
                data: {request:'prof_list'},
                success: function (json) {
                    $.each(json, function (i, v) {
                        professionsList[v.prof_id] = v.prof_name;
                    });
                }
            });

            $.ajax({
                type: "POST",
                url: "/ajax/randphoto.php?n="+getRandomInt(0,99999999),
                dataType: "json",
                success: function (json) {
                    $.each(json, function (i, v) {
                        tempPhotosCount++;
                        tempPhotos[tempPhotosCount] = '/temp_photos/'+v;
                    });
                }
            });

            createTextfromTurn();

            raycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 1, 0, 0 ), 0, 3 );

            light = new THREE.PointLight( 0xffffff, 1, 0 );
            light_1 = light.clone();
            light_2 = new THREE.PointLight( 0xffffff, 1, 30 );

            light_3 = new THREE.PointLight( 0xffffff, 1, 0 );

            scene1Container.add( light );
            scene1Container.add( light_1 );
            scene1Container.add( light_2 );
            scene1Container.add( light_3 );

            camera.position.x = 0;
            camera.position.y = 0;
            camera.position.z = -800;

            var x = camera.position.x,
                y = camera.position.y,
                z = camera.position.z;
            rotSpeed = 0.05;

            camera.position.x = x * Math.cos(rotSpeed) + z * Math.sin(rotSpeed);
            camera.position.z = z * Math.cos(rotSpeed) - x * Math.sin(rotSpeed);
            camera.lookAt(scene1Container.position);


        };

        var material;
        var geometry = new THREE.BoxGeometry(0.8, 0.8, 0.8 );

        //Функция загрузки всех пользователей
        function loadAllUsers(prof,level,uid,once,pos) {

            prof = prof || false;
            level = level || false;
            uid = uid || '';
            once = once || false;
            pos = pos || false;

            if(((!prof)||(!level))&&(uid=='')) {
                for (i=0;i<spheresArray.length;i++) {
                    for (z=0;z<spheresArray[i].length;z++) {
                        if(!spheresArray[i][z]) {
                            prof = i;
                            level = z;
                            break;
                        }
                    }
                    if((prof)&&(level)){
                        break;
                    }
                }
            }


            if(((prof!==false)&&(level!==false))||(uid!='')) {

                if(!spheresArray[prof][level]) { spheresArray[prof][level] = new THREE.Object3D(); }

                $.ajax({
                    type: "POST",
                    url: "/ajax/users.php",
                    data: 'act=userslist&prof=' + prof + '&level=' + level + '&uid='+uid,
                    dataType: "json",
                    success: function (json) {
                        var photo;
                        $.each(json, function (i, v) {
                            users.push(v);
                            if ((!mybox) || (myid != v.user_id)) {
                                if (v.user_id == myid) {
                                    var my_mat = new THREE.MeshPhongMaterial({color: 0xfff667});
                                    var my_geo = new THREE.BoxGeometry(0.5, 0.5, 0.5);
                                    spheres[v.user_id] = new THREE.Mesh(my_geo, my_mat);
                                } else {
                                    var material = new THREE.MeshPhongMaterial({color: 0xffffff});
                                    var geometry = new THREE.BoxGeometry(0.4, 0.4, 0.4);
                                    spheres[v.user_id] = new THREE.Mesh(geometry, material);
                                }

                                var profession = 302;
                                if ((v.user_profession) && (v.user_profession != '') && (v.user_profession > 0)) {
                                    profession = v.user_profession;
                                }

                                if (!v.user_rank) {
                                    v.user_rank = 0;
                                }

                                var pc;
                                if(pos == false) { pc=levelPoint(v.user_rank, profession); } else {
                                    pc = pos;
                                }

                                if (v.user_id == myid) {
                                    myBoxPos = pc;
                                    addConnectionLines(v.user_id);
                                }

                                spheres[v.user_id].position.x = pc[0];
                                spheres[v.user_id].position.y = pc[1];
                                spheres[v.user_id].position.z = pc[2];
                                spheres[v.user_id].name = 'Box ' + (v.user_id);
                                if (v.user_id != myid) {
                                    spheresArray[prof][level].add(spheres[v.user_id]);
                                } else {
                                    if (mybox == false) {
                                        mybox = spheres[v.user_id];
                                        scene1Container.add(spheres[v.user_id]);
                                    }
                                }
                                if (countUsers <= v.user_id) {
                                    countUsers = v.user_id + 1;
                                }
                                //TEXT
                                //addToTextTurn(v.user_name+' '+ v.user_second_name,pc[0],pc[1]-1,pc[2],scene1Container);
                            }
                        });

                        if(!once) {
                            setTimeout(function(){loadAllUsers();},50);
                        }else
                        if(once == 'pos'){
                                return true;
                        } else {
                            scene1Container.add(spheresArray[prof][level]);
                        }
                    },
                    error: function (msg) {
                        if(!once) {
                            if (msg.status == 200) {
                                setTimeout(function(){loadAllUsers();},50);
                            } else {
                                setTimeout(function(){loadAllUsers(prof, level);},50);
                            }
                        }
                    }
                });
            }
        }

        var getRandomInt = function(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        var addConnectionLines = function(id) {


            if(usersConections[id]) {
                if(connectionLines!=false) {
                    if(showConnectionsAct) {scene1Container.remove(connectionLines);}
                    connectionLines = false;
                }
                connectionLines = usersConections[id];
                if(showConnectionsAct) { scene1Container.add(connectionLines); }

            }else {
                $.ajax({
                    type: "POST",
                    url: "ajax/conections.php?id=" + id,
                    dataType: "json",
                    success: function (json) {

                        if (connectionLines != false) {
                            scene1Container.remove(connectionLines);
                            connectionLines = false;
                        }

                        connectionLines = new THREE.Object3D();

                        $.each(json, function (i, v) {
                            if(spheres[i]) {
                                myBoxPos[0] = spheres[i].position.x;
                                myBoxPos[1] = spheres[i].position.y;
                                myBoxPos[2] = spheres[i].position.z;

                                $.each(v, function (i1, v1) {
                                    if(spheres[i1]) {
                                        if (v1 == 3) {
                                            var color = 0xffffff;
                                        } else {
                                            var color = 0x0093d7;
                                        }
                                        color = 0xffffff;

                                        var geometry = new THREE.Geometry();
                                        var material = new THREE.LineBasicMaterial({
                                            color: color,
                                            transparent: true,
                                            opacity: 0.2,
                                            fog: true
                                        });


                                        geometry.vertices.push(
                                            new THREE.Vector3(myBoxPos[0], myBoxPos[1], myBoxPos[2]),
                                            new THREE.Vector3(spheres[i1].position.x, spheres[i1].position.y, spheres[i1].position.z)
                                        );

                                        connectionLines.add(new THREE.Line(geometry, material));
                                    }
                                });


                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "ajax/uinfo.php",
                                    data: "id=" + i,
                                    dataType: "json",
                                    success: function (json) {
                                        if(json) {
                                            myBoxPos = levelPoint(json.level, json.prof);
                                            loadAllUsers(json.prof, json.level, i, 'pos', myBoxPos);
                                            $.each(v, function (i1, v1) {
                                                if (spheres[i1]) {
                                                    if (v1 == 3) {
                                                        var color = 0xffffff;
                                                    } else {
                                                        var color = 0x0093d7;
                                                    }
                                                    color = 0xffffff;

                                                    var geometry = new THREE.Geometry();
                                                    var material = new THREE.LineBasicMaterial({
                                                        color: color,
                                                        transparent: true,
                                                        opacity: 0.2,
                                                        fog: true
                                                    });


                                                    geometry.vertices.push(
                                                        new THREE.Vector3(myBoxPos[0], myBoxPos[1], myBoxPos[2]),
                                                        new THREE.Vector3(spheres[i1].position.x, spheres[i1].position.y, spheres[i1].position.z)
                                                    );

                                                    connectionLines.add(new THREE.Line(geometry, material));
                                                }
                                            });
                                        }

                                    }
                                });

                            }

                        });

                        conectionIds[id] = json;

                        usersConections[id] = connectionLines;
                        if(showConnectionsAct){scene1Container.add(connectionLines);}

                        conectionIds[id] = json;

                        usersConections[id] = connectionLines;
                        if(showConnectionsAct){scene1Container.add(connectionLines);}

                    }
                });
            }
        }

        var friendConectionLines = function(id) {

            if(spheres[id]) {
                var friendLines = new THREE.Object3D();

                var friendPos = [];
                friendPos[0] = spheres[id].position.x;
                friendPos[1] = spheres[id].position.y;
                friendPos[2] = spheres[id].position.z;

                $.ajax({
                    type: "POST",
                    url: "ajax/conections.php?id=" + id,
                    dataType: "json",
                    success: function (json) {
                        var color = 0xececec;
                        var material = new THREE.LineBasicMaterial({
                            color: color, transparent: true, opacity: 0.15
                        });

                        $.each(json, function (i, v) {

                            if (spheres[i]) {
                                var sId = spheres[i].name.replace(/[^\d]/gi, '');


                                if (sId != myid) {

                                    var geometry = new THREE.Geometry();

                                    geometry.vertices.push(
                                        new THREE.Vector3(friendPos[0], friendPos[1], friendPos[2]),
                                        new THREE.Vector3(spheres[i].position.x, spheres[i].position.y, spheres[i].position.z)
                                    );
                                    friendLines.add(new THREE.Line(geometry, material));
                                }

                            } else {
                                var sId = '';
                            }

                        });

                    }

                });

                return friendLines;
            } else {
                return false;
            }

        }

        var createTextfromTurn = function () {
            if((!inRoom)&&(turnStart < turnLength)) {
                turnStart++;
                var options = {
                    font		: "arial",
                    size		: 0.4,
                    height		: 0,
                };

                var text = TextTurn[turnStart].text;

                var mesh = new THREE.Mesh(new THREE.TextGeometry(text, options), new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    side: THREE.DoubleSide
                }));

                mesh.position.x = TextTurn[turnStart].x;
                mesh.position.y = TextTurn[turnStart].y;
                mesh.position.z = TextTurn[turnStart].z;
                mesh.lookAt(camera.position);
                TextTurn[turnStart].e.add(mesh);

                TextTurn[turnStart] = false;

            }
            setTimeout(function(){createTextfromTurn();},50);
        }

        var addToTextTurn = function(text,x,y,z,e) {
            turnLength++;
            TextTurn[turnLength] = [];
            TextTurn[turnLength].text = text;
            TextTurn[turnLength].x = x;
            TextTurn[turnLength].y = y;
            TextTurn[turnLength].z = z;
            TextTurn[turnLength].e = e;

        }

        var getAtProffesions = function(faceIndex) {
            var a = icosaednrons[1].geometry.faces[faceIndex].a;
            var b = icosaednrons[1].geometry.faces[faceIndex].b;
            var c = icosaednrons[1].geometry.faces[faceIndex].c;
            //0 - 319
            facesToShow.my = faceIndex;

            var n = 0;

            icosaednrons[1].geometry.faces.forEach(function(item, v){
                if (
                    (item.a == a) ||(item.a == b) ||(item.a == c) ||
                    (item.b == a) ||(item.b == b) ||(item.b == c) ||
                    (item.c == a) ||(item.c == b) ||(item.c == c)
                ) {
                    n++;
                    facesToShow[n] = v;
                }
            });
        }


        var makeTextSprite = function( message, parameters )
        {
            if ( parameters === undefined ) parameters = {};

            var fontface = parameters.hasOwnProperty("fontface") ?
                parameters["fontface"] : "'Jura', sans-serif";

            var fontsize = parameters.hasOwnProperty("fontsize") ?
                parameters["fontsize"] : 18;

            var borderThickness = parameters.hasOwnProperty("borderThickness") ?
                parameters["borderThickness"] : 4;

            var borderColor = parameters.hasOwnProperty("borderColor") ?
                parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

            var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
                parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            context.font = fontsize + "px " + fontface;
            context.shadowBlur = true;
            context.shadowColor = "rgba(255, 255, 255, 1)";

            // get size data (height depends only on font size)
            var metrics = context.measureText( message );
            var textWidth = metrics.width;

            // background color
            context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
                + backgroundColor.b + "," + backgroundColor.a + ")";
            // border color
            context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
                + borderColor.b + "," + borderColor.a + ")";

            context.lineWidth = borderThickness;
            //roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
            // 1.4 is extra height factor for text below baseline: g,j,p,q.

            // text color
            context.fillStyle = "rgba(255, 255, 255, 1.0)";

            context.fillText( message, borderThickness, fontsize + borderThickness);

            // canvas contents will be used for a texture
            var texture = new THREE.Texture(canvas)
            texture.needsUpdate = true;

            var spriteMaterial = new THREE.SpriteMaterial(
                { map: texture, useScreenCoordinates: false } );
            var sprite = new THREE.Sprite( spriteMaterial );
            sprite.scale.set(100,50,1.0);
            return sprite;
        }

// function for drawing rounded rectangles
        var roundRect = function(ctx, x, y, w, h, r)
        {
            ctx.beginPath();
            ctx.moveTo(x+r, y);
            ctx.lineTo(x+w-r, y);
            ctx.quadraticCurveTo(x+w, y, x+w, y+r);
            ctx.lineTo(x+w, y+h-r);
            ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
            ctx.lineTo(x+r, y+h);
            ctx.quadraticCurveTo(x, y+h, x, y+h-r);
            ctx.lineTo(x, y+r);
            ctx.quadraticCurveTo(x, y, x+r, y);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
        }

        //Функция выделения луча соответствующей проффесии. Вход параметр - айди проффесии
        var selectProffesion = function(faceIndex,opacity) {
            selectedProf = faceIndex;
            opacity = opacity || false;
            if(opacity){opacity = 0;}else{opacity = 0.2;}
            //getAtProffesions(faceIndex);
            //Удаление существующего выделения
            if(line!=false) {
                scene1Container.remove(line);
                line = false;
            }
            if(!inRoom) {
                //Создание обекта выделения и его свойств
                line = new THREE.Object3D();
                var material = new THREE.LineDashedMaterial({
                    color: 0xffffff, transparent: true, opacity: opacity, fog: true
                });
                var geometry = new THREE.Geometry();

                //Получение координат вершин и добавление линий
                var wasGeted = false;
                var proLinePoints = [];
                for (var i = 0; i < icosaednrons.length; i++) {
                    var a = icosaednrons[i].geometry.faces[faceIndex].a;
                    var b = icosaednrons[i].geometry.faces[faceIndex].b;
                    var c = icosaednrons[i].geometry.faces[faceIndex].c;
                    var ax = icosaednrons[i].geometry.vertices[a].x;
                    var bx = icosaednrons[i].geometry.vertices[b].x;
                    var cx = icosaednrons[i].geometry.vertices[c].x;
                    var ay = icosaednrons[i].geometry.vertices[a].y;
                    var by = icosaednrons[i].geometry.vertices[b].y;
                    var cy = icosaednrons[i].geometry.vertices[c].y;
                    var az = icosaednrons[i].geometry.vertices[a].z;
                    var bz = icosaednrons[i].geometry.vertices[b].z;
                    var cz = icosaednrons[i].geometry.vertices[c].z;

                    if(!wasGeted) {
                        wasGeted = true;
                        var ax1 = ax;
                        var ay1 = ay;
                        var az1 = az;
                        var bx1 = bx;
                        var by1 = by;
                        var bz1 = bz;
                        var cx1 = cx;
                        var cy1 = cy;
                        var cz1 = cz;

                    }

                    proLinePoints[i] = [];

                    proLinePoints[i].ax = ax;
                    proLinePoints[i].bx = bx;
                    proLinePoints[i].cx = cx;
                    proLinePoints[i].ay = ay;
                    proLinePoints[i].by = by;
                    proLinePoints[i].cy = cy;
                    proLinePoints[i].az = az;
                    proLinePoints[i].bz = bz;
                    proLinePoints[i].cz = cz;

                    if((!proFaces.a)&&(!proFaces.b)&&(!proFaces.c)) {
                        proFaces.a = [];
                        proFaces.b = [];
                        proFaces.c = [];
                        proFaces.a.x = ax;
                        proFaces.a.y = ay;
                        proFaces.a.z = az;
                        proFaces.b.x = bx;
                        proFaces.b.y = by;
                        proFaces.b.z = bz;
                        proFaces.c.x = cx;
                        proFaces.c.y = cy;
                        proFaces.c.z = cz;
                    }
                }

                proLinePoints.forEach(function(v, n){
                    if(proLinePoints[n+1]) {
                        var v1 = proLinePoints[n+1];

                        var dax = (v1.ax - v.ax) / 10;
                        var dbx = (v1.bx - v.bx) / 10;
                        var dcx = (v1.cx - v.cx) / 10;
                        var day = (v1.ay - v.ay) / 10;
                        var dby = (v1.by - v.by) / 10;
                        var dcy = (v1.cy - v.cy) / 10;
                        var daz = (v1.az - v.az) / 10;
                        var dbz = (v1.bz - v.bz) / 10;
                        var dcz = (v1.cz - v.cz) / 10;

                        for (i=0;i<10;i++) {
                            var nax = v.ax + (dax * i);
                            var nbx = v.bx + (dbx * i);
                            var ncx = v.cx + (dcx * i);
                            var nay = v.ay + (day * i);
                            var nby = v.by + (dby * i);
                            var ncy = v.cy + (dcy * i);
                            var naz = v.az + (daz * i);
                            var nbz = v.bz + (dbz * i);
                            var ncz = v.cz + (dcz * i);

                            geometry.vertices.push(
                                new THREE.Vector3(nax, nay, naz),
                                new THREE.Vector3(nbx, nby, nbz),
                                new THREE.Vector3(ncx, ncy, ncz),
                                new THREE.Vector3(nax, nay, naz)
                            );

                        }
                    } else {
                        geometry.vertices.push(
                            new THREE.Vector3(ax, ay, az),
                            new THREE.Vector3(bx, by, bz),
                            new THREE.Vector3(cx, cy, cz),
                            new THREE.Vector3(ax, ay, az)
                        );
                        proFaces.a1.x = ax;
                        proFaces.a1.y = ay;
                        proFaces.a1.z = az;
                        proFaces.b1.x = bx;
                        proFaces.b1.y = by;
                        proFaces.b1.z = bz;
                        proFaces.c1.x = cx;
                        proFaces.c1.y = cy;
                        proFaces.c1.z = cz;
                    }
                });
                line.add(new THREE.Line(geometry, material));

                var textTo = professionsList[faceIndex];

                var textCoords = getTextpos(ax1, ay1, az1, bx1, by1, bz1, cx1, cy1, cz1);

                var spritey = makeTextSprite( " " + textTo + " ", { fontsize: 22, backgroundColor: {r:100, g:100, b:255, a:1} } );
                spritey.position.x = textCoords[0];
                spritey.position.y = textCoords[1];
                spritey.position.z = textCoords[2];
                line.add(spritey);

                var a = gsphere.geometry.faces[faceIndex].a;
                var x0 = gsphere.geometry.vertices[a].x;
                var y0 = gsphere.geometry.vertices[a].y;
                var z0 = gsphere.geometry.vertices[a].z;

                var textMesh = spritey.clone();
                textMesh.position.x = x0;
                textMesh.position.y = y0;
                textMesh.position.z = z0;
                line.add(textMesh);

                //line.add(new THREE.AreaLight({color: 0xffffff}));

                scene1Container.add(line);

                /*
                if (spheresArray[fIndex][level] === false) {
                    loadAllUsers(fIndex,level);
                }
                */
                for(var i = 0;i<icosaednrons.length;i++) {
                    if (spheresArray[faceIndex][i] === false) {
                        loadAllUsers(faceIndex,i,null,true);
                    } else {
                        scene1Container.add(spheresArray[faceIndex][i]);
                    }

                }

                return line;
            }
        }

        //Получение координат расположения текста
        var getTextpos = function(ax,ay,az,bx,by,bz,cx,cy,cz) {
            var dx = cx + (bx - cx)/2;
            var dy = cy + (by - cy)/2;
            var dz = cz + (bz - cz)/2;

            var x = bx + (ax - dx)/2;
            var y = by + (ay - dy)/2;
            var z = bz + (az - dz)/2;

            //return [x,y,z];
            return [ax,ay,az];

        }

        //дробрая часть от деления
        var getDecimal = function (num) {
            var str = "" + num;
            var zeroPos = str.indexOf(".");
            if (zeroPos == -1) return 0;
            str = str.slice(zeroPos);
            return +str;
        }

        //Генерация координат точки в пределах нужного луча и уровня (level - уровень, faceIndex - айди проффесии)
        var levelPoint = function(level,faceIndex,steps) {
            level = level || 2; //уровень пользователя
            faceIndex = faceIndex || 1; //номер луча (професии)
            steps = steps || 10;//количество пересечений сетки


            var level0 = (level / 10 | 0);
            var level1 = (getDecimal(level/10)*10);

            //получение координат сторон a,b,c
            var a = icosaednrons[10].geometry.faces[faceIndex].a;
            var b = icosaednrons[10].geometry.faces[faceIndex].b;
            var c = icosaednrons[10].geometry.faces[faceIndex].c;
            var ax = icosaednrons[10].geometry.vertices[a].x;
            var bx = icosaednrons[10].geometry.vertices[b].x;
            var cx = icosaednrons[10].geometry.vertices[c].x;
            var ay = icosaednrons[10].geometry.vertices[a].y;
            var by = icosaednrons[10].geometry.vertices[b].y;
            var cy = icosaednrons[10].geometry.vertices[c].y;
            var az = icosaednrons[10].geometry.vertices[a].z;
            var bz = icosaednrons[10].geometry.vertices[b].z;
            var cz = icosaednrons[10].geometry.vertices[c].z;

                var a1 = icosaednrons[0].geometry.faces[faceIndex].a;
                var b1 = icosaednrons[0].geometry.faces[faceIndex].b;
                var c1 = icosaednrons[0].geometry.faces[faceIndex].c;
                var a1x = icosaednrons[0].geometry.vertices[a].x;
                var b1x = icosaednrons[0].geometry.vertices[b].x;
                var c1x = icosaednrons[0].geometry.vertices[c].x;
                var a1y = icosaednrons[0].geometry.vertices[a].y;
                var b1y = icosaednrons[0].geometry.vertices[b].y;
                var c1y = icosaednrons[0].geometry.vertices[c].y;
                var a1z = icosaednrons[0].geometry.vertices[a].z;
                var b1z = icosaednrons[0].geometry.vertices[b].z;
                var c1z = icosaednrons[0].geometry.vertices[c].z;


            //Деление на подуровни
            var deltaAX = (ax - a1x) / 100;
            var deltaBX = (bx - b1x) / 100;
            var deltaCX = (cx - c1x) / 100;
            var deltaAY = (ay - a1y) / 100;
            var deltaBY = (by - b1y) / 100;
            var deltaCY = (cy - c1y) / 100;
            var deltaAZ = (az - a1z) / 100;
            var deltaBZ = (bz - b1z) / 100;
            var deltaCZ = (cz - c1z) / 100;

            ax = a1x + (deltaAX * (level0 * 10 + level1));
            bx = b1x + (deltaBX * (level0 * 10 + level1));
            cx = c1x + (deltaCX * (level0 * 10 + level1));
            ay = a1y + (deltaAY * (level0 * 10 + level1));
            by = b1y + (deltaBY * (level0 * 10 + level1));
            cy = c1y + (deltaCY * (level0 * 10 + level1));
            az = a1z + (deltaAZ * (level0 * 10 + level1));
            bz = b1z + (deltaBZ * (level0 * 10 + level1));
            cz = c1z + (deltaCZ * (level0 * 10 + level1));

            a1x = a1x + (deltaAX * (level0 * 10 + level1-1));
            b1x = b1x + (deltaBX * (level0 * 10 + level1-1));
            c1x = c1x + (deltaCX * (level0 * 10 + level1-1));
            a1y = a1y + (deltaAY * (level0 * 10 + level1-1));
            b1y = b1y + (deltaBY * (level0 * 10 + level1-1));
            c1y = c1y + (deltaCY * (level0 * 10 + level1-1));
            a1z = a1z + (deltaAZ * (level0 * 10 + level1-1));
            b1z = b1z + (deltaBZ * (level0 * 10 + level1-1));
            c1z = c1z + (deltaCZ * (level0 * 10 + level1-1));

            // Координаты триугольного подуровня
            var Vpos = getRandomInt(1,steps);
            var max =  a1x + ((ax - a1x) / steps * Vpos);
            var may =  a1y + ((ay - a1y) / steps * Vpos);
            var maz =  a1z + ((az - a1z) / steps * Vpos);
            var mbx =  b1x + ((bx - b1x) / steps * Vpos);
            var mby =  b1y + ((by - b1y) / steps * Vpos);
            var mbz =  b1z + ((bz - b1z) / steps * Vpos);
            var mcx =  c1x + ((cx - c1x) / steps * Vpos);
            var mcy =  c1y + ((cy - c1y) / steps * Vpos);
            var mcz =  c1z + ((cz - c1z) / steps * Vpos);

            //Уровень точки по cтороне
            var pos = getRandomInt(1,steps);
            var lax = max;
            var lay = may;
            var laz = maz;
            var lbx = mbx + ((mcx - mbx) / steps * pos);
            var lby = mby + ((mcy - mby) / steps * pos);
            var lbz = mbz + ((mcz - mbz) / steps * pos);

            // Получение координат точки
            pos = getRandomInt(1,steps);
            var px = lax + ((lbx - lax) / steps * pos);
            var py = lay + ((lby - lay) / steps * pos);
            var pz = laz + ((lbz - laz) / steps * pos);

            return ([px,py,pz]);
        }

        ///////////////////////////////////////////////////////////////////

        this.setCamera = function ( value ) {

            camera = value;
            //camera = new THREE.PerspectiveCamera(60,0,1,1000);
            //camera.lookAt(0,0,0);
            camera.aspect = this.width / this.height;
            camera.updateProjectionMatrix();


            if ( vr === true ) {

                if ( camera.parent === undefined ) {

                    scene.add( camera );

                }

                var camera2 = camera.clone();
                camera.add( camera2 );

                camera = camera2;

                //controls = new THREE.VRControls( camera );
                //renderer = new THREE.VREffect( renderer );



                document.addEventListener( 'keyup', function ( event ) {

                    switch ( event.keyCode ) {
                        case 90:
                            controls.zeroSensor();
                            break;
                    }

                } );

            }

        };

        this.setScene = function ( value ) {

            scene = value;

        },

            this.controlSet = function (enabled) {
                if(controls){controls.enabled = enabled;}
            },

            this.pos = function (x,y,z) {
                if(PControl){
                    PControl.getObject().position.x = x;
                    PControl.getObject().position.y = y;
                    PControl.getObject().position.z = z;
                }
            },

            this.closeBook = function() {
                bookReadingMode = false;
                scene2Container.remove(activeBook);
                PControl.enabled = true;
                $('body').click();

                $('#controlImage').removeAttr('style');
                $('#controlName').removeAttr('style');
                $('#controlBody').html(savedControlBody);
                savedControlBody = '';
                $('#controlPanel').attr('style','display:none;');
                $('#book_frame').remove();
                controlDialogShow  = false;
            },

            this.selectProf = function(faceIndex) {
                selectProffesion(faceIndex);
            },

            this.goToRoom = function() {
                if((myid>-1)&&(!inRoom)) {
                    clearDOM();

                    var item = [];
                    item.object = mybox;

                    if (item.object.name.indexOf('Box') > -1) {
                        pos0.x = camera.position.x;
                        pos0.y = camera.position.y;
                        pos0.z = camera.position.z;
                        pos1.x = item.object.position.x;
                        pos1.y = item.object.position.y;
                        pos1.z = item.object.position.z;

                        profileFound = true;
                        var uId = item.object.name.replace(/[^\d]/gi, '');
                        currentUserId = uId;
                        $('#coubId').val(uId);

                        cameraRotationSpeed = 0;
                        controls.enabled = false;


                        var a = camera.position;
                        retCoords = a;
                        var b = item.object.position;

                        //controls.target = new THREE.Vector3(b.x, b.y, b.z);

                        //Количество шагов
                        var steps = 200;

                        //Один шаг
                        var delta = [];
                        delta.x = (a.x - b.x) / steps;
                        delta.y = (a.y - b.y) / steps;
                        delta.z = (a.z - b.z) / steps;
                        canLoadProfiles = false;

                        controlsCenter(item.object.position);
                        jumpToSphere(delta, steps, b, item.object);

                        roomTextures.floor = 'images/textures/room/pol.jpg';
                        roomTextures.ceil = 'images/textures/room/beton.jpg';
                        roomTextures.left = 'images/textures/room/kirpich.jpg';
                        roomTextures.right = 'images/textures/room/kirpich.jpg';

                        $.ajax({
                            type: "POST",
                            url: "/index.php",
                            data: {request: 'room_textures', uid: uId},
                            dataType: "json",
                            success: function (json) {
                                $.each(json, function (i, v) {
                                    var user_folder = 'userphoto/' + v.user_id + '/userroom/';

                                    if (v.user_room_flooring) {
                                        roomTextures.floor = user_folder + v.user_room_flooring;
                                    }
                                    if (v.user_room_ceiling) {
                                        roomTextures.ceil = user_folder + v.user_room_ceiling;
                                    }
                                    if (v.user_room_wall_left) {
                                        roomTextures.right = user_folder + v.user_room_wall_left;
                                    }
                                    if (v.user_room_wall_right) {
                                        roomTextures.left = user_folder + v.user_room_wall_right;
                                    }

                                });
                            }
                        });

                        return false;
                    }
                }
            },

            this.addToFriend = function () {

                $.ajax({
                    type: "POST",
                    url: "ajax/addfriend.php",
                    data: 'act=add&uid='+userRoomId+'&state=1',
                    success: function(html) {
                        var controlBody = fromFriendText;
                        controlBody = controlBody + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                        $('#controlBody').html(controlBody);
                    }
                });

            },

            this.updateDistance = function (pos, obj) {
                obj = obj || camera;

                if(pos!=currentDistance) {

                    var perc = 100 - currentDistance;
                    if(perc == 0) {perc = 1;}

                    var deltaX = obj.position.x / perc;
                    var deltaY = obj.position.y / perc;
                    var deltaZ = obj.position.z / perc;

                    var newX = +deltaX * (100 - pos);
                    var newY = +deltaY * (100 - pos);
                    var newZ = +deltaZ * (100 - pos);

                    obj.position.x = newX;
                    obj.position.y = newY;
                    obj.position.z = newZ;

                    if(obj == camera) {
                        currentDistance = pos;
                    }

                    var lightIntensivity = (100 - currentDistance) / 100;
                    if(lightIntensivity<0.1) {lightIntensivity = 0.1;}
                    light.intensity = lightIntensivity;
                    light_1.intensity = lightIntensivity;
                }

            },

            this.addToRead = function () {
                $.ajax({
                    type: "POST",
                    url: "ajax/addfriend.php",
                    data: 'act=add&uid='+userRoomId+'&state=2',
                    success: function() {
                        var controlBody = toFriendText+fromReadText;
                        controlBody = controlBody + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                        $('#controlBody').html(controlBody);
                    }
                });
            },

            this.removeFromFriend = function () {
                $.ajax({
                    type: "POST",
                    url: "ajax/addfriend.php",
                    data: 'act=remove&uid='+userRoomId+'&state=1',
                    success: function() {
                        var controlBody = toFriendText+toReadText;
                        controlBody = controlBody + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                        $('#controlBody').html(controlBody);
                    }
                });
            },

            this.removeFromRead = function () {
                $.ajax({
                    type: "POST",
                    url: "ajax/addfriend.php",
                    data: 'act=remove&uid='+userRoomId+'&state=2',
                    success: function() {
                        var controlBody = toFriendText+toReadText;
                        controlBody = controlBody + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                        $('#controlBody').html(controlBody);
                    }
                });
            },

            this.showConnections = function(show) {
                showConnectionsAct = show;
                if(show) {
                    scene1Container.add(connectionLines);
                }else{
                    scene1Container.remove(connectionLines);
                }
            },

            this.setSize = function ( width, height ) {

                if ( renderer._fullScreen ) return;

                this.width = width;
                this.height = height;

                camera.aspect = this.width / this.height;
                camera.updateProjectionMatrix();

                renderer.setSize( width, height );

            };

        var dispatch = function ( array, event ) {

            //for ( var i = 0, l = array.length; i < l; i ++ ) {
            //    array[ i ]( event );
            //}

        };

        var request;

        var controlsCenter = function(b,step,n) {
            if(controls) {
                step = step || false;
                n = n || 0;
                var a = controls.target;

                if (step == false) {
                    var step = [];
                    step.x = (b.x - a.x) / 300;
                    step.y = (b.y - a.y) / 300;
                    step.z = (b.z - a.z) / 300;
                }

                if (n < 300) {
                    n++;
                    var pos = [];
                    pos.x = +a.x + (step.x * +n);
                    pos.y = +a.y + (step.y * +n);
                    pos.z = +a.z + (step.z * +n);

                    controls.center = pos;
                    controls.target = controls.center;
                    controls.object.lookAt(controls.target);
                    controls.update();

                    setTimeout(function () {
                        controlsCenter(b, step, n);
                    }, 15);
                } else {
                    var pos = b;
                    controls.center = pos;
                    controls.target = controls.center;
                    controls.object.lookAt(controls.target);
                    controls.update();
                }

            }
        }

        var clearDOM = function() {
            $('.reg_body, .reg_body2, .v_colour, .reg_body5').remove();
            resetLocation();
        }

        var checkDistance = function (v1,v2){

            deltaX = v2.x - v1.x;
            deltaY = v2.y - v1.y;
            deltaZ = v2.z - v1.z;

            distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);

            return distance.toFixed((1));
        }

        var setDistance = function (distance){

        }

        var animate = function ( time ) {

            request = requestAnimationFrame( animate );

            dispatch( events.update, { time: time, delta: time - prevTime } );

            if(bookReadingMode) {
                controls.enabled = false;
            }

            if(inRoom) {
                /* Зеркала
                groundMirror.renderWithMirror( verticalMirror );
                verticalMirror.renderWithMirror( groundMirror );
                */
                cubeCamera.updateCubeMap(renderer, scene);
                water.material.uniforms.time.value += 1.0 / 60.0;
                water.render();
                if(canPlayVideo) {
                    if ( video.readyState === video.HAVE_ENOUGH_DATA )
                    {
                        videoImageContext.drawImage( video, 0, 0 );
                        if ( videoTexture )
                            videoTexture.needsUpdate = true;
                    }
                }
            }
            if((canLoadProfiles)&&(inRoom)&&(PointerLock())&&(!bookReadingMode)) {

                if ((!checkExit(12,null,10))&&(!exitRoom)) {

                    var moveSpeed = 200.0;//Скорость движения
                    var strongRoom = true;//Не выходить за пределы комнаты

                    if (!roomFurnitureMoving) {
                        var time = performance.now();
                        var delta = ( time - prevTime ) / 1000;

                        velocity.x -= velocity.x * 400.0 * delta;
                        velocity.z -= velocity.z * 400.0 * delta;

                        velocity.x = velocity.z = 0;

                        if (moveForward) velocity.z = -1 * moveSpeed * delta;
                        if (moveBackward) velocity.z = moveSpeed * delta;

                        if (moveLeft) velocity.x = -1 * moveSpeed * delta;
                        if (moveRight) velocity.x = moveSpeed * delta;

                        PControl.getObject().translateX(velocity.x * delta);
                        PControl.getObject().translateZ(velocity.z * delta);

                        if (strongRoom) {
                            if (PControl.getObject().position.x > 12.3) {
                                PControl.getObject().position.x = 12.3;
                            }

                            if (PControl.getObject().position.x < -12.3) {
                                PControl.getObject().position.x = -12.3;
                            }

                            if (PControl.getObject().position.z > 10.9) {
                                PControl.getObject().position.z = 10.9;
                            }

                            if (PControl.getObject().position.z < -10.9) {
                                PControl.getObject().position.z = -10.9;
                            }
                        }

                        camera.updateProjectionMatrix();

                        prevTime = time;
                    } else {
                        if(!bookMoving) {

                            var moveSpeed = 0.1;

                            var elX = activeFurnitureClone.position.x;
                            var elZ = activeFurnitureClone.position.z;
                            if (inArray(activeFurnitureClone.movingAxis, 'x')) {
                                if (moveForward) elX += moveSpeed;
                                if (moveBackward) elX -= moveSpeed;
                                if (elX > activeFurnitureClone.positionLimits.max[0]) {
                                    elX = activeFurnitureClone.positionLimits.max[0];
                                }
                                if (elX < activeFurnitureClone.positionLimits.min[0]) {
                                    elX = activeFurnitureClone.positionLimits.min[0];
                                }
                                if (activeFurnitureClone.positionLimits.max[0] == 0) {
                                    elX = activeFurnitureClone.positionLimits.max[0];
                                }
                                if (activeFurnitureClone.positionLimits.min[0] == 0) {
                                    elX = activeFurnitureClone.positionLimits.min[0];
                                }
                            }
                            if (inArray(activeFurnitureClone.movingAxis, 'z')) {
                                if (moveLeft) velocity.x = elZ -= moveSpeed;
                                if (moveRight) velocity.x = elZ += moveSpeed;
                                if (elZ > activeFurnitureClone.positionLimits.max[2]) {
                                    elZ = activeFurnitureClone.positionLimits.max[2];
                                }
                                if (elZ < activeFurnitureClone.positionLimits.min[2]) {
                                    elZ = activeFurnitureClone.positionLimits.min[2];
                                }
                                if (activeFurnitureClone.positionLimits.max[2] == 0) {
                                    elZ = activeFurnitureClone.positionLimits.max[2];
                                }
                                if (activeFurnitureClone.positionLimits.min[2] == 0) {
                                    elZ = activeFurnitureClone.positionLimits.min[2];
                                }
                            }

                            activeFurnitureClone.position.x = elX;
                            activeFurnitureClone.position.z = elZ;
                        } else {
                            var moveSpeed = 0.02;

                            var elX = activeFurnitureClone.position.x*1;
                            var elY = activeFurnitureClone.position.y*1;
                            var elZ = activeFurnitureClone.position.z*1;

                            if (bookXUp) { elZ -= +moveSpeed*1; }
                            if (bookXDown) { elZ += +moveSpeed*1; }
                            if (bookYUp) { elY += +moveSpeed*1; }
                            if (bookYDown) { elY -= +moveSpeed*1; }
                            if (bookZUp) { elX += +moveSpeed*1; }
                            if (bookZDown) { elX -= +moveSpeed*1; }

                            activeFurnitureClone.position.x = elX;
                            activeFurnitureClone.position.y = elY;
                            activeFurnitureClone.position.z = elZ;
                        }
                    }

                } else {
                    doExitRoom();
                }
            }
            if(!inRoom){
                if(myid!=-1) {
                    Rotation(camera, scene1Container);
                    controls.update();
                } else {
                    camera.rotation.z = degToRad(-23);
                }

                checkForObjects();
                rotateLight();

                var distance = checkDistance(camera.position,new THREE.Vector3(0,0,0));
                var newDistance = 100 - (distance / 8).toFixed(8);
                if(newDistance != currentDistance) {
                    currentDistance = 100 - (distance / 8).toFixed(8);
                    setScroolPos(currentDistance);
                }

            }

            if (PointerLock()) {

            }else {

            }

            renderer.render( scene, camera );

        };

        var objRotate = function(obj,time) {
            time = time || 1000;

            var step = 360 / time * 5;

            obj.rotation.y +=degToRad(-step);

            setTimeout(function(){objRotate(obj,time);},5);

        }

        var doExitRoom = function() {
            canLoadProfiles = false;
            exitRoom = false;
            document.exitPointerLock = document.exitPointerLock    ||
                document.mozExitPointerLock ||
                document.webkitExitPointerLock;
            document.exitPointerLock();

            scene.remove(scene2Container);
            scene.add(scene1Container);
            renderer.setClearColor( 0x002035 );

            camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1700 );
            player.setCamera( camera );
            controls = new THREE.TrackballControls( camera );
            controls.rotateSpeed = 0.3;
            controls.zoomSpeed = 0.2;
            controls.panSpeed = 1.8;
            controls.noZoom = false;
            controls.noPan = false;
            controls.staticMoving = false;
            controls.dynamicDampingFactor = 0.07;
            controls.zoomSpeed = 0.01;
            controls.noPan = true;
            controls.maxDistance = 800;
            controls.enabled = true;
            inRoom = false;
            moveInRoom = false;

            PControl.enabled = false;
            PControl = false;

            moveForward = false;
            moveBackward = false;
            moveLeft = false;
            moveRight = false;

            var canvas = document.body;
            canvas.onclick = function() {}

            $('#controlPanel').attr('style','display:none !important;');
            $('#zoom_control').attr('style','height:'+(window.innerHeight-100)+'px;');

            camera.position.x = pos1.x;
            camera.position.y = pos1.y;
            camera.position.z = pos1.z;

            var center = [];
            center.x = 0;
            center.y = 0;
            center.z = 0;
            controlsCenter(center);

            var tbox = new THREE.Mesh(new THREE.BoxGeometry(1,1,1),new THREE.MeshBasicMaterial({transparent: true, opacity: 0}));
            tbox.visible = false;
            tbox.position.x = pos0.x;
            tbox.position.y = pos0.y;
            tbox.position.z = pos0.z;
            scene1Container.add(tbox);

            var a = camera.position;
            var b = tbox.position;

            //Количество шагов
            var steps = 200;


            //Один шаг
            var delta = [];
            delta.x = (a.x - b.x) / steps;
            delta.y = (a.y - b.y) / steps;
            delta.z = (a.z - b.z) / steps;

            jumpToSphere(delta,steps,b,tbox,'no');
        }

        var checkExit = function(x,y,z) {
            var pos = PControl.getObject().position;
            if((pos.x>=x)&&(pos.z>=z)) {
                    return true;
            } else {return false;}
        }

        var inArray = function(array,needle) {
            var ret = false;
            array.forEach(function(el){
                if (el == needle) {
                    ret = true;
                    return ret;
                }
            });
            return ret;
        }

        var rotateLight = function(){

            //checkDistance(v1,v2);
            var v1 = new THREE.Vector3(0,0,0);
            //Light ------------------------------
            var v2 = camera.position.clone();
            while(checkDistance(v1,v2)<750) {
                v2.x = v2.x + (v2.x / 100);
                v2.y = v2.y + (v2.y / 100);
                v2.z = v2.z + (v2.z / 100);
            }
            light.position.set(v2.x,v2.y,v2.z);
            light_1.position.set((v2.x*-1),v2.y,v2.z);
            light_2.position.set((v2.x*-1),(v2.y*-1),v2.z);
            light_3.position.set((v2.x*-1),(v2.y*-1),(v2.z*-1));
            /*

             var x = camera.position.x,
             y = camera.position.y,
             z = camera.position.z;
             rotPos = 15;
             rotNeg = -15;

             var zm = 50;

             light.position.x = x + zm;
             light.position.y = y + zm;
             light.position.z = z + zm;

             light_2.position.x = x;
             light_2.position.y = y;
             light_2.position.z = z;

             x = x * Math.cos(rotPos) + z * Math.sin(rotPos);
             z = z * Math.cos(rotPos) - x * Math.sin(rotPos);

             light_1.position.x = x;
             light_1.position.y = y;
             light_1.position.z = z;

             player.updateDistance (0, light);
             player.updateDistance (0, light_1);
            //------------------------------------
            */

        }


        this.play = function () {

            document.addEventListener( 'keydown', onDocumentKeyDown );
            document.addEventListener( 'keyup', onDocumentKeyUp );
            document.addEventListener( 'keypress', onDocumentKeyPress );
            document.addEventListener( 'mousedown', onDocumentMouseDown );
            document.addEventListener( 'mouseup', onDocumentMouseUp );
            document.addEventListener( 'mousemove', onDocumentMouseMove );
            document.addEventListener( 'touchstart', onDocumentTouchStart );
            document.addEventListener( 'touchend', onDocumentTouchEnd );
            document.addEventListener( 'touchmove', onDocumentTouchMove );
            document.addEventListener( 'mousewheel', onDocumentWheel );
            document.addEventListener( 'dblclick', onDblClick );

            request = requestAnimationFrame( animate );
            prevTime = performance.now();

        };

        this.stop = function () {

            document.removeEventListener( 'keydown', onDocumentKeyDown );
            document.removeEventListener( 'keyup', onDocumentKeyUp );
            document.removeEventListener( 'keypress', onDocumentKeyPress );
            document.removeEventListener( 'mousedown', onDocumentMouseDown );
            document.removeEventListener( 'mouseup', onDocumentMouseUp );
            document.removeEventListener( 'mousemove', onDocumentMouseMove );
            document.removeEventListener( 'touchstart', onDocumentTouchStart );
            document.removeEventListener( 'touchend', onDocumentTouchEnd );
            document.removeEventListener( 'touchmove', onDocumentTouchMove );
            document.removeEventListener( 'mousewheel', onDocumentWheel );
            document.removeEventListener( 'dblclick', onDblClick );

            cancelAnimationFrame( request );

        };

        var onDocumentKeyDown = function ( event ) {

            dispatch( events.keydown, event );

            if(inRoom) {
                if(event.which==27){
                    exitRoom = true;
                    doExitRoom();
                }
                if(((event.which==49)||(event.which==50))&&($('#btn_'+event.which))) {
                    $('#btn_'+event.which).click();
                }

            }

            if(event.which == 17) {
                ctrlDown = true;
                controls.zoomSpeed = 0.7;
                controls.rotateSpeed = 3.3;
                controls.staticMoving = true;
            }

            if((ctrlDown)&&(event.which == 67)&&(myid!=-1)) {
                if(controlDialogShow) {
                    $('#controlPanel').attr('style', 'display: none !important;');
                    controlDialogShow = false;
                }else{
                    $('#controlPanel').removeAttr('style');
                    controlDialogShow = true;
                }

            }

            if(inRoom) {
                switch ( event.keyCode ) {

                    case 38: // up
                    case 87: // w
                        moveForward = true;
                        break;

                    case 37: // left
                    case 65: // a
                        moveLeft = true;
                        break;

                    case 40: // down
                    case 83: // s
                        moveBackward = true;
                        break;

                    case 39: // right
                    case 68: // d
                        moveRight = true;
                        break;

                }

                switch ( event.keyCode ) {

                    case 87: // w
                        bookYUp = true;
                        break;

                    case 83: // s
                        bookYDown = true;
                        break;

                    case 38: // up
                        bookXUp = true;
                        break;

                    case 40: // down
                        bookXDown = true;
                        break;

                    case 37: // left
                        bookZDown = true;
                        break;

                    case 39: // right
                        bookZUp = true;
                        break;

                }

            }

        };

        var onDocumentKeyUp = function ( event ) {


            dispatch( events.keyup, event );
            if(event.which == 17) {
                ctrlDown = false;
                controls.zoomSpeed = 0.03;
                controls.rotateSpeed = 0.3;
                controls.staticMoving = false;
            }

            if(inRoom) {
                switch( event.keyCode ) {

                    case 38: // up
                    case 87: // w
                        moveForward = false;
                        break;

                    case 37: // left
                    case 65: // a
                        moveLeft = false;
                        break;

                    case 40: // down
                    case 83: // s
                        moveBackward = false;
                        break;

                    case 39: // right
                    case 68: // d
                        moveRight = false;
                        break;

                }

                switch ( event.keyCode ) {

                    case 87: // w
                        bookYUp = false;
                        break;

                    case 83: // s
                        bookYDown = false;
                        break;

                    case 38: // up
                        bookXUp = false;
                        break;

                    case 40: // down
                        bookXDown = false;
                        break;

                    case 37: // left
                        bookZDown = false;
                        break;

                    case 39: // right
                        bookZUp = false;
                        break;

                }
            }

        };

        var onDocumentKeyPress = function ( event ) {

            dispatch( events.keypress, event );

        }

        var mouseDown = false;

        var onDocumentMouseDown = function ( event ) {
            mouseDown = true;
            if(controls.enabled) {
                $('#map_canvas').animate({opacity: 0}, 300, null, function () {
                    $('#map_canvas').attr('style', 'display:none;');
                    showMap = false;
                });
            }

            dispatch(events.mousedown, event);

            var rayCaster = new THREE.Raycaster();
            var mouse = new THREE.Vector2();

            if(inRoom) {
                var el = chechForObjectAtMouse('tvset',scene2Container);
                if(el!=false){
                    if(videoplay == true) {
                        videoplay = false;
                        video.pause();
                    }else{
                        videoplay = true;
                        video.play();
                    }
                }
            } else {
                mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
                mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

                rayCaster.setFromCamera(mouse, camera);

                var intersectedObjects = rayCaster.intersectObjects(scene.children, true);

                var profileFound = false;

                var uId;

                intersectedObjects.forEach(function (item) {

                    if (((item.object.name.indexOf('Box') > -1)&&(!profileFound))||(wasRememberedE)) {

                        var curEl = wasRememberedE || item.object;

                        profileFound = true;
                        uId = curEl.name.replace(/[^\d]/gi, '');

                        onceClick = true;
                        if(uId!=myid) {
                            setTimeout(function () {
                                loadProfileView(uId);
                            }, 500);
                        }

                        var myBoxPos = [];
                        myBoxPos[0] = curEl.position.x;
                        myBoxPos[1] = curEl.position.y;
                        myBoxPos[2] = curEl.position.z;

                        /*
                        var sphere = new THREE.Mesh(
                            new THREE.SphereGeometry(1.5,32,32),
                            new THREE.MeshLambertMaterial({color: 0xffffff, transparent: true, opacity: 0.1})
                        );
                        sphere.position.x = myBoxPos[0];
                        sphere.position.y = myBoxPos[1];
                        sphere.position.z = myBoxPos[2];
                        scene1Container.add(sphere);
                        scaleElement(sphere,scene1Container);
                        */

                        addConnectionLines(uId);
                        return false;
                    }
                });

                if(intersectedObjects.length<1) {

                        if (wasRememberedE) {

                            var curEl = wasRememberedE;

                            profileFound = true;
                            uId = curEl.name.replace(/[^\d]/gi, '');

                            onceClick = true;
                            if(uId!=myid) {
                                setTimeout(function () {
                                    loadProfileView(uId);
                                }, 500);
                            }

                            var myBoxPos = [];
                            myBoxPos[0] = curEl.position.x;
                            myBoxPos[1] = curEl.position.y;
                            myBoxPos[2] = curEl.position.z;

                            /*
                            var sphere = new THREE.Mesh(
                                new THREE.SphereGeometry(1.5,32,32),
                                new THREE.MeshLambertMaterial({color: 0xffffff, transparent: true, opacity: 0.1})
                            );
                            sphere.position.x = myBoxPos[0];
                            sphere.position.y = myBoxPos[1];
                            sphere.position.z = myBoxPos[2];
                            scene1Container.add(sphere);
                            scaleElement(sphere,scene1Container);
                            */

                            addConnectionLines(uId);
                            return false;
                        }

                }

                if(profileFound) {
                    users.forEach(function (u) {
                        if (u.user_id == uId) {
                            selectProffesion(u.user_profession);
                        }
                    });
                }

                //End_of_searching_____________________________________

            }
        }

        var loadProfileView = function(uId) {
            if (onceClick) {
                onceClick = false;
                $.ajax({
                    type: "POST",
                    url: "/ajax/user_view.php",
                    data: 'uid=' + uId,
                    success: function (html) {
                        if (html != 0) {
                            if ($('#user_profile_view')) {
                                $('#user_profile_view').remove();
                            }
                            $('body').append(html);
                            var w = window.innerWidth - 40;
                            var h = window.innerHeight - 100;
                            $('#user_profile_view').attr('style', 'width:' + w + 'px !important;height:' + h + 'px !important;opacity:0;');
                            if (player) {
                                player.controlSet(false);
                            }
                            $('#user_profile_view').animate({opacity: 1}, 500);
                            setLocation('personal.html');
                            //dhtmlLoadScript('/js/sh_personal.js');
                        }
                    }
                });
            }
        }

        function dhtmlLoadScript(url)
        {
            var e = document.createElement("script");
            e.src = url;
            e.type="text/javascript";
            document.getElementsByTagName("head")[0].appendChild(e);
        }

        var clearLastMovedElement = function() {
            lastMovedElement = false;
        }

        var scaleElement = function (e,parent) {
            parent = parent || scene;
            var scale = e.scale.x + 15;
            if (scale<1000) {
                e.scale.x = scale;
                e.scale.y = scale;
                e.scale.z = scale;
                setTimeout(function(){scaleElement(e,parent);},5);
            } else {
                parent.remove(e);
            }
        }

        var animateSelection = function(recursive) {
            recursive = recursive || 1;

            var maxScaledE = false;
            if(profileSelection) {
                profileSelection.children.forEach(function(e){
                    if(e.name=='wave') {
                        //min^ 0,9 0,11
                        //max^ 1,9 0,06

                        if (!maxScaledE) {
                            maxScaledE = e;
                        }

                        var eret = false;

                        var scale = e.scale.x + 0.04;
                        var opacity = e.material.opacity - 0.001;
                        if (scale > 2.9) {
                            scale = 0.9;
                            opacity = 0.2;
                            eret = true;
                        }

                        if (scale > maxScaledE.scale.x) {
                            maxScaledE = e;
                        }
                        if((eret)&&(recursive == 2)) {
                            profileSelection.remove(e);
                        }else {
                            e.scale.x = scale;
                            e.scale.y = scale;
                            e.scale.z = scale;
                            e.material.opacity = opacity;
                        }
                    }

                });

                maxScaledE.material.opacity -= 0.005;

                if(profileSelection.children.length == 0) {
                    profileSelection = false;
                }

                setTimeout(function(){animateSelection(recursive);},5);
            }else{
                selectionAnimated = false;
            }
        }


        var onDblClick = function ( event ) {
            mouseDown = true;
            onceClick = false;

            dispatch(events.dblclick, event);

            if(!inRoom) {
                if(!lastMovedElement) {
                    //Starting_search_of_coube_on_mouse_position__________
                    var rayCaster = new THREE.Raycaster();
                    var mouse = new THREE.Vector2();

                    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
                    mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

                    rayCaster.setFromCamera(mouse, camera);

                    var intersectedObjects = rayCaster.intersectObjects(scene.children, true);


                    var profileFound = false;

                    intersectedObjects.forEach(function (item) {
                        checkForUserBox(item.object);
                    });
                    //End_of_searching_____________________________________
                } else
                if(wasRememberedE){
                    checkForUserBox(wasRememberedE);
                }else{
                    checkForUserBox(lastMovedElement);
                }
            } else
            if((!bookReadingMode)&&(inRoom)&&(event.which == 1)){
                var el = chechForObjectAtMouse('CanSelectObject',scene2Container);
                if(el!=false) {
                    activeBook = el.clone();

                    var pos = getAbsolutePosition(el,scene2Container);

                    activeBook.position.x = pos.x;
                    activeBook.position.y = pos.y;
                    activeBook.position.z = pos.z;

                    var bookClone = activeBook.clone();
                    bookClone.material = new THREE.MeshLambertMaterial('trancparent: true, opacity: 0');
                    scene2Container.add(bookClone);
                    bookClone.lookAt(PControl.getObject().position);

                    scene2Container.add(activeBook);

                    //objectFlyToMe(activeBook,el.data,PControl.getObject().position,pos,activeBook.rotation,bookClone.rotation);
                    //stepBackStart(200);
                    openBook(el.data);

                    scene2Container.remove(bookClone);
                    bookClone = false;
                    /*
                     var rbook = createReadableBook(el,2);
                     scene2Container.add(rbook);
                     PControl.getObject().lookAt(rbook.position);
                     */
                }
            }else
            if((!bookReadingMode)&&(inRoom)&&(event.which == 3)) {
                if (!activeFurnitureClone) {
                    var el = chechForObjectAtMouse('CanSelectObject', scene2Container);
                    if (el) {
                        activeFurniture = el;
                        activeFurnitureClone = activeFurniture.clone();
                        activeFurnitureClone.data = activeFurniture.data;
                        activeFurnitureClone.movingAxis = activeFurniture.movingAxis;
                        activeFurnitureClone.positionLimits = activeFurniture.positionLimits;
                        groupOpacity(activeFurnitureClone,0.4);
                        el.parent.add(activeFurnitureClone);
                        activeFurniture.visible = false;
                        roomFurnitureMoving = true;
                        bookMoving = true;

                        var controlBody = movingBookText + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                        $('#controlImage').attr('style', 'display: none;');
                        $('#controlName').attr('style', 'display: none;');
                        savedControlBody = $('#controlBody').html();
                        $('#controlBody').html(controlBody);
                        $('#controlPanel').attr('style','z-index: 2;');
                        controlDialogShow  = true;
                    } else {
                        var el = chechForObjectAtMouse('roomFurniture', scene2Container);
                        if (el) {
                            activeFurniture = el;
                            activeFurnitureClone = activeFurniture.clone();
                            activeFurnitureClone.movingAxis = activeFurniture.movingAxis;
                            activeFurnitureClone.positionLimits = activeFurniture.positionLimits;
                            groupOpacity(activeFurnitureClone, 0.4);
                            scene2Container.add(activeFurnitureClone);
                            activeFurniture.visible = false;
                            roomFurnitureMoving = true;

                            var controlBody = movingFurnitureText + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                            $('#controlImage').attr('style', 'display: none;');
                            $('#controlName').attr('style', 'display: none;');
                            savedControlBody = $('#controlBody').html();
                            $('#controlBody').html(controlBody);
                            $('#controlPanel').attr('style', 'z-index: 2;');
                            controlDialogShow = true;
                        }
                    }
                } else {
                    activeFurniture.position.x = activeFurnitureClone.position.x;
                    activeFurniture.position.y = activeFurnitureClone.position.y;
                    activeFurniture.position.z = activeFurnitureClone.position.z;
                    if(bookMoving){
                        $.ajax({
                            type: "POST",
                            url: "/ajax/book_position.php",
                            data: 'act=update&bid=' + activeFurniture.data.user_book_id + '&x='+activeFurnitureClone.position.x+ '&y='+activeFurnitureClone.position.y+ '&z='+activeFurnitureClone.position.z,
                            success: function(html){

                            }
                        });
                    }
                    activeFurniture.visible = true;
                    groupOpacity(activeFurniture,1);
                    activeFurnitureClone.parent.remove(activeFurnitureClone);
                    activeFurnitureClone = false;
                    roomFurnitureMoving = false;

                    $('#controlImage').removeAttr('style');
                    $('#controlName').removeAttr('style');
                    $('#controlBody').html(savedControlBody);
                    $('#controlPanel').attr('style','z-index: 2;');
                    controlDialogShow  = true;
                    bookMoving = false;
                }
            }
        };

        var checkForUserBox = function(item) {

            if (item.name.indexOf('Box') > -1) {
                pos0.x = camera.position.x;
                pos0.y = camera.position.y;
                pos0.z = camera.position.z;

                pos1.x = item.position.x;
                pos1.y = item.position.y;
                pos1.z = item.position.z;

                profileFound = true;
                var uId = item.name.replace(/[^\d]/gi, '');
                currentUserId = uId;
                $('#coubId').val(uId);

                cameraRotationSpeed = 0;
                controls.enabled = false;


                var a = camera.position;
                retCoords = a;
                var b = item.position;

                //controls.target = new THREE.Vector3(b.x, b.y, b.z);

                //Количество шагов
                var steps = 200;

                //Один шаг
                var delta = [];
                delta.x = (a.x - b.x) / steps;
                delta.y = (a.y - b.y) / steps;
                delta.z = (a.z - b.z) / steps;

                //controls.target = new THREE.Vector3(b.x, b.y, b.z);

                canLoadProfiles = false;

                controlsCenter(item.position);
                jumpToSphere(delta, steps, b, item);

                roomTextures.floor = 'images/textures/room/pol.jpg';
                roomTextures.ceil = 'images/textures/room/beton.jpg';
                roomTextures.left = 'images/textures/room/kirpich.jpg';
                roomTextures.right = 'images/textures/room/kirpich.jpg';

                $.ajax({
                    type: "POST",
                    url: "/index.php",
                    data: {request: 'room_textures',uid: uId},
                    dataType: "json",
                    success: function (json) {
                        $.each(json, function (i, v) {
                            var user_folder = 'userphoto/'+ v.user_id +'/userroom/';

                            if(v.user_room_flooring) { roomTextures.floor = user_folder + v.user_room_flooring; }
                            if(v.user_room_ceiling) { roomTextures.ceil = user_folder + v.user_room_ceiling; }
                            if(v.user_room_wall_left) { roomTextures.right = user_folder + v.user_room_wall_left; }
                            if(v.user_room_wall_right) { roomTextures.left = user_folder + v.user_room_wall_right; }

                        });
                    }
                });

                return false;
            }
        }

        var groupOpacity = function(group,opacity) {
            if(group.material) {
                group.material.transparent = true;
                group.material.opacity = opacity;
                if(group.material.materials) {
                    group.material.materials.forEach(function(e){
                        e.transparent = true;
                        e.opacity = opacity;
                    });
                }
            }
            group.children.forEach(function(e){
                groupOpacity(e,opacity);
            });
        }

        var objectFlyToMe = function(obj,data,targetPosition,objPos,rotation0,rotation1,steps,deltaPos,deltaRot,step) {
            objPos = objPos || obj.position;
            steps = steps || 100;
            deltaPos = deltaPos || false;
            step = step || 0;

            if (!deltaPos) {
                //Один шаг
                var a = objPos;
                var b = targetPosition;

                var deltaPos = [];
                deltaPos.x = (a.x - b.x) / steps;
                deltaPos.y = (a.y - b.y) / steps;
                deltaPos.z = (a.z - b.z) / steps;

                //Шаг поворота
                var a = rotation1;
                var b = rotation0;

                var deltaRot = [];
                deltaRot.x = (a.x - b.x) / steps;
                deltaRot.y = (a.y - b.y) / steps;
                deltaRot.z = (a.z - b.z) / steps;
            }

            if (steps>step) {

                step++;

                var newpos = [];
                newpos.x = obj.position.x * 1 - (deltaPos.x) * 1;
                newpos.y = obj.position.y * 1 - (deltaPos.y) * 1;
                newpos.z = obj.position.z * 1 - (deltaPos.z) * 1;
                obj.position.x = newpos.x;
                obj.position.y = newpos.y;
                obj.position.z = newpos.z;

                obj.rotation.x += deltaRot.x;
                obj.rotation.y += deltaRot.y;
                obj.rotation.z += deltaRot.z;


                setTimeout(function () {
                    objectFlyToMe(obj,data,targetPosition,objPos,rotation0,rotation1,steps,deltaPos,deltaRot,step);
                }, 10);
            } else {
                openBook(data);
                obj.lookAt(PControl.getObject().position);
            }

        }

        var openBook = function(data) {
            document.exitPointerLock = document.exitPointerLock    ||
                document.mozExitPointerLock ||
                document.webkitExitPointerLock;
            document.exitPointerLock();

            if(data.user_book_id!=-1) {

            }

            var controlBody = exitBookReadingModeText + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
            if(data.user_book_id!=-1) {
                var bookSRC = 'http://' + window.location.host + '/userphoto/' + data.user_id + '/books/' + data.user_book_photo;
                var bookTexts = data.user_book_author + ' - ' + data.user_book_discript;
                var frame_src = 'http://'+window.location.host+'/userphoto/'+data.user_id+'/books/'+data.user_book_link;
            } else {
                var bookSRC = '/images/textures/book/takein_big.png';
                var bookTexts = 'Taken posts';
                var frame_src = '/ajax/takein.php?uid='+currentUserId;
            }
            $('#controlImage').attr('src', bookSRC);
            $('#controlName').html(bookTexts);
            savedControlBody = $('#controlBody').html();
            $('#controlBody').html(controlBody);
            $('#controlPanel').attr('style', 'z-index: 2;');
            controlDialogShow = true;

            $('body').append('<div id="book_frame" style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,.7);z-index:1;opacity:0;">' +
                '<div id="bookframesrc" style="position:relative;width:65%;height: 80%; margin:80px auto;background: #fff;border-radius: 15px;box-shadow: 0 0 25px #fff;">' +
                '<iframe width="100%" height="100%" src="'+frame_src+'" style="border:1px #bbbbbb solid;border-radius: 15px;"></iframe>' +
                '</div>'+
                '</div>');
            $('#book_frame').animate({opacity:1},500);
        }

        var stepBackStart = function(t) {
            moveBackward = true;
            setTimeout(function() {stepBackStop();},t);
        }

        var stepBackStop = function() {
            moveBackward = false;
            bookReadingMode = true;
            PControl.enabled = false;
        }

        var jumpToSphere = function (delta,i,finish,e,room) {
            room = room || true;
            if($('#user_profile_view')){$('#user_profile_view').remove();}
            $('#custom_cursor').attr('style','display:none;')
            i--;
            if(i>0){
                var position = [];
                position.x = camera.position.x - (delta.x);
                position.y = camera.position.y - (delta.y);
                position.z = camera.position.z - (delta.z);
                camera.position.x = position.x;
                camera.position.y = position.y;
                camera.position.z = position.z;
                setTimeout(function( ){jumpToSphere(delta,i,finish,e,room);},7);
            }else{
                // Создание комнаты
                if(room!='no') {createRoom(e);}
                else {canLoadProfiles = true;}
            }
        }

        var createRoom = function(userProfile, show) {
            show = show || false;

            $('canvas').removeAttr('style');

            scene2Container = new THREE.Object3D();

            renderer.setClearColor( 0x2080ca );
            scene.remove(scene1Container);
            $('#zoom_control').attr('style','display: none !important;');
            scene.add(scene2Container);

            prevTime = performance.now();
            hideInvisibleProfiles(-1);
            canLoadProfiles = true;

            var left = THREE.ImageUtils.loadTexture(roomTextures.left);
            var right = THREE.ImageUtils.loadTexture(roomTextures.right);
            var beton = THREE.ImageUtils.loadTexture(roomTextures.ceil);
            var pol = THREE.ImageUtils.loadTexture(roomTextures.floor);

            right.minFilter = left.minFilter = beton.minFilter = pol.minFilter = THREE.LinearFilter;

            var urls = [
                'images/textures/room/kirpich_.jpg', 'images/textures/room/kirpich_.jpg',
                'images/textures/room/beton_.jpg', 'images/textures/room/pol_.jpg',
                'images/textures/room/outwindow.jpg', 'images/textures/room/outwindow.jpg',
            ];

            cubeCamera = new THREE.CubeCamera(1, 1000, 512);
            cubeCamera.renderTarget.minFilter = THREE.LinearMipMapLinearFilter;
            cubeCamera.rotation.x = degToRad(180);
            cubeCamera.rotation.y = degToRad(180);
            scene2Container.add(cubeCamera);
            var glass = new THREE.MeshPhongMaterial({
                ambient: 0xc6ffe8,
                reflectivity: 1,
                color: 0xc6ffe8,
                specular: 0xa9e4ff,
                shininess: 100,
                shading: THREE.FlatShading, //THREE.FlatShading,
                blending: THREE.NormalBlending,
                side: THREE.DoubleSide,
                envMap: cubeCamera.renderTarget,
                reflectivity: 1,
                wireframe: false,
                transparent: true,
                opacity: 0.03
            });

            var materials = [
                new THREE.MeshLambertMaterial({
                    map: left, side: THREE.BackSide
                }),
                new THREE.MeshLambertMaterial({
                    map: right, side: THREE.BackSide
                }),
                new THREE.MeshLambertMaterial({
                    map: beton, side: THREE.BackSide
                }),
                new THREE.MeshLambertMaterial({
                    map: pol, side: THREE.BackSide
                }),
                glass,
                glass
            ];

            var roomMaterial = new THREE.MeshFaceMaterial( materials ) ;

            var roomGeometry = new THREE.BoxGeometry(28, 8, 25);
            var room = new THREE.Mesh(roomGeometry, roomMaterial);
            room.position.x = 0;
            room.position.y = 0;
            room.position.z = 0;

            scene2Container.add(room);

            // Солнце
            var textureFlare0 = THREE.ImageUtils.loadTexture( "/images/textures/room/lensflare/lensflare0.png" );
            var textureFlare2 = THREE.ImageUtils.loadTexture( "/images/textures/room/lensflare/lensflare2.png" );
            var textureFlare3 = THREE.ImageUtils.loadTexture( "/images/textures/room/lensflare/lensflare3.png" );
            var l1 = addLight( 0.55, 0.9, 0.5, 0, 300, -1000 );
            scene2Container.add( l1 );

            function lensFlareUpdateCallback( object ) {

                var f, fl = object.lensFlares.length;
                var flare;
                var vecX = -object.positionScreen.x * 2;
                var vecY = -object.positionScreen.y * 2;


                for( f = 0; f < fl; f++ ) {

                    flare = object.lensFlares[ f ];

                    flare.x = object.positionScreen.x + vecX * flare.distance;
                    flare.y = object.positionScreen.y + vecY * flare.distance;

                    flare.rotation = 0;

                }

                object.lensFlares[ 2 ].y += 0.025;
                object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );

            }

            function addLight( h, s, l, x, y, z ) {

                var light = new THREE.PointLight( 0xffffff, 1, 2000 );
                light.color.setHSL( h, s, l );
                light.position.set( x, y, z );
                scene2Container.add( light );

                var flareColor = new THREE.Color( 0xffffff );
                flareColor.setHSL( h, s, l + 0.5 );

                var lensFlare = new THREE.LensFlare( textureFlare0, 800, 0.0, THREE.AdditiveBlending, flareColor );

                lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
                lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
                lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );

                lensFlare.add( textureFlare3, 60, 0.6, THREE.AdditiveBlending );
                lensFlare.add( textureFlare3, 70, 0.7, THREE.AdditiveBlending );
                lensFlare.add( textureFlare3, 120, 0.9, THREE.AdditiveBlending );
                lensFlare.add( textureFlare3, 70, 1.0, THREE.AdditiveBlending );

                lensFlare.customUpdateCallback = lensFlareUpdateCallback;
                lensFlare.position.copy( light.position );

                return lensFlare;

            }

            //Pesok
            /*
            var groundTexture = THREE.ImageUtils.loadTexture( '/images/textures/room/pesok.png' );
            groundTexture.minFilter = THREE.LinearFilter;

            groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
            groundTexture.repeat.set(1, 1 );
            groundTexture.anisotropy = 5;

            var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: groundTexture } );

            var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 100, 60 ), groundMaterial );
            mesh.position.y = -4.1;
            mesh.position.z = -23;
            mesh.rotation.x = - Math.PI / 2;
            mesh.rotation.z = degToRad(15);
            mesh.receiveShadow = true;
            scene2Container.add( mesh );
            */
            //____________________________________
            //Trava
            var groundTexture = THREE.ImageUtils.loadTexture( 'images/textures/room/trava.jpg' );
            groundTexture.minFilter = THREE.LinearFilter;

            groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
            groundTexture.repeat.set(1500, 1500 );
            groundTexture.anisotropy = 16;

            var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: groundTexture } );

            var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
            mesh.position.y = -4.1;
            mesh.position.z = 10000;
            mesh.rotation.x = - Math.PI / 2;
            mesh.receiveShadow = true;
            scene2Container.add( mesh );
            //____________________________________

            // Океан
            var parameters = {
                width: 2000,
                height: 2000,
                widthSegments: 250,
                heightSegments: 250,
                depth: 1500,
                param: 4,
                filterparam: 1
            };

            waterNormals = new THREE.ImageUtils.loadTexture( '/images/textures/room/waternormals.jpg' );
            waterNormals.wrapS = waterNormals.wrapT = THREE.RepeatWrapping;

            water = new THREE.Water( renderer, camera, scene, {
                textureWidth: 512,
                textureHeight: 512,
                waterNormals: waterNormals,
                alpha: 	1.0,
                sunDirection: light.position.clone().normalize(),
                sunColor: 0xffffff,
                waterColor: 0x001e0f,
                distortionScale: 50.0,
            } );


            mirrorMesh = new THREE.Mesh(
                new THREE.PlaneBufferGeometry( parameters.width * 500, parameters.height * 500 ),
                water.material
            );

            mirrorMesh.add( water );
            mirrorMesh.rotation.x = - Math.PI * 0.5;
            mirrorMesh.position.y = -5;
            scene2Container.add( mirrorMesh );

            scene2Container.add( new THREE.AmbientLight({color: 0xf2ff85}) );

            //Photo
            var uId = userRoomId = userProfile.name.replace(/[^\d]/gi, '');
            var photo = 'images/no_photo.png';

            var name = '';
            $.each(users, function (i, v) {
                if ((v.user_photo != '')&&(uId==v.user_id)) {
                    photo = 'userphoto/' + v.user_id + '/' + v.user_photo;
                }
                if(uId==v.user_id){
                    name = v.user_surname+' '+ v.user_name;
                }
            });

            if (uId>99999) {
                var photo = tempPhotos[getRandomInt(0,tempPhotosCount)];
            }

            if((myid!=userRoomId)&&(myid!=-1)) {
                //1 и 3 если в друзьях то и подписан. Предложение: 1) удалить из друзей и отписатся
                //2 если только подписан. Предложение: 1) Добавить в друзья; 2) Отписатся
                //пусто если не в друзьях и не подписан. Предложение: 1) Добавить в друзья; 2) Подписатся

                var controlBody = toFriendText + toReadText;

                $.each(conectionIds[myid], function (i, v) {
                    if (i == uId) {
                        if ((v == 1) || (v == 3)) {
                            controlBody = fromFriendText;
                        } else if (v == 2) {
                            controlBody = toFriendText + fromReadText;
                        }
                    }
                });
                controlBody = controlBody + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';


                $('#controlImage').attr('src', photo);
                $('#controlName').html(name);
                $('#controlBody').html(controlBody);
                $('#controlPanel').removeAttr('style');
                controlDialogShow  =true;
            }else
            if(myid!=-1){
                controlBody = toNewsLinkText + '<li class="divider_vertical"></li><li class="divider_vertical"></li>';
                $('#controlImage').attr('src', photo);
                $('#controlName').html(name);
                $('#controlBody').html(controlBody);
                $('#controlPanel').removeAttr('style');
                controlDialogShow  =true;
            }else{
                $('#controlPanel').attr('style','display:none !important;');
                controlDialogShow  = false;
            }

            ///* фото на стене
            var texture = THREE.ImageUtils.loadTexture( photo );
            texture.minFilter = THREE.LinearFilter;
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(1, 1 );
            texture.anisotropy = 16;

            var material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: texture } );

            var plane = new THREE.Mesh( new THREE.PlaneBufferGeometry( 3, 3 ), material );
            plane.position.x=-13.99;
            plane.position.y=1;
            plane.position.z=-7;
            plane.rotation.y =  Math.PI / 2;
            plane.name = 'Photo '+uId;
            scene2Container.add( plane );
            //*/
            //______________________________________

            //Стол
            desk = createDesk(2,5,2);
            desk.position.y = -5;
            desk.position.x = -13;

            //Порт
            var portGeometry = new THREE.CylinderGeometry( 0.5, 0, 1.5, 32 );
            var portMaterial = new THREE.MeshBasicMaterial({color: 0xFFF900, transparent: true, opacity: 0.7});
            var port = new THREE.Mesh(portGeometry,portMaterial);
            port.name = 'exitPort';
            port.position.set(12.5,-1,11);
            scene2Container.add(port);
            //

            var objectLoader = new THREE.ObjectLoader();

            /*
            objectLoader.load("/models_3d/furniture/all/door/door.json", function ( obj ) {
                obj.position.x = 10;
                obj.position.y = -4;
                obj.position.z = -7;


                obj.children.forEach(function(e){
                    e.scale.x = 0.5;
                    e.scale.y = 0.5;
                    e.scale.z = 0.5;
                });

                scene2Container.add( obj );
            });

            // Baba
            objectLoader.load("/models_3d/furniture/people/naked_women/elexis-mdb.json", function ( obj ) {
                obj.position.x = 10;
                obj.position.y = -4;
                obj.position.z = -7;

                 obj.children.forEach(function(e){
                 e.scale.x = 0.063;
                 e.scale.y = 0.063;
                 e.scale.z = 0.063;
                 });
                scene2Container.add( obj );
                var x111 = 10;
                while(x111>-10) {
                    x111-=3;
                    var newb=obj.clone();
                    newb.position.x = x111;
                    scene2Container.add( newb );
                }
            });


            objectLoader.load("/models_3d/furniture/all/tim-gra200-grace-two-seater/bu-tim-gra200-grace-two-seater.json", function ( obj ) {
                obj.position.x = 12.5;
                obj.position.y = -4;
                obj.position.z = 5;
                obj.rotation.y = degToRad(90);

                obj.children.forEach(function(e){
                    e.scale.x = 3;
                    e.scale.y = 3;
                    e.scale.z = 3;

                });
                obj.name = 'roomFurniture';
                obj.movingAxis = ['x','z'];
                obj.positionLimits = [];
                obj.positionLimits.max = [12.5,null,10];
                obj.positionLimits.min = [-12.4,null,-10.2];
                scene2Container.add( obj );
            });

            objectLoader.load("/models_3d/furniture/all/table-and-chairs/table-and-chairs.json", function ( obj ) {
                obj.position.x = 0;
                obj.position.y = -4;
                obj.position.z = -7;
                obj.rotation.y = degToRad(90);

                obj.children.forEach(function(e){
                    e.scale.x = 3;
                    e.scale.y = 3;
                    e.scale.z = 3;

                    if((e.name == '3dsmesh-5-5')||(e.name == "3dsmesh-3-3")||(e.name == "3dsmesh-4-4")||(e.name == "lightpreset")||(e.name == "3dsmesh-1-1")) {
                        e.visible = false;
                    } else
                    if ((e.name!='lightpreset')&&(e.name!='camerapreset')) {

                        e.material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: THREE.ImageUtils.loadTexture( '/models_3d/furniture/all/table-and-chairs/texture.png' ) } );
                    }

                });
                obj.name = 'roomFurniture';
                obj.movingAxis = ['x','z'];
                obj.positionLimits = [];
                obj.positionLimits.max = [10.2,null,10.2];
                obj.positionLimits.min = [-11.1,null,-7.6];
                scene2Container.add( obj );
            });
             */

            objectLoader.load("/models_3d/technic/tvset/tvset.json", function ( obj ) {

                obj.position.x = -13.8;
                obj.position.y = -2;
                obj.position.z = 4;
                obj.rotation.y = degToRad(-90);
                obj.children[5].visible = false;

                scene2Container.add( obj );

                video = document.createElement( 'video' );
                // video.id = 'video';
                video.width = 640;
                video.height = 360;
                video.type = ' video/mp4; codecs="avc1.42E01E, mp4a.40.2"';
                video.src = "video/demo.mp4";
                video.load(); // must call after setting/changing source
                //video.play();

                videoImage = document.createElement( 'canvas' );
                videoImage.width = 640;
                videoImage.height = 360;

                videoImageContext = videoImage.getContext( '2d' );
                // background color if no video present
                videoImageContext.fillStyle = '#000000';
                videoImageContext.fillRect( 0, 0, videoImage.width, videoImage.height );

                videoTexture = new THREE.Texture( videoImage );
                videoTexture.minFilter = THREE.LinearFilter;
                videoTexture.magFilter = THREE.LinearFilter;

                var movieMaterial = new THREE.MeshBasicMaterial( { map: videoTexture, overdraw: true, side:THREE.DoubleSide } );
                // the geometry on which the movie will be displayed;
                // 		movie image will be scaled to fit these dimensions.
                var movieGeometry = new THREE.PlaneGeometry( 5.7, 3.2, 4, 4 );
                var movieScreen = new THREE.Mesh( movieGeometry, movieMaterial );
                movieScreen.position.set(0,50,0);
                movieScreen.position.x = -13.739;
                movieScreen.position.y = 0.1;
                movieScreen.position.z = 4;
                movieScreen.rotation.y = degToRad(90);
                movieScreen.name = 'tvset';
                scene2Container.add(movieScreen);
                canPlayVideo = true;

            });

            var bookShelf = false;
            var loader = new THREE.ColladaLoader();
            loader.load(
                // resource URL
                '/models_3d/furniture/all/shelf/shelf.dae',
                // Function when resource is loaded
                function ( collada ) {

                    var colscene = collada.scene;

                    colscene.name = 'roomFurniture';
                    colscene.movingAxis = ['z'];
                    colscene.positionLimits = [];
                    colscene.positionLimits.max = [null,null,6];
                    colscene.positionLimits.min = [null,null,-12.4];
                    scene2Container.add(colscene);
                    bookShelf = colscene.children[1].children[2];

                    /*
                     data: {request:'user_books',uid:uId},
                     dataType: "json",
                     */
                    var px = 0.34 - 0.05;
                    var rx = degToRad(90);
                    var ry = degToRad(-90);
                    var rz = degToRad(90);
                    var py = 1.5;
                    var pz = 1.22;
                    var last_book = false;
                    $.ajax({
                        type: "POST",
                        url: "/ajax/book_position.php",
                        data: 'act=get&uid='+uId,
                        dataType: "json",
                        success: function (json) {

                            //var book = createBook(desk,2);

                            $.each(json, function (i, v) {
                                if(!allBooks[uId]) {
                                    allBooks[uId] = [];
                                }

                                allBooks[uId][allBooks[uId].length] = v;

                                var b = createBook(desk,2);

                                px+=0.05;
                                if((v.x!='unset')&&(v.y!='unset')&&(v.z!='unset')) {
                                    b.position.x = v.x;
                                    b.position.y = v.y;
                                    b.position.z = v.z;
                                } else {
                                    b.position.x = px;
                                    b.position.y = py;
                                    b.position.z = pz;
                                }
                                b.rotation.x = rx;
                                b.rotation.y = ry;
                                b.rotation.z = rz;

                                b.data = v;
                                last_book = b;

                                b.movingAxis = ['x','y','z'];
                                b.positionLimits = [];
                                b.positionLimits.max = [0,0,0];
                                b.positionLimits.min = [0,0,0];

                                bookShelf.add(b);
                            });
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/ajax/book_position.php",
                        data: 'act=get&uid='+uId+'&bid=-1',
                        dataType: "json",
                        success: function (json) {
                            if(!allBooks[uId]) {
                                allBooks[uId] = [];
                            }

                            json = json[0] || json;

                            allBooks[uId][allBooks[uId].length] = json;

                            var b = createBook(desk,2,true);

                            px+=0.05;
                            if((json.x!='unset')&&(json.y!='unset')&&(json.z!='unset')) {
                                b.position.x = json.x;
                                b.position.y = json.y;
                                b.position.z = json.z;
                            } else {
                                b.position.x = px;
                                b.position.y = py;
                                b.position.z = pz;
                            }
                            b.rotation.x = rx;
                            b.rotation.y = ry;
                            b.rotation.z = rz;

                            b.data = json;
                            last_book = b;

                            b.movingAxis = ['x','y','z'];
                            b.positionLimits = [];
                            b.positionLimits.max = [0,0,0];
                            b.positionLimits.min = [0,0,0];

                            bookShelf.add(b);
                        }
                    });

                    var light_1 = new THREE.PointLight(0xffffff, 0.7, 3, 0.5);
                    light_1.position.set(-0.91,0.7,0.1);

                    light_2 = light_1.clone();
                    light_2.position.x = -0.03;

                    light_3 = light_1.clone();
                    light_3.position.x = 0.89;

                    bookShelf.add(light_1);
                    bookShelf.add(light_2);
                    bookShelf.add(light_3);



                    colscene.children[1].children[0].visible = false;
                    colscene.children.forEach(function(e){
                        e.scale.x = 2.5;
                        e.scale.y = 2.5;
                        e.scale.z = 2.5;
                    });
                    colscene.rotation.y = degToRad(-90);
                    //colscene.rotation.z = degToRad(-90);
                    colscene.position.x = 13.5;
                    colscene.position.y = -4;
                    colscene.position.z = -5;

                },
                // Function called when download progresses
                function ( xhr ) {
                }
            );

            //setCookie('backX',camera.position.x);
            //setCookie('backY', camera.position.y);
            //setCookie('backZ', camera.position.z);
            //scene.fog = new THREE.Fog( 0xcce0ff, 40, 140 );

            //camera.up.set(0,0,0);
            camera.position.x =0;
            camera.position.y =0;
            camera.position.z =0;

            /*// Зеркала
            var WIDTH = window.innerWidth;
            var HEIGHT = window.innerHeight;

            var planeGeo = new THREE.PlaneBufferGeometry( 28, 8 );

            // MIRROR planes
            groundMirror = new THREE.Mirror( renderer, camera, { clipBias: 0.003, textureWidth: WIDTH, textureHeight: HEIGHT, color: 0x889999, side: THREE.DoubleSide, transparent: true, opacity: 0.2 } );
            var mirrorMesh = new THREE.Mesh( planeGeo, groundMirror.material );
            mirrorMesh.add( groundMirror );
            mirrorMesh.position.x=0;
            mirrorMesh.position.y=0;
            mirrorMesh.position.z=-12.1;
            scene2Container.add( mirrorMesh );

            verticalMirror = new THREE.Mirror( renderer, camera, { clipBias: 0.003, textureWidth: WIDTH, textureHeight: HEIGHT, color:0x889999, side: THREE.DoubleSide, transparent: true, opacity: 0.2 } );
            var verticalMirrorMesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 28, 8 ), verticalMirror.material );
            verticalMirrorMesh.add( verticalMirror );
            verticalMirrorMesh.position.z = 12.1;
            verticalMirrorMesh.rotation.y =  Math.PI;
            scene2Container.add( verticalMirrorMesh );
            */

            inRoom = true;

            //animatePort(port);

            var canvas = document.body;
            canvas.requestPointerLock = canvas.requestPointerLock ||
                canvas.mozRequestPointerLock ||
                canvas.webkitRequestPointerLock;

            canvas.onclick = function() {
                if(!bookReadingMode) {
                    canvas.requestPointerLock();
                    moveInRoom = true;
                }

            }

            controls = false;

            PControl = new THREE.PointerLockControls(camera);
            PControl.enabled = true;

            PControl.getObject().rotation.x = 0; // Rotates Yaw Object
            PControl.getObject().rotation.y = 0; // Rotates Yaw Object
            PControl.getObject().rotation.z = 0; // Rotates Yaw Object
            PControl.getObject().children[0].rotation.x = 0; // Rotates the Pitch Object
            PControl.getObject().children[0].rotation.y = 0;
            PControl.getObject().children[0].rotation.z = 0;
            PControl.getObject().lookAt(new THREE.Vector3(0,0,0));

            camera.updateProjectionMatrix();
            scene2Container.add(PControl.getObject());

        }

        var animatePort = function(port,cof,min,max) {
            cof = cof || -1;
            min = min || -2;
            max = max || -1
            var pos = port.position.y;
            pos = pos - (0.01 * cof);
            if ((pos<-2)||(pos>-1)) {
                cof = cof * -1;
            }
            port.position.y = pos;
            if(inRoom) {
                setTimeout(function () {
                    animatePort(port, cof, min, max);
                }, 5);
            }
        }

        var getAbsolutePosition = function(obj,parent0) {
            var pos = [];
            pos.x = 0;
            pos.y = 0;
            pos.z = 0;

            var ebook = obj;

            while ((ebook != parent0)&&(ebook.parent)&&(ebook.position)) {

                pos.x += ebook.position.x;
                pos.y += ebook.position.y;
                pos.z += ebook.position.z;

                ebook = ebook.parent;
            }
            return pos;
        }

        var createBook = function (place,l,takein,width,height,weight) {
            if (place) {
                var cof = 45;
                takein = takein || false;
                width =  width || (0.4 * 12) / cof;
                height = height || (0.5 * 12) / cof;
                weight = weight || (0.1 * 12) / cof;

                var bookGeometry = new THREE.BoxGeometry(width,height,weight);

                if(!takein) {
                    var mapPapers = THREE.ImageUtils.loadTexture('/images/textures/book/1.jpg');
                    var mapFront = THREE.ImageUtils.loadTexture('/images/textures/book/2.jpg');
                    var mapLeft = THREE.ImageUtils.loadTexture('/images/textures/book/3.jpg');
                } else {
                    var mapPapers = THREE.ImageUtils.loadTexture('/images/textures/book/1.jpg');
                    var mapFront = THREE.ImageUtils.loadTexture('/images/textures/book/takein_big.png');
                    var mapLeft = THREE.ImageUtils.loadTexture('/images/textures/book/takein_side.png');
                }

                mapPapers.minFilter = mapFront.minFilter = mapLeft.minFilter = THREE.LinearFilter;

                var color = colors[getRandomInt(0,colors.length-1)];

                var materials = [
                    new THREE.MeshLambertMaterial({
                        color: color
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapFront
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapFront
                    })
                ];

                var bookMaterial = new THREE.MeshFaceMaterial( materials ) ;
                var book = new THREE.Mesh(bookGeometry,bookMaterial);
                book.name = 'CanSelectObject';

                book.rotation.x = degToRad(90);
                book.rotation.y = degToRad(180);
                book.rotation.z = degToRad(35);

                book.position.y = l * 1.5 + weight;
                book.position.x = 1;
                book.position.z = 3.5;

                return book;
            }
        }

        var createReadableBook = function (obj,l) {
            var width = obj.geometry.parameters.width;
            var height = obj.geometry.parameters.height / 2;
            var depth = obj.geometry.parameters.depth;
            if (obj) {

                var bookGeometry = new THREE.BoxGeometry(width,height,depth);

                var mapPapers = THREE.ImageUtils.loadTexture('images/textures/book/1.jpg');
                var mapFront = THREE.ImageUtils.loadTexture('images/textures/book/2.jpg');
                var mapLeft = THREE.ImageUtils.loadTexture('images/textures/book/3.jpg');
                var mapPaper = THREE.ImageUtils.loadTexture('images/textures/book/4.jpg');

                mapPapers.minFilter = mapFront.minFilter = mapLeft.minFilter = mapPaper.minFilter = THREE.LinearFilter;

                var materialsDown = [
                    new THREE.MeshLambertMaterial({
                        map: mapLeft
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPaper
                    }),
                    new THREE.MeshLambertMaterial({
                        color: 0x000000
                    })
                ];

                var materialsUp = [
                    new THREE.MeshLambertMaterial({
                        map: mapLeft
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPapers
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapFront
                    }),
                    new THREE.MeshLambertMaterial({
                        map: mapPaper
                    })
                ];

                var allBook = new THREE.Object3D();

                var bookMaterialUp = new THREE.MeshFaceMaterial( materialsUp ) ;
                var bookMaterialDown = new THREE.MeshFaceMaterial( materialsDown ) ;

                var bookUp = new THREE.Mesh(bookGeometry,bookMaterialUp);
                var bookDown = new THREE.Mesh(bookGeometry,bookMaterialDown);

                allBook.add(bookUp);
                allBook.add(bookDown);

                allBook.name = 'CanSelectObject';

                allBook.rotation.x = degToRad(90);
                allBook.rotation.y = degToRad(180);
                allBook.rotation.z = degToRad(35);

                allBook.position.y = l * 1.5 + depth + 1;
                allBook.position.x = 1;
                allBook.position.z = 3.5;

                return allBook;
            }
        }

        var createDesk = function (w,h,l,headColor,legColor) {
            headColor = headColor || 0xffffff;
            legColor = legColor || 0xffffff;
            if((w>0)&&(h>0)&&(l>0)) {
                var nogi = [];
                var stepW = w / 20;
                var stepH = h / 20;
                nogi[1] = [];
                nogi[1].x = stepW;
                nogi[1].z = stepH;
                nogi[2] = [];
                nogi[2].x = stepW;
                nogi[2].z = h - stepH;
                nogi[3] = [];
                nogi[3].x = w - stepW;
                nogi[3].z = h - stepH;
                nogi[4] = [];
                nogi[4].x = w - stepW;
                nogi[4].z = stepH;
            }
            var CR = 0;
            if(stepH<stepW){CR = stepH/2;}else{CR = stepW/2;}
            var legGeometry = new THREE.CylinderGeometry(CR,CR,l);
            var headGeometry = new THREE.BoxGeometry(w,h,((stepH+stepW)/4));
            var legMaterial = new THREE.MeshLambertMaterial({color: legColor});
            var headMaterial = new THREE.MeshLambertMaterial({color: headColor});
            nogi[1].e = new THREE.Mesh(legGeometry,legMaterial);
            nogi[1].e.position.set(nogi[1].x,l,nogi[1].z);
            nogi[2].e = new THREE.Mesh(legGeometry,legMaterial);
            nogi[2].e.position.set(nogi[2].x,l,nogi[2].z);
            nogi[3].e = new THREE.Mesh(legGeometry,legMaterial);
            nogi[3].e.position.set(nogi[3].x,l,nogi[3].z);
            nogi[4].e = new THREE.Mesh(legGeometry,legMaterial);
            nogi[4].e.position.set(nogi[4].x,l,nogi[4].z);
            var head = new THREE.Mesh(headGeometry,headMaterial);
            head.rotation.x = degToRad(90);
            head.position.y = l * 1.5;
            head.position.x = w/2;
            head.position.z = h/2;
            var desk = new THREE.Object3D();
            desk.name = 'Desk';
            desk.add(nogi[1].e);
            desk.add(nogi[2].e);
            desk.add(nogi[3].e);
            desk.add(nogi[4].e);
            desk.add(head);
            return desk;
        }

        var onDocumentMouseUp = function ( event ) {
            dispatch( events.mouseup, event );
            if((!inRoom)&&(controls)) {
                controls.rotateCamera();
            }

            mouseDown = false;
        };

        var chechForObjectAtMouse = function(searchName,paretnElement) {

            var mouse = new THREE.Vector2();

            mouse.x = 0;
            mouse.y = 0;

            var rayCaster = new THREE.Raycaster();

            var cam = PControl.getObject().children[0].children[0].clone();

            var pos = PControl.getObject().position;
            cam.position.x = pos.x;
            cam.position.y = pos.y;
            cam.position.z = pos.z;

            var el_found = false;

            rayCaster.setFromCamera(mouse, cam);
            var intersectedObjects = rayCaster.intersectObjects(paretnElement.children, true);
            intersectedObjects.forEach(function (item) {
                if (item.object.name.indexOf(searchName) > -1) {
                    el_found = item.object;
                }
            });

            if(!el_found) {
                var el = false;
                intersectedObjects.forEach(function (item) {
                    el = item.object;
                    while((!el_found) && (el != paretnElement) ) {
                        if (el.parent.name.indexOf(searchName) > -1) {
                            el_found = el.parent;
                            return el_found;
                        } else {
                            el = el.parent;
                        }
                    }
                });
            }

            return el_found;
        }

        var onDocumentMouseMove = function ( event ) {

            dispatch( events.mousemove, event );

            if(customCursor) {
                var pos = $('#custom_cursor').offset();
                var dx = pos.left - event.pageX;
                var dy = pos.top - event.pageY;
                if ((dx>50)||(dx<-50)||(dy>50)||(dy<-50)) {
                    $('#custom_cursor').attr('style',"display:none;");
                    $('canvas').attr('style', "cursor:url('../images/cursor/normal.cur'), default !important;");
                    wasRememberedE =  customCursor = false;
                }
            }

            //Starting_search_of_coube_on_mouse_position__________

            if(inRoom) {
                var el = chechForObjectAtMouse('CanSelectObject',scene2Container);
                if(el==false) {
                    $('#cursorImage').attr('src','images/cursor.png');
                    $('#cursorLabes').attr('style','display:none !important;');
                }else {
                    $('#cursorImage').attr('src','images/cursor_select.png');
                    if(el.data.user_book_id!=-1) {
                        $('#cursorLabes').html(el.data.user_book_author + ' - ' + el.data.user_book_discript);
                    }else {
                        $('#cursorLabes').html('Taken posts');
                    }
                    $('#cursorLabes').removeAttr('style');
                }
            } else {
                var rayCaster = new THREE.Raycaster();
                var mouse = new THREE.Vector2();

                mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
                mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

                rayCaster.setFromCamera(mouse, camera);

                var intersectedObjects = rayCaster.intersectObjects(scene.children, true);

                var ItemName = '';
                if ((selectedUser != -1)&&(spheres[selectedUser])) {

                    var id = spheres[selectedUser].name.replace(/[^\d]/gi, '');

                    spheres[selectedUser].material = selectedMaterial;


                    //changeSphereRadius(spheres[selectedUser],1.5,'-');

                    selectedUser = -1;

                    if (!inRoom) {
                        cameraRotationSpeed = 0.00005;
                    }

                }

                var faceIndex = -1;
                var profileFound = false;
                intersectedObjects.forEach(function (item) {

                    if ((item.faceIndex > -1) && (faceIndex == -1)&&(item.object.name.indexOf('Level') > -1)) {
                        faceIndex = item.faceIndex;
                    }

                    if ((item.object.name.indexOf('Box') > -1) && (!profileFound)) {
                        profileFound = true;

                        if(!customCursor) {
                            $('canvas').attr('style', "cursor:url('../images/cursor/invisible.cur'), default !important;");
                            $('#custom_cursor').attr('style', "position:absolute;width:32px;height:32px;z-index: 100000;cursor:url('../images/cursor/invisible.cur'), default !important;");
                            $('#custom_cursor').css('left', event.pageX - 16);
                            $('#custom_cursor').css('top', event.pageY - 16);
                            wasRememberedE = item.object;
                            customCursor = true;
                        }



                        selectedMaterial = item.object.material;
                        item.object.material = new THREE.MeshPhongMaterial({color: 0x35a2ff});

                        var id = item.object.name.replace(/[^\d]/gi, '');

                        selectedUser = id;

                        cameraRotationSpeed = 0.000001;

                        if(profileSelection){
                            scene1Container.remove(profileSelection);
                            profileSelection = false;
                        }
                        profileFound = true;
                        var uId = id;

                        myBoxPos[0] = item.object.position.x;
                        myBoxPos[1] = item.object.position.y;
                        myBoxPos[2] = item.object.position.z;

                        lastMovedElement = item.object.clone();
                        setTimeout(function(){clearLastMovedElement();},3000);

                        /*
                        profileSelection = new THREE.Object3D();
                        var o = 0.21;
                        for(var i = 0.9;i<2.9;i+=0.4) {
                            o -= 0.01;
                            var sphere = new THREE.Mesh(
                                new THREE.SphereGeometry(2.5,32,32),
                                new THREE.MeshLambertMaterial({color: 0x00c0ff, transparent: true, opacity: o})
                            );
                            sphere.scale.x = i;
                            sphere.scale.y = i;
                            sphere.scale.z = i;

                            sphere.name = 'wave';

                            profileSelection.add(sphere);
                        }

                        profileSelection.position.x = myBoxPos[0];
                        profileSelection.position.y = myBoxPos[1];
                        profileSelection.position.z = myBoxPos[2];

                        //profileSelection.add(new THREE.AreaLight({color: 0xffffff}));

                        scene1Container.add(profileSelection);
                        if(!selectionAnimated) {
                            animateSelection(2);
                            selectionAnimated = true;
                        }
                        */

                        return false;
                    }
                });
                //End_of_searching_____________________________________

            }

        };

        var PointerLock = function() {
            var canvas = document.body;
            if(document.pointerLockElement === canvas ||
                document.mozPointerLockElement === canvas ||
                document.webkitPointerLockElement === canvas) {
                var x = (screen.width / 2).toFixed(0);
                var y = (window.innerHeight / 2).toFixed(0);
                $('#cursor').css('margin-top',y+'px');
                $('#cursor').css('display','block');
                return true;
            } else {
                $('#cursor').css('display','none');
                return false;
            }
        }

        var onDocumentTouchStart = function ( event ) {

            dispatch( events.touchstart, event );

        };

        var onDocumentTouchEnd = function ( event ) {

            dispatch( events.touchend, event );

        };

        var onDocumentTouchMove = function ( event ) {

            dispatch( events.touchmove, event );

        };

        var onDocumentWheel = function ( event ) {

            dispatch( events.wheel, event );

        };

        function checkForObjects ()
        {
            if(canLoadProfiles) {
                var rayCaster = new THREE.Raycaster();
                var mouse = new THREE.Vector2();

                mouse.x = ( (window.innerWidth / 2).toFixed(0) / window.innerWidth ) * 2 - 1;
                mouse.y = -( (window.innerHeight / 2).toFixed(0) / window.innerHeight ) * 2 + 1;

                rayCaster.setFromCamera(mouse, camera);

                var intersectedObjects = rayCaster.intersectObjects(scene1Container.children);

                var ItemName = '';


                intersectedObjects.forEach(function (item) {

                    if (item.object.name) {
                        ItemName = item.object.name;
                    } else {
                        ItemName = '';
                    }

                    var minDistance = 30;
                    var maxDistance = 200;

                    if (ItemName.indexOf('Level') > -1) {
                        var level = item.object.name.replace(/[^\d]/gi, '') ;
                        var levelmin = level * 10;
                        var levelmax = level * 10 + 9;
                        var fIndex = item.faceIndex;

                        if ((item.distance > maxDistance) && (spheresArray[level][fIndex]) && (selectedProf != fIndex)) {
                            scene1Container.remove(spheresArray[level][fIndex]);
                        }
                        hideInvisibleProfiles(fIndex);

                        if(((item.distance<minDistance/2)||(item.distance<0.1))&&(currentshinedLevel!=level)) {
                            currentshinedLevel = level;
                            //if(shinedLevel){scene1Container.remove(shinedLevel);}

                            var mat = new THREE.MeshBasicMaterial( { color: 0x90f1ff, transparent: true, opacity: 0.15} );
                            var geo = item.object.geometry;


                            var shinedLevel = new THREE.Object3D();

                            var border = new THREE.Mesh(geo,mat);
                            border.material.wireframe = true;
                            border.material.opacity = 0.5;
                            border.material.transparent = true;
                            shinedLevel.add(border);

                            /*
                            border.geometry.vertices.forEach(function(e){
                                var point = new THREE.Mesh(new THREE.SphereGeometry(0.5,16,16),new THREE.MeshBasicMaterial({ color: 0x90f1ff, transparent: true, opacity: 0.35, wireframe: true}));
                                point.position.x = e.x;
                                point.position.y = e.y;
                                point.position.z = e.z;
                                shinedLevel.add(point);
                            });
                            */
                            scene1Container.add(shinedLevel);
                            hideShinedLevel(shinedLevel);
                        }

                        //Подгрузка елементов и выгрузка елементов
                        if ((item.distance <= maxDistance) && (item.distance > minDistance)) {

                            if (spheresArray[fIndex][level] === false) {
                                loadAllUsers(fIndex,level);
                            }


                            scene1Container.add(spheresArray[fIndex][level]);
                        }
                    }
                });
            }
        }

        var hideShinedLevel = function(level,o) {
            /*
            var o = shinedLevel.material.opacity;
            if(o>0) {
                o -= 0.01;
                shinedLevel.material.opacity = o;
                setTimeout(function(){hideShinedLevel();},1);
            }else{
                scene1Container.remove(shinedLevel);
                shinedLevel = false;
            }
            */
            var o = o || 0.3;
            if(o>0.000001) {
                o *= 0.99;

                level.children.forEach(function(e){
                    e.material.opacity = o;
                });

                setTimeout(function(){hideShinedLevel(level,o);},1);
            }else{
                level.children.forEach(function(e){
                    level.remove(e);
                    e=false;
                });
                scene1Container.remove(level);
                level = false;
            }
        }

        var hideInvisibleProfiles = function(faceIndex) {
                for (var i = 0; i < icosaednrons[0].geometry.faces.length; i++) {
                    if ((i != faceIndex) && (i != selectedProf)) {
                        for (z = 0; z < icosaednrons.length; z++) {
                            if (spheresArray[i][z]) {
                                scene1Container.remove(spheresArray[i][z]);
                            }
                        }
                    }
                }
        }

        var hidePyramidicPart = function() {
            scene1Container.remove(line);
            line = false;
        }

        var degToRad = function (deg) {
            return deg / 180 * Math.PI;
        }

        var getCookie = function(name) {
            var cookie = " " + document.cookie;
            var search = " " + name + "=";
            var setStr = null;
            var offset = 0;
            var end = 0;
            if (cookie.length > 0) {
                offset = cookie.indexOf(search);
                if (offset != -1) {
                    offset += search.length;
                    end = cookie.indexOf(";", offset)
                    if (end == -1) {
                        end = cookie.length;
                    }
                    setStr = unescape(cookie.substring(offset, end));
                }
            }
            return(setStr);
        }

        var setCookie = function (name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;
        }

        var professionsPresentation = function() {
            var profLength = icosaednrons[0].geometry.faces.length-2;
            var profIndex = getRandomInt(1,profLength);
            var line = selectProffesion(profIndex);
            showLinePresentation(line);
        }
        var showLinePresentation = function(line,o) {
            o = o || 0;
            o+=0.005;
            groupOpacity(line,o);
            if (o<0.2) {
                setTimeout(function(){showLinePresentation(line,o);},50);
            } else {
                setTimeout(function(){hideLinePresentation(line,o);},3500);
            }
        }
        var hideLinePresentation = function(line,o) {
            o-=0.005;
            if (o>0) {
                groupOpacity(line,o);
                setTimeout(function(){hideLinePresentation(line,o);},50);
            } else {
                setTimeout(function(){professionsPresentation();},1);
            }
        }

    }

};

function Rotation(camera,scene){
    var x = camera.position.x,
        y = camera.position.y,
        z = camera.position.z;
    rotSpeed = .000005;

    camera.position.x = x * Math.cos(rotSpeed) + z * Math.sin(rotSpeed);
    camera.position.z = z * Math.cos(rotSpeed) - x * Math.sin(rotSpeed);
    camera.lookAt(scene.position);
}
