//работа с профилем пользователя
$(document).ready(function(){
//дружить
	$('body').on('click','.friend', function(){
		var target=$(this);
		var values=$(target).attr('data');
		$.ajax({
			type: "POST",
			data: {datas: values, request:'user_friend'},
			url: "index.php",
			dataType: "json",
			success: function (data) {
				$(target).attr('data',data.value);
				$(target).empty();
				$(target).append(data.text);
				if(data.sub){
					$(target).after(data.sub);
				}
			}
		});
	});
//подписаться
	$('body').on('click','.sub', function(){
		var target=$(this);
		var values=$(target).attr('data');
		$.ajax({
			type: "POST",
			data: {datas: values, request:'user_sub'},
			url: "index.php",
			dataType: "json",
			success: function (data) {
				$(target).attr('data',data.value);
				$(target).empty();
				$(target).append(data.text);
			}
		});
	});
//Голосование
	$('body').on('change','.vote',function(){
		var volume=$(this).val();
		$.ajax({
			type: "POST",
			data: {datas: volume, request:'like_user'},
			url: "index.php"
		});
	});
});
