<?php
class note extends body
{
	function __construct(){
		$this->db_method();
		$this->dom_obj();
	}
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		$messadge=$this->db_method->db_select('note',array('user_in_id'=>$_SESSION['id_user']),null,'note_id DESC');
		if($messadge!==false){
			$counter=count($messadge);
			for($i=0;$i<$counter;$i++){
				foreach($messadge[$i] as $key=>$value){
					if($key=='user_out_id'){
						$user=$this->db_method->db_select('user',array('user_id'=>$value),"user_name,user_surname,user_photo");
						if($user!==false){
							$user_name=null;
							foreach($user[0] as $user_keys=>$user_value){
								if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
									$user_name.=" $user_value";
								}
								if($user_keys=='user_photo'){
									if(!empty($user_value)){
										$user_photo="/userphoto/$value/$user_value";
									}
									else{
										$user_photo="/images/no_photo.png";
									}
								}
							}							
						}
						else{
							$data['messadge']=$this->db_method->db_error();
						}
					}
					if($key=='note_time'){
						$mes_time=$value;
					}
					if($key=='note_id'){
						$mes_id=$value;
					}
					if($key=='note_type'){
						$mes_type=$value;
					}
					if($key=='note_status'){
						$mes_status=$value;
					}
					if($key=='note_text'){
						$mes_text=$value;
					}
				}
				@$data['element'].="<div class='element";
				if($mes_status==1){
					$data['element'].=" read";
				}
				$data['element'].="' data='$mes_id'><div class='span4 massage'>$mes_text</div>
										<div class='span3 contacts'>
											<div class='user_contact'>
												<p class='contact_name'>$user_name</p>
												<div class='date'>$mes_time</div>
											</div>
											<div class='user_image'>
												<img src='$user_photo' class='img-thumbnail mini_User_Image' alt='Photo'>
											</div>
										</div>";
				if($mes_type==3 AND $mes_status==0){
					$data['element'].="<div class='span5'>
											<button class='bottom_r true'>Подтвердить дружбу</button>					
											<button class='bottom_r false'>Отказать в дружбе</button>	
										</div>";
				}
				$data['element'].="</div>";
			}
			$messadge=$this->db_method->db_select('note',"user_in_id='".$_SESSION['id_user']."' AND note_status=0 AND note_type<>3",'note_id');
			if($messadge!==false){
				if(is_array($messadge)){
					$counter=count($messadge);
					for($i=0;$i<$counter;$i++){
						if($i==0){$where="note_id='".$messadge[$i]['note_id']."'";}
						else {$where.="OR note_id='".$messadge[$i]['note_id']."'";}
					}
				}
				else{
					$where="note_id='$messadge'";
				}
				$this->db_method->db_update('note',array('note_status'=>1),$where);
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data)) return $data;
	}
}
