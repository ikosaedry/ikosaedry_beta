<?php
class registration extends body
{
	function __construct(){
		$this->db_method();
		$this->dom_obj();
	}
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		if(isset($_POST['registration'])){
			$data['messadge']=$this->reg();
		}
		if(isset($_POST['org_registration'])){
			$data['messadge']=$this->org_reg();
		}
		$profession=$this->db_method->db_select('profesions',null,null,'prof_name');
		$counter=count($profession);
		for($i=0;$i<$counter;$i++){
			foreach($profession[$i] as $key=>$value){
				if($key=='prof_id'){
					$prof_id=$value;
				}
				if($key=='prof_name'){
					$prof_name=$value;
				}
			}
			$pos=strpos($prof_name,'Тест ');
			if($pos===false){
				$select_data[$prof_id]=$prof_name;
			}
		}
		$data['prof']=$this->dom_obj->dom_select('user_profession',$select_data,null,$class_select="form-control sel", $id_select="sel2");
		if(isset($data)){
			return $data;
		}
    }
//регистрация пользователя
	protected function reg(){
		unset($_POST['registration']);
		unset($_POST['user_pass2']);
		$user_id=$this->db_method->db_select('user',array('user_e_mail'=>$_POST['user_e_mail']),'user_id');
		if($user_id){
			return "Пользователь с указанным e-mail зарегестриорван";
		}
		else{
			foreach($_POST as $key=>$value){
				if($key=='user_pass'){
					$query_data[$key]=md5($value);
				}
				else{
					$query_data[$key]=$value;
				}
			}
			$worked=$this->db_method->db_insert('user',$query_data);
			if($worked===false){
				return $this->db_method->db_error();
			}
			else{
				$user_id=mysql_insert_id();
				$worked=$this->db_method->db_insert('user_rank',array('user_id'=>$user_id));
				if($worked===false){
					return $this->db_method->db_error();
				}
				else{
					$worked=$this->db_method->db_insert('user_status',array('user_id'=>$user_id));
					if($worked===false){
						return $this->db_method->db_error();
					}
					else{					
						$_SESSION['id_user']=$user_id;
						header("Location:/news.html");
					}
				}
			}
		}
	}
//регистрация организации
	protected function org_reg(){
		unset($_POST['org_registration']);
		unset($_POST['org_pass2']);
		$org_id=$this->db_method->db_select('organisation',array('org_e_mail'=>$_POST['org_e_mail']),'org_id');
		if($org_id){
			return "Пользователь с указанным e-mail зарегестриорван";
		}
		else{
			foreach($_POST as $key=>$value){
				if($key=='org_pass'){
					$query_data[$key]=md5($value);
				}
				else{
					$query_data[$key]=$value;
				}
			}
			$worked=$this->db_method->db_insert('organisation',$query_data);
			if($worked===false){
				return $this->db_method->db_error();
			}
			else{
				$_SESSION['id_org']=mysql_insert_id();
				header("Location:/");
			}
		}
	}
}
