<?php
class message extends body
{
	function __construct(){
		$this->db_method();
		$this->dom_obj();
	}
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		$sql="(SELECT * FROM `messadge` WHERE `user_in_id` =".$_SESSION['id_user']." AND `mes_dialog` >0 GROUP BY `mes_dialog`)
				UNION 
				(SELECT * FROM `messadge` WHERE `user_in_id` =".$_SESSION['id_user']." AND `mes_dialog` =0)
				ORDER BY mes_time DESC LIMIT 0,30";
		$messadge=$this->db_method->db_all($sql);
		if($messadge!==false){
			$messadge=$this->db_method->result($messadge);
			$counter=count($messadge);
			for($i=0;$i<$counter;$i++){
				foreach($messadge[$i] as $key=>$value){
					if($key=='user_out_id'){
						$user_id=$value;
						$user=$this->db_method->db_select('user',array('user_id'=>$value),"user_name,user_surname,user_photo");
						if($user!==false){
							$user_name=null;
							foreach($user[0] as $user_keys=>$user_value){
								if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
									$user_name.=" $user_value";
								}
								if($user_keys=='user_photo'){
									if(!empty($user_value)){
										$user_photo="/userphoto/$value/$user_value";
									}
									else{
										$user_photo="/images/no_photo.png";
									}
								}
							}							
						}
						else{
							$data['messadge']=$this->db_method->db_error();
						}
					}
					if($key=='mes_time'){
						$mes_time=$value;
					}
					if($key=='mes_id'){
						$mes_id=$value;
					}
					if($key=='mes_status'){
						$mes_status=$value;
					}
					if($key=='mes_text'){
						$mes_text=$value;
					}
				}
				@$data['element'].="<div class='element";
				if($mes_status==1){
					$data['element'].=" read";
				}
				$data['element'].="'><div class='span4 massage' data='$mes_id'>$mes_text</div>
									<div class='span3 contacts' data='$user_id'>
											<div class='user_contact'>
												<p class='contact_name'>$user_name</p>
												<div class='date'>$mes_time</div>
											</div>
										<a href='/personal.html'>
											<div class='user_image'>
												<img src='$user_photo' class='img-thumbnail mini_User_Image' alt='Photo'>
											</div>
										</a>
									</div>
								</div>";
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data)) return $data;
	}

}
