<?php
class news extends body
{
//автоматическая фукнция формировани доступа к библиотекам
	function __construct(){
		$this->db_method();
		$this->file();
		$this->dom_obj();
		$this->formed_news();
		$this->strings();
	}
//формирование ленты новостей, открытая функция
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		if(isset($_POST['news'])){
			$this->save_news();
		}
		if(isset($_POST['photo'])){
			$this->save_photo();
		}
		if(isset($_POST['save_comment'])){
			$this->save_comment();
		}
		$data=$this->formed_news->news($_SESSION['id_user']);
		$_SESSION['anchor_news']=$_SESSION['anchor_like']=$_SESSION['anchor_comment']=$this->strings->microtime_float();
		$status=$this->db_method->db_update('user_status',array('status_news'=>date("Y-m-d H:i:s")),"user_id='".$_SESSION['id_user']."'");
		if($status===false)  $data['messadge']=$this->db_method->db_error();
		$news_level=$this->db_method->db_select('news_rank');
		$counter=count($news_level);
		for($i=0;$i<$counter;$i++){
			foreach($news_level[$i] as $key=>$value){
				if($key=='news_rank_id'){
					$news_rank_id=$value;
				}
				if($key=='news_rank_name'){
					$news_rank_name=$value;
				}
			}
			$select_data[$news_rank_id]=$news_rank_name;
		}
		$data['news_level']=$this->dom_obj->dom_select('news_rank',$select_data,null,$class_select="form-control input-sm select_class bottom_r");
		return $data;
    }
//сохрание фото в новостях, защищенная функция
	protected function save_photo()
	{
		if(isset($_FILES["user_photo"]) AND !empty($_FILES["user_photo"]['tmp_name'])){
			$temp=$_SESSION['id_user'].$_FILES["user_photo"]['name'];
			$block=md5($temp);
			$access=1;
			if(isset($_SESSION['block']) AND $_SESSION['block']==$block) $access=0;
			else $_SESSION['block']=$block;
			if($access>0){
				$files=$this->file->load_file(BASE_PATH."userphoto/".$_SESSION['id_user'],$_FILES["user_photo"]);
				if($files===false){
					return "Ошибка загрузки фото";
				}
				else{
					$data['user_id']=$_SESSION['id_user'];
					$data['news_body']=$files;
					$data['news_type']=2;
					$data['news_rank']=$_POST['news_rank'];
					$data['news_comment']=$_POST['news_comment'];
					$this->db_method->db_insert('news',$data);
					$this->file->open_file("anchor_news.log");
				}
			}
		}
	}
//сохранение новости, защищенная функция
	protected function save_news()
	{
		$temp=$_SESSION['id_user'].$_POST['chat'].'3'.$_POST['news_rank'];
		$block=md5($temp);
		if(!empty($_POST['chat'])){
			if((isset($_SESSION['block']) AND $_SESSION['block']!=$block) OR !isset($_SESSION['block'])){
				$_SESSION['block']=$block;
				$data['user_id']=$_SESSION['id_user'];
				$data['news_body']=$_POST['chat'];
				$data['news_type']=3;
				$data['news_rank']=$_POST['news_rank'];
				$this->db_method->db_insert('news',$data);
				$this->file->open_file("anchor_news.log");
			}
		}
	}
//сохранение коммента, закрытая функция
	protected function save_comment(){
		if(!empty($_POST['user_comment'])){
			$this->db_method->db_insert('news_comment',array('news_id'=>$_POST['save_comment'],'user_id'=>$_SESSION['id_user'],'news_com_body'=>$_POST['user_comment']));
			$this->file->open_file("anchor_comment.log");
		}
	}
}
