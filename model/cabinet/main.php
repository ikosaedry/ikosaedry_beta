<?php
class main extends body
{
	function __construct(){
		$this->db_method();
		$this->file();
		$this->dom_obj();
	}

    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		if(isset($_POST['save'])){
			$data['messadge']=$this->save_choise();
		}
		$user=$this->db_method->db_select('user',array('user_id'=>$_SESSION['id_user']));
		if($user!==false){
			unset($user[0]['user_id']);
			unset($user[0]['user_pass']);
			foreach($user[0] as $key=>$value){
				if($key=='user_sex'){
					$data['user_sex']="<input type='radio' class='inpt_radio1' name='user_sex' value='1'";
					if($value==1) $data['user_sex'].="checked";
					$data['user_sex'].="> Мужской  <input type='radio' class='inpt_radio2' name='user_sex' value='2'";
					if($value==2) $data['user_sex'].="checked";
					$data['user_sex'].="> Женский ";
				}
				else if($key=='user_profession'){
					$profession=$this->db_method->db_select('profesions',null,null,'prof_name');
					$counter=count($profession);
					for($i=0;$i<$counter;$i++){
						foreach($profession[$i] as $keys=>$val){
							if($keys=='prof_id'){
								$prof_id=$val;
							}
							if($keys=='prof_name'){
								$prof_name=$val;
							}
						}
						$pos=strpos($prof_name,'Тест ');
						if($pos===false){
							$select_data[$prof_id]=$prof_name;
						}
					}
					$data[$key]=$this->dom_obj->dom_select('user_profession',$select_data,$value,$class_select="form-control sel", $id_select="sel2");
				}
				else{
					if(!empty($value)){
						if($key=='user_photo'){
							$data[$key]="/userphoto/".$_SESSION['id_user']."/".$value;
						}
						else{
							$data[$key]=htmlspecialchars($value);
						}
					}
				}
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		return $data;
    }

	protected function save_choise()
	{
		if(isset($_FILES["user_photo"]) AND !empty($_FILES["user_photo"]['tmp_name'])){
			$temp=$_SESSION['id_user'].$_FILES["user_photo"]['name'];
			$block=md5($temp);
			$access=1;
			if(isset($_SESSION['block']) AND $_SESSION['block']==$block) $access=0;
			else $_SESSION['block']=$block;
			if($access>0){
				$files=$this->file->load_file(BASE_PATH."userphoto/".$_SESSION['id_user'],$_FILES["user_photo"]);
				if($files===false){
					return "Ошибка загрузки фото";
				}
				else{
					$this->db_method->db_update('user',array('user_photo'=>$files),"user_id='".$_SESSION['id_user']."'");
				}
			}
		}
		unset($_POST['save']);
		foreach($_POST as $key=>$value){
			if(!empty($value)){
				$query_data[$key]=$value;
			}
		}
		$worked=$this->db_method->db_update('user',$query_data,"user_id='".$_SESSION['id_user']."'");
		if($worked===false){
			return $this->db_method->db_error();
		}
	}
}
