<?php
class progres extends body
{
	function __construct(){
		$this->db_method();
	}
    public function get_data()
    {
		if(!isset($_SESSION)){
			session_start();
		}
		if(isset($_POST['save'])){
			$data['messadge']=$this->save_choise();
		}
		$user=$this->db_method->db_select('user_archivement',array('user_id'=>$_SESSION['id_user']));
		if($user!==false){
			$counter=count($user);
			$data['form']=null;
			if($counter>0){
				for($i=0;$i<$counter;$i++){
					unset($user[$i]['user_id']);
					unset($user[$i]['arch_id']);
					foreach($user[0] as $key=>$value){
						switch($key){
							case "arch_name":
								$name=htmlspecialchars($value);
								break;
							case "arch_subject":
								$subject=htmlspecialchars($value);
								break;
							case "arch_document":
								$document=htmlspecialchars($value);
								break;
							case "arch_date":
								$dates=htmlspecialchars($value);
								break;
						}
					}
					$data['form'].="<div class='cont_for_clone'>
										<div class='span8 offset2'>
											<label>Название:<br><input type='text' class='lg_inpt input-xxlarge'  placeholder='Название' name='arch_name[]' value='$name'></label>
										</div>
										<div class='span8 offset2'>
											<label>Описание события:<br><textarea rows='5' class='input-xxlarge' cols='100' placeholder='Описание события' name='arch_subject[]'>$subject</textarea></label>
										</div>
										<div class='span3 offset2'>
											<label>Дата достижения:<br><input type='text' class='datepicker lg_inpt input-large' name='arch_date[]' value='$dates'></label>
										</div>
										<div class='span3 offset1'>
											<label>Диплом:<br><input type='text' class='lg_inpt input-large' placeholder='Диплом' name='arch_document[]' value='$document'></label>
										</div>
									</div>";
				}
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data)) return $data;
    }

	protected function save_choise()
	{
		unset($_POST['save']);
		$worked=$this->db_method->db_delete('user_archivement',"user_id='".$_SESSION['id_user']."'");
		if($worked===false){
			return $this->db_method->db_error();
		}
		foreach($_POST as $key=>$value){
			$counter=count($value);
			for($i=0;$i<$counter;$i++){
				$temp[$i][$key]=$value[$i];
			}
		}
		$_POST=null;
		for($i=0;$i<$counter;$i++){
			foreach($temp[$i] as $key=>$value){
				$query_data[$key]=$value;
			}
			$query_data['user_id']=$_SESSION['id_user'];
			$worked=$this->db_method->db_insert('user_archivement',$query_data);
			if($worked===false){
				return $this->db_method->db_error();
			}
		}
    }

}
