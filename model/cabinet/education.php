<?php
class education extends body
{
	function __construct(){
		$this->db_method();
	}
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		if(isset($_POST['save'])){
			$data['messadge']=$this->save_choise();
		}
		$user=$this->db_method->db_select('user_teach',array('user_id'=>$_SESSION['id_user']));
		if($user!==false){
			$data['form']=null;
			$counter=count($user);
			if($counter>0){
				for($i=0;$i<$counter;$i++){
					unset($user[$i]['user_id']);
					unset($user[$i]['edu_id']);
					foreach($user[0] as $key=>$value){
						switch($key){
							case "edu_diplom_type":
								$diplom_type=htmlspecialchars($value);
								break;
							case "edu_organisation":
								$organisation=htmlspecialchars($value);
								break;
							case "edu_diplom":
								$diplom=htmlspecialchars($value);
								break;
							case "edu_special":
								$special=htmlspecialchars($value);
								break;
							case "edu_start_date":
								$start_date=htmlspecialchars($value);
								break;
							case "edu_finish_date":
								$finish_date=htmlspecialchars($value);
								break;
						}
					}
					$data['form'].="<div class='cont_for_clone'>
										<div class='span8 offset2'>
											<label>Заведение:<br><input type='text' placeholder='Заведение' class='lg_inpt input-xxlarge' name='edu_organisation[]' value='$organisation'></label>
										</div>
										<div class='span8 offset2'>
											<label>Специальность:<br><input type='text' placeholder='Специальность' class='lg_inpt input-xxlarge' name='edu_special[]' value='$special'></label>
										</div>
										<div class='span3 offset2'>
											<label>Начало:<br><input type='text' placeholder='Начало' class='datepicker lg_inpt input-large' name='edu_start_date[]' value='$start_date'></label>
											<label>Окончание:<br><input type='text' placeholder='Окончание' class='datepicker lg_inpt input-large' name='edu_finish_date[]' value='$finish_date'></label>
										</div>
										<div class='span3 offset1'>
											<label>Диплом:<br><input type='text' placeholder='Диплом' class='lg_inpt input-large' name='edu_diplom[]' value='$diplom'></label>
											<label>Тип диплома:<br><input type='text' placeholder='Тип диплома' class='lg_inpt input-large' name='edu_diplom_type[]' value='$diplom_type'></label>
										</div>
									</div>";
				}
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data)) return $data;
    }

	protected function save_choise()
	{
		unset($_POST['save']);
		$worked=$this->db_method->db_delete('user_teach',"user_id='".$_SESSION['id_user']."'");
		if($worked===false){
			return $this->db_method->db_error();
		}
		foreach($_POST as $key=>$value){
			$counter=count($value);
			for($i=0;$i<$counter;$i++){
				$temp[$i][$key]=$value[$i];
			}
		}
		$_POST=null;
		for($i=0;$i<$counter;$i++){
			foreach($temp[$i] as $key=>$value){
				$query_data[$key]=$value;
			}
			$query_data['user_id']=$_SESSION['id_user'];
			$worked=$this->db_method->db_insert('user_teach',$query_data);
			if($worked===false){
				return $this->db_method->db_error();
			}
		}
    }

}
