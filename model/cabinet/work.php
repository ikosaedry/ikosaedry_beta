<?php
class work extends body
{
	function __construct(){
		$this->db_method();
	}
    public function get_data()
    {
		if(!isset($_SESSION)){
			session_start();
		}
		if(isset($_POST['save'])){
			$data['messadge']=$this->save_choise();
		}
		$user=$this->db_method->db_select('user_work',array('user_id'=>$_SESSION['id_user']));
		if($user!==false){
			$counter=count($user);
			$data['form']=null;
			if($counter>0){
				for($i=0;$i<$counter;$i++){
					unset($user[$i]['user_id']);
					unset($user[$i]['work_id']);
					foreach($user[0] as $key=>$value){
						switch($key){
							case "work_position":
								$position=htmlspecialchars($value);
								break;
							case "work_organisation":
								$organisation=htmlspecialchars($value);
								break;
							case "work_arhivement":
								$arhivement=htmlspecialchars($value);
								break;
							case "work_start_date":
								$start_date=htmlspecialchars($value);
								break;
							case "work_finish_date":
								$finish_date=htmlspecialchars($value);
								break;
						}
					}
					$data['form'].="<div class='cont_for_clone'>
										<div class='span8 offset2'>
											<label>Компания:<br><input type='text' class='lg_inpt input-xxlarge' placeholder='Место работы' name='work_organisation[]' value='$organisation'></label>
										</div>
										<div class='span8 offset2'>
											<label>Должность:<br><input type='text' class='lg_inpt input-xxlarge' placeholder='Должность' name='work_position[]' value='$position'></label>
										</div>
										<div class='span3 offset2'>
											<label>Начало:<br><input type='text' class='datepicker lg_inpt input-large' name='work_start_date[]' value='$start_date'></label>
										</div>
										<div class='span3 offset1'>
											<label>Окончание:<br><input type='text' class='datepicker lg_inpt input-large' name='work_finish_date[]' value='$finish_date'></label>
										</div>
										<div class='span8 offset2'>
											<label>Достижение:<br><input type='text' class='lg_inpt input-xxlarge' placeholder='Достижение' name='work_arhivement[]' value='$arhivement'></label>
										</div>
									</div>";
				}
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data)) return $data;
    }

	protected function save_choise()
	{
		unset($_POST['save']);
		$worked=$this->db_method->db_delete('user_work',"user_id='".$_SESSION['id_user']."'");
		if($worked===false){
			return $this->db_method->db_error();
		}
		foreach($_POST as $key=>$value){
			$counter=count($value);
			for($i=0;$i<$counter;$i++){
				$temp[$i][$key]=$value[$i];
			}
		}
		$_POST=null;
		for($i=0;$i<$counter;$i++){
			foreach($temp[$i] as $key=>$value){
				$query_data[$key]=$value;
			}
			$query_data['user_id']=$_SESSION['id_user'];
			$worked=$this->db_method->db_insert('user_work',$query_data);
			if($worked===false){
				return $this->db_method->db_error();
			}
		}
    }

}
