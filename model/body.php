<?php
class body extends Model
{
    public function get_data($action,$zone)
    {
		if(!empty($zone)){
			if(!isset($_SESSION)) session_start();
			require_once "model/$zone/$action.php";
			$ob_classes=new $action();
			return $ob_classes->get_data();
		}
    }
}
?>
