<?php
class main extends body
{
	function __construct(){
		$this->db_method();
	}
    public function get_data()
    {
		if(!isset($_SESSION)) session_start();
		if($_SESSION['current_user_id']==$_SESSION['id_user']){
			header("Location:/cabinet.html");
		}
		if(isset($_POST['send'])){
			$this->send_message();
		}
		$user=$this->db_method->db_select('user',array('user_id'=>$_SESSION['current_user_id']));
		if($user!==false){
			unset($user[0]['user_id']);
			unset($user[0]['user_pass']);
			foreach($user[0] as $key=>$value){
				if(!empty($value)){
					if($key=='user_photo'){
						$data[$key]="/userphoto/".$_SESSION['current_user_id']."/$value";
					}
					elseif($key=='user_sex'){
						if($value==1)$data[$key]='Мужской';
						if($value==2)$data[$key]='Женский';
					}
					elseif($key=='user_profession'){
						$data[$key]=$this->db_method->db_select('profesions',array('prof_id'=>$value),'prof_name');
					}
					else{
						$data[$key]=htmlspecialchars($value);
					}
				}
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		$friend=$this->db_method->db_select('friends',array('user_id'=>$_SESSION['id_user'],'friends_user_id'=>$_SESSION['current_user_id']),'friends_state');
		if($friend!==false){
			if(count($friend)>0){
				switch($friend){
					case 0:
						$data['friends']="<div style='float: right; position: relative'><div class='title3' data-title='Oжидание ответа' style='float: right'><img src='../../images/дружба предложена и ожидание ответа.png' style='height: 25px;margin-left: 10px;'></div></div>";
						break;
					case 1:
						$data['friends']="<div style='float: right; position: relative'><div class='title3' data-title='Oжидание ответа' style='float: right'><img src='../../images/дружба предложена и ожидание ответа.png' style='height: 25px;margin-left: 10px;'></div></div>";
						break;
					case 2:
						$data['friends']="<div style='float: right; position: relative'><div class='title3' data-title='Удалить из друзей' style='float: right'><img src='../../images/удалить.png' style='height: 25px; margin-left: 10px;'></div></div>";
						break;
				}
				$data['friends_value']=$friend;
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if($friend!=2){
			$sub=$this->db_method->db_select('subscription',array('user_id'=>$_SESSION['id_user'],'sub_user_id'=>$_SESSION['current_user_id']));
			if($sub!==false){
				if(count($sub)>0)$data['sub_value']=1;
			}
			else{
				$data['messadge']=$this->db_method->db_error();
			}
		}
		$data['vote']=$this->db_method->db_select('user_like',array('user_liked_id'=>$_SESSION['id_user'],'user_id'=>$_SESSION['current_user_id']),'user_liked_power');
		$data['like']=count($this->db_method->db_select('user_like',array('user_id'=>$_SESSION['current_user_id'],'user_liked_id'=>$_SESSION['id_user']),'user_like_id'));
		$data['level']=$this->db_method->db_select('user_rank',array('user_id'=>$_SESSION['id_user']),'user_rank');
		return $data;
    }
//запись сообщения в базу	
	protected function send_message(){
		if(!empty($_POST['message'])){
			$this->db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],10,$_POST['message']);
		}
	}

}
