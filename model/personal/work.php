<?php
class work extends body
{
	function __construct(){
		$this->db_method();
	}
    public function get_data()
    {
		if(!isset($_SESSION)){
			session_start();
		}
		$user=$this->db_method->db_select('user_work',array('user_id'=>$_SESSION['current_user_id']));
		if($user!==false){
			if(!empty($user)){
				$count_edu=count($user);
				for($i=0;$i<$count_edu;$i++){
					unset($user[$i]['work_id']);
					unset($user[$i]['user_id']);
					@$data['table'].="<tr>";
					foreach($user[$i] as $key=>$value){
						$data['table'].="<td>".htmlspecialchars($value)."</td>";
					}
					$data['table'].="</tr>";
				}
			}
			else{
				$data['table']="<tr><td colspan=5>Данные отсутсвуют</td></tr>";
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		return $data;
    }

}
