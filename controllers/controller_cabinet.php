<?php
class Controller_cabinet extends Controller
{
	private $action;
	private $body;

	function __construct($action)
	{
		$this->action=$action;
		$this->body=new body($action);
		parent::__construct();
	}
	function action_main()
    {
		$temp="view_cabinet/view_$this->action.php";
		$data=$this->body->get_data($this->action,'cabinet');
		if(!empty($data) AND is_array($data)){
			$data=array_merge($data,$this->log->get_data());
		}
		else{
			$data=$this->log->get_data();
		}
		if(file_exists($temp)){
			$this->view->generate($temp, 'cabinet_view.php', $data);
		}
		else{
			$this->view->generate('404_view.php', 'cabinet_view.php');
		}
	}
}
