<!--head-->
	<link rel="stylesheet" href="/css/bootstrap_3.min.css">
<!--/head-->
<script src="/js/sh_book.js"></script>
<?if(isset($data['messadge'])) echo $data['messadge']; ?>
<div class="v_scroll-bar">
<form  method="post" action="" enctype="multipart/form-data">
	<div class="container v_colour v_wrapper news_block2">
		<div class="col-md-12"><h1 class="text-center" style="padding-right:80px">книги</h1></div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<span class="center-block btn-file text-center">
						загрузить книгу<input type="file" name="user_book_link">
				</span>	
                    <div class="v_overflow_wrapper">
						<img src='../images/pdf.PNG' alt='pdf' class='book_format'/>
						<img src='../images/txt.PNG' alt='txt' class='book_format'/>
						<img src='../images/doc.PNG' alt='doc' class='book_format'/>
				    </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<span class="center-block btn-file text-center bg_input_file">
					загрузить обложку<input type="file" class="file" name="user_book_photo">
				</span>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<label class="v_txt v_txt_margin">введите автора книги.</label>
				<input type="text" class="center-block v_txt_input lg_inpt" name="user_book_discript">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<label class="v_txt v_txt_margin text-center">название книги.</label>
				<textarea type="text" class="center-block v_txt_area" name="user_book_author"></textarea>
			</div>
		</div>
		<button type="submit" name="save" class="pull-right">сохранить</button>
</form>	
		<?if(isset($data['table'])){?>
			<label>Загруженные книги</label>
			<table class="v_books-list">
				<?echo $data['table'];?>
			</table>
		<?}?>
	</div>
</div>
