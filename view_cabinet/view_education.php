<div class="reg_body4 container_2">
<div class="reg_body7 cont">
	<div class="container news_block2 margin_l">
		<legend>Учеба</legend>
		<?if(isset($data['messadge'])) echo $data['messadge'];?>
		<form  method="post" action="">
			<div class="span12 form">
				<?if(isset($data['form'])) echo $data['form'];
				else{?>
					<div class="cont_for_clone"> <!--Контейнер с содержимы что будет клонироваться, что бы работала кн. Добавить его нельзя удалят--->
						<div class="span2">
										<span class="remuve_bnt" style='display:none'>
											<img src='../images/Закрыть.png' style='height: 15px;'>
										</span>							
						</div>						
						<div class="span8 offset2">
							<label>Заведение:<br><input type='text' placeholder='Заведение' class="lg_inpt input-xxlarge" name='edu_organisation[]'></label>						
						</div>
						<div class="span8 offset2">
							<label>Специальность:<br><input type='text' placeholder='Специальность' class="lg_inpt input-xxlarge" name='edu_special[]'></label>
						</div>
						<div class="span3 offset2">
							<label>Начало:<br><input type='text' placeholder='Начало' class='datepicker lg_inpt input-large' name='edu_start_date[]'></label>
							<label>Окончание:<br><input type='text' placeholder='Окончание' class='datepicker lg_inpt input-large' name='edu_finish_date[]'></label>
						</div>
						<div class="span3 offset1">
							<label>Диплом:<br><input type='text' placeholder='Есть диплом/Нету диплома' class="lg_inpt input-large" name='edu_diplom[]'></label>
							<label>Тип диплома:<br><input type='text' placeholder='Бакалавр, Специалист, Магистр' class="lg_inpt input-large" name='edu_diplom_type[]'></label>
						</div>
						<div class="span11 divide"></div>
					</div>
				<?}?>
			</div>
			<div class="span11">
				<button type="submit" class="bottom_r input-medium" name="save">Сохранить</button>
				<button class="bottom_r clon_btn input-medium">Добавить</button>
			</div>
		</form>
	</div>
</div>
</div>