<div class="reg_body4 container_2">
<div class="reg_body7 cont container">
	<div class="row news_block2 margin_l">
		<legend>Комната
			<a href='/' class='pull-right' style='position: relative'><div class="title5" data-title="Закрыть"><img src='../images/Закрыть.png' style='height: 10px'></div></a>
		</legend>
		<form  method="post" action="" enctype="multipart/form-data">
			<div class="form" style='width: 100%'>
				<div class="cont_for_clone">
					<div class="span6" style='width: 50%'>
						<label for="user_room_flooring" style='position: relative'>
							<div class="btn_choose_photo">Выберите текстуру пола</div>
							<img src="<?if(isset($data['user_room_flooring'])) echo $data['user_room_flooring']; else echo "/images/textures/room/beton.jpg"?>" height="50px" width="50px" style='margin-left:10px'>
							<input type="file" class="form-control choose_photo1" name="user_room_flooring">
						</label>
					</div>
					<div class="span5" style='width: 40%'>
						<label for="user_room_wall_right" style='position: relative'>
							<div class="btn_choose_photo">Выберите текстуру правой стены</div>
							<img src="<?if(isset($data['user_room_wall_right'])) echo $data['user_room_wall_right']; else echo "/images/textures/room/beton.jpg"?>" height="50px" width="50px" style='margin-left:10px'>
							<input type="file" class="form-control choose_photo2" name="user_room_wall_right">
						</label>
					</div>
					<div class="span6" style='width: 50%'>
						<label for="user_room_wall_left" style='position: relative'>
							<div class="btn_choose_photo">Выберите текстуру левой стены</div>
							<img src="<?if(isset($data['user_room_wall_left'])) echo $data['user_room_wall_left']; else echo "/images/textures/room/beton.jpg"?>" height="50px" width="50px" style='margin-left:10px'>
							<input type="file" class="form-control choose_photo3" name="user_room_wall_left">
						</label>
					</div>
					<div class="span5" style='width: 40%'>
						<label for="user_room_ceiling" style='position: relative'>
							<div class="btn_choose_photo">Выберите текстуру потолка</div>
							<img src="<?if(isset($data['user_room_ceiling'])) echo $data['user_room_ceiling']; else echo "/images/textures/room/beton.jpg"?>" height="50px" width="50px" style='margin-left:10px'>
							<input type="file" class="form-control choose_photo4" name="user_room_ceiling">
						</label>
					</div>
					<!--div class="span4">
						<label for="user_room_window_before">
							<div class="btn_choose_photo">Выберите вид из окна перед вами</div>
							<img src="<!--?if(isset($data['user_room_window_before'])) echo $data['user_room_window_before']; else echo "/images/textures/room/trava.jpg"?>" height="50px" width="50px"-->
							<!--input type="file" class="form-control choose_photo5" name="user_room_window_before">
						</label>
					</div-->
					<!--div class="span3">
						<label for="user_room_window_after">
							<div class="btn_choose_photo">Выберите вид из окна за вами</div>
							<img src="<!--?if(isset($data['user_room_window_after'])) echo $data['user_room_window_after']; else echo "/images/textures/room/trava.jpg"?>" height="50px" width="50px"-->
							<!--input type="file" class="form-control choose_photo6" name="user_room_window_after">
						</label>
					</div-->
				</div>
			</div>
			<div class="span11" style='width: 100%'>
				<button type="submit" class="bottom_r" name="save">Сохранить</button>
			</div>
		</form>
	</div>
</div>
</div>
