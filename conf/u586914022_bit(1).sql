
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 20 2015 г., 08:17
-- Версия сервера: 10.0.20-MariaDB
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u586914022_bit`
--

-- --------------------------------------------------------

--
-- Структура таблицы `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `friends_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friends_user_id` int(11) NOT NULL,
  `friends_state` int(11) NOT NULL,
  PRIMARY KEY (`friends_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `news_type` int(11) NOT NULL,
  `news_body` text COLLATE utf8_unicode_ci NOT NULL,
  `news_rank` double NOT NULL,
  `news_date` date NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`news_id`, `user_id`, `news_type`, `news_body`, `news_rank`, `news_date`) VALUES
(38, 15, 0, '6ff20efe.jpg', 0, '2015-08-19'),
(37, 15, 0, '93608_1H.jpg', 0, '2015-08-19'),
(36, 15, 1, '14000000.jpg', 0, '2015-08-19'),
(35, 16, 1, '11816807.jpg', 0, '2015-08-18'),
(34, 10, 1, '93608_1H.jpg', 0, '2015-08-18'),
(33, 10, 1, '11816807.jpg', 0, '2015-08-17'),
(32, 10, 1, '101439559678.jpeg', 0, '2015-08-14'),
(31, 10, 1, '101439559470.jpeg', 0, '2015-08-14'),
(30, 10, 1, '101439559004.jpeg', 0, '2015-08-14'),
(29, 10, 1, '101439491681.jpeg', 0, '2015-08-13'),
(28, 10, 1, '101439491681.jpeg', 0, '2015-08-13'),
(27, 10, 1, '101439491637.jpeg', 0, '2015-08-13'),
(26, 10, 1, '101439491637.jpeg', 0, '2015-08-13'),
(25, 10, 1, '101439491549.5084.jpeg', 0, '2015-08-13'),
(24, 10, 1, '101439491549.5084.jpeg', 0, '2015-08-13');

-- --------------------------------------------------------

--
-- Структура таблицы `profesions`
--

CREATE TABLE IF NOT EXISTS `profesions` (
  `prof_id` int(11) NOT NULL AUTO_INCREMENT,
  `prof_name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`prof_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `profesion_rank`
--

CREATE TABLE IF NOT EXISTS `profesion_rank` (
  `prof_rank_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `prof_rank` int(11) NOT NULL,
  PRIMARY KEY (`prof_rank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` text COLLATE utf8_unicode_ci NOT NULL,
  `user_second_name` text COLLATE utf8_unicode_ci NOT NULL,
  `user_surname` text COLLATE utf8_unicode_ci NOT NULL,
  `user_sex` int(11) NOT NULL,
  `user_birthday` date NOT NULL,
  `user_tel_number` text COLLATE utf8_unicode_ci NOT NULL,
  `user_e_mail` text COLLATE utf8_unicode_ci NOT NULL,
  `user_skype` text COLLATE utf8_unicode_ci NOT NULL,
  `user_photo` text COLLATE utf8_unicode_ci NOT NULL,
  `user_pass` text COLLATE utf8_unicode_ci NOT NULL,
  `user_country` text COLLATE utf8_unicode_ci NOT NULL,
  `user_sity` text COLLATE utf8_unicode_ci NOT NULL,
  `user_adres` text COLLATE utf8_unicode_ci NOT NULL,
  `user_rank` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_second_name`, `user_surname`, `user_sex`, `user_birthday`, `user_tel_number`, `user_e_mail`, `user_skype`, `user_photo`, `user_pass`, `user_country`, `user_sity`, `user_adres`, `user_rank`) VALUES
(3, 'Валера', '', 'Шнуровой', 0, '0000-00-00', '', 'drahma2@rambler.ru', '', '', '202cb962ac59075b964b07152d234b70', '', '', '', 0),
(4, 'Некто', '', 'Нектоевич', 0, '0000-00-00', '', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 0),
(5, 'nurlan', '', 'turekhanov', 0, '0000-00-00', '', 'nurlanturekhanov@gmail.com', '', '', '963dede1da3cf330deadf03026c85c76', '', '', '', 0),
(6, 'Юрий', '', 'Рудакевич', 0, '0000-00-00', '', 'yurij.rudakevych@gmail.com', '', '', '3fde6bb0541387e4ebdadf7c2ff31123', '', '', '', 0),
(7, 'Анна', '', 'Петрова', 0, '0000-00-00', '', 'anna.rudakevych@gmail.com', '', '', '1b8ce220e62e5634c784b0254708b678', '', '', '', 0),
(8, 'Тарас', '', 'Приставский ', 0, '0000-00-00', '', 'happyuser@list.ru', '', '', 'ae46de999858181c85b0bcaad9c21c4d', '', '', '', 0),
(9, 'Drahma', '', 'Sh', 0, '0000-00-00', '', '_val@rambler.ru', '', '', 'aa723cbb7a1259722d413fc56b9e040b', '', '', '', 0),
(10, 'Valera', 'Sergeevich', 'Drahma', 0, '1980-08-12', '', 'a@a.com', '', '93608_1H.jpg', 'c4ca4238a0b923820dcc509a6f75849b', 'Украина', 'Днепропетровск', '', 0),
(11, 'Юра', '', 'Петрович', 0, '0000-00-00', '', 'tester1.rudakevych@gmail.com', '', '', '3926987edf0f9215b5533ec14743b988', '', '', '', 0),
(12, 'serhy', '', 'serhy', 0, '0000-00-00', '', 'serhy@bk.ru', '', '', '1babef99fd8eaf65042f8ce8a52328cb', '', '', '', 0),
(13, 'qqqq', '', 'wwwww', 0, '0000-00-00', '', 'v.chuy@meta.ua', '', '', '96e79218965eb72c92a549dd5a330112', '', '', '', 0),
(14, 'Тес', '', 'Петрович', 0, '0000-00-00', '', 'tester4.rudakevych@gmail.com', '', '', '3fde6bb0541387e4ebdadf7c2ff31123', '', '', '', 0),
(15, 'b', '', 'b', 0, '0000-00-00', '', 'b@b.com', '', '6ff20efe.jpg', 'c4ca4238a0b923820dcc509a6f75849b', '', '', '', 0),
(16, 'c', '', 'c', 0, '0000-00-00', '', 'c@c.com', '', '11816807.jpg', 'c4ca4238a0b923820dcc509a6f75849b', '', '', '', 0),
(17, 'c', '', 'c', 0, '0000-00-00', '', 'e@e.com', '', '', 'c4ca4238a0b923820dcc509a6f75849b', '', '', '', 0),
(18, 'oh_hello', '', 'did_i_know_you', 0, '0000-00-00', '', 'spider.kom@gmail.com', '', '', 'dd77e967177a9257c1b9bbaca16336ab', '', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_archivement`
--

CREATE TABLE IF NOT EXISTS `user_archivement` (
  `arch_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `arch_name` text COLLATE utf8_unicode_ci NOT NULL,
  `arch_subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `arch_date` date NOT NULL,
  `arch_document` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`arch_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `user_archivement`
--

INSERT INTO `user_archivement` (`arch_id`, `user_id`, `arch_name`, `arch_subject`, `arch_date`, `arch_document`) VALUES
(1, 10, 'test', 'test', '2015-08-01', 'test'),
(3, 5, 'rtgerwfg', 'srfgewr', '2015-08-11', 'rgerg');

-- --------------------------------------------------------

--
-- Структура таблицы `user_teach`
--

CREATE TABLE IF NOT EXISTS `user_teach` (
  `edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `edu_organisation` text COLLATE utf8_unicode_ci NOT NULL,
  `edu_start_date` date NOT NULL,
  `edu_finish_date` date NOT NULL,
  `edu_special` text COLLATE utf8_unicode_ci NOT NULL,
  `edu_diplom` text COLLATE utf8_unicode_ci NOT NULL,
  `edu_diplom_type` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`edu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `user_teach`
--

INSERT INTO `user_teach` (`edu_id`, `user_id`, `edu_organisation`, `edu_start_date`, `edu_finish_date`, `edu_special`, `edu_diplom`, `edu_diplom_type`) VALUES
(2, 10, 'test', '2015-08-01', '2015-08-02', 'test', 'test', 'test'),
(3, 5, 'dghgh', '2015-08-12', '2015-02-24', 'dghrh', 'dbhrt', 'dgh');

-- --------------------------------------------------------

--
-- Структура таблицы `user_work`
--

CREATE TABLE IF NOT EXISTS `user_work` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `work_organisation` text COLLATE utf8_unicode_ci NOT NULL,
  `work_start_date` date NOT NULL,
  `work_finish_date` date NOT NULL,
  `work_position` text COLLATE utf8_unicode_ci NOT NULL,
  `work_arhivement` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`work_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `user_work`
--

INSERT INTO `user_work` (`work_id`, `user_id`, `work_organisation`, `work_start_date`, `work_finish_date`, `work_position`, `work_arhivement`) VALUES
(5, 0, '', '0000-00-00', '0000-00-00', '', ''),
(4, 0, '', '0000-00-00', '0000-00-00', '', ''),
(6, 0, 'test', '0000-00-00', '0000-00-00', 'test', 'test'),
(7, 0, 'test', '0000-00-00', '0000-00-00', 'test', 'test'),
(8, 0, 'test', '0000-00-00', '0000-00-00', 'test', 'test'),
(12, 10, 'test', '2015-08-01', '2015-08-02', 'test', 'test'),
(13, 5, 'rge', '2015-08-11', '2015-08-13', 'ge', 'ge');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
