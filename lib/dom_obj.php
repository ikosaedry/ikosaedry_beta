<?php


class dom_obj extends lib
{
	public function dom_select($name,$value,$preselect=null,$class_select=null,$id_select=null){
		$data="<select name='$name'";
		if(!empty($class_select)) $data.=" class='$class_select'";
		if(!empty($id_select)) $data.=" id='$id_select'";
		$data.='>';
		foreach($value as $key=>$value){
			$data.='<option ';
			if($key==$preselect) $data.=" selected ";
			$data.=" value='$key'>$value</option>";
		}
		$data.='</select>';
		return $data;
	}
}
?>
