<?php

class ajax extends lib{
	private $data;
//формирование данных по запросу аякса, функция автоматическая, работает по обращению к классу
	function __construct()
	{
		if(isset($_POST['request'])){
			$this->data=$this->$_POST['request']();
		}
	}
//возврат данных JSON, функция автоматическая, работает по окончанию работы класса
	function __destruct()
	{
		if(is_array($this->data)) echo json_encode($this->data);
		else echo $this->data;
	}
//БЛОК РАБОТЫ ИКОСАЕДРА
//возврат всех ID пользователей в базе, защищенная функция
	protected function all_id(){
		$db_method=new db_method();
		return $db_method->db_select('user,user_rank','user.user_id=user_rank.user_id AND user.profession='.$_POST['prof']." AND user_rank.user_rank=".$_POST['level'],'user.user_id AS user_id, user.user_photo AS user_photo, user.user_name AS user_name, user.user_second_name AS user_second_name, user.user_surname AS user_surname, user.user_profession AS user_profession, user_rank.user_rank AS user_rank');
	}
//возврат оформления комнаты пользователя, защищенная функция
	protected function room_textures(){
		$db_method=new db_method();
		return $db_method->db_select('user_room',array('user_id'=>$_POST['uid']),'user_id, user_room_flooring, user_room_wall_right, user_room_wall_left, user_room_ceiling, user_room_window_before, user_room_window_after');
	}
//возврат загруженых книг пользователя
	protected function user_books(){
		$db_method=new db_method();
		return $db_method->db_select('user_book',array('user_id'=>$_POST['uid']),'user_book_id,user_id,user_book_photo,user_book_link,user_book_discript,user_book_author');
		/*
		 SELECT
	`user_book`.*,
        IFNULL(`user_book_position`.`x`,'unset') as 'x',
        IFNULL(`user_book_position`.`y`,'unset') as 'y',
        IFNULL(`user_book_position`.`z`,'unset') as 'z'
FROM `user_book`
LEFT JOIN `user_book_position`
ON `user_book_position`.`book_id`=`user_book`.`user_book_id`
WHERE `user_book`.`user_id` = 1
		 */

		//return array(0=>array('uid'=>-1,'book_id'=>0,'url'=>'1/1.html','name'=>'Book name','info'=>'Information about the book','picture_url'=>'1/image.jpg'));
	}
//возврат списка профессий, защищенная функция
	protected function prof_list(){
		$db_method=new db_method();
		return $db_method->db_select('profesions',null,'prof_id, prof_name');
	}

//БЛОК РАБОТЫ НОВОСТЕЙ
//запись в базу лайка новости, защищенная функция
	protected function like_news(){
		if(!isset($_SESSION)) session_start();
		$db_method=new db_method();
		$file=new file();
		$user_rank=$db_method->db_select('user_rank',array('user_id'=>$_SESSION['id_user']),'user_rank');
		if($user_rank!==false){
			if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
				$like_news=$db_method->db_select('news_like',array('news_id'=>$_POST['param1'],'user_id'=>$_SESSION['id_user']));
				if($like_news!==false){
					if(count($like_news)==0){
						$db_method->db_insert('news_like',array('news_id'=>$_POST['param1'],'user_id'=>$_SESSION['id_user'],'news_like_type'=>$_POST['param2']));
						$file->open_file("anchor_like.log");
						$user_id=$db_method->db_select('news',array('news_id'=>$_POST['param1']),'user_id');
						$user_rank=$db_method->db_select('levels',array('levels'=>$user_rank),'levels_power')/1000;
						$db_method->db_user_rank($user_id,$user_rank,$_POST['param2']);
						if($_POST['param2']==1){
							$db_method->db_messadge($user_id,$_SESSION['id_user'],1);
						}
						else{
							$db_method->db_messadge($user_id,$_SESSION['id_user'],2);
						}
					}
					else return false;
				}
				else return $this->db_method->db_error();
			}
			else return false;
		}
		else return $this->db_method->db_error();
	}
//запись в базу комента новости, защищенная функция
	protected function comment_news(){
		if(!isset($_SESSION)) session_start();
		$db_method=new db_method();
		$file=new file();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			$result=$db_method->db_insert('news_comment',array('news_id'=>$_POST['save_comment'],'user_id'=>$_SESSION['id_user'],'news_com_body'=>$_POST['user_comment']));
			if($result===false) return $this->db_method->db_error();
			else $file->open_file("anchor_comment.log");
		}
	}
//запись в базу прошаренной новости, защищенная функция
	protected function share_news(){
		if(!isset($_SESSION)) session_start();
		$db_method=new db_method();
		$file=new file();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			$like_news=$db_method->db_select('news',array('news_type'=>'4','news_body'=>$_POST['param1'],'user_id'=>$_SESSION['id_user']));
			if($like_news!==false){
				if(count($like_news)==0){
					$result=$db_method->db_insert('news',array('news_rank'=>'3','news_type'=>'4','news_body'=>$_POST['param1'],'user_id'=>$_SESSION['id_user']));
					if($result===false) return $this->db_method->db_error();
					else $file->open_file("anchor_news.log");
				}
				else return false;
			}
			else return $this->db_method->db_error();
		}
		else return false;

	}
//обновление новосной ленты, защищенная функция
	protected function update_news(){
		if(!isset($_SESSION)) session_start();
		$formed_news=new formed_news();
		$strings=new strings();
		$db_method=new db_method();
		if(isset($_SESSION['anchor_news']) AND !empty($_SESSION['anchor_news'])){
			if($_SESSION['anchor_news']<fileatime(BASE_PATH."anchor/anchor_news.log")){
				$data=$formed_news->news($_SESSION['id_user'],date("Y-m-d H:i:s",$_SESSION['anchor_news']));
				$_SESSION['anchor_news']=fileatime(BASE_PATH."anchor/anchor_news.log");
			}
		}
		if(isset($_SESSION['anchor_like']) AND !empty($_SESSION['anchor_like'])){
			if($_SESSION['anchor_like']<fileatime(BASE_PATH."anchor/anchor_like.log")){
				$number_like=$db_method->db_all("SELECT news_id,news_like_type FROM news_like WHERE news_like_time >'".date("Y-m-d H:i:s",$_SESSION['anchor_news'])."'");
				if($number_like!==false){
					$rows=mysql_num_rows($number_like);
					if(!empty($number_like)){
						for($i=0;$i<$rows;$i++){
							$row=mysql_fetch_array($number_like, MYSQL_ASSOC);
							foreach ($row as $key=>$value){
								$news_id[$key][]=$value;
							}
						}
						$counter=count($news_id['news_id']);
						for($i=0;$i<$counter;$i++){
							$data['like'][$i]['id']=$news_id['news_id'][$i];
							$data['like'][$i]['type']=$news_id['news_like_type'][$i];
							if($news_id['news_like_type'][$i]==1){
								$data['like'][$i]['body']=count($db_method->db_select('news_like',array('news_id'=>$news_id['news_id'][$i],'news_like_type'=>1),'news_like_id'));
							}
							if($news_id['news_like_type'][$i]==2){
								$data['like'][$i]['body']=count($db_method->db_select('news_like',array('news_id'=>$news_id['news_id'][$i],'news_like_type'=>2),'news_like_id'));
							}
						}
					}
				}
				else{
					$data['error']=$db_method->db_error();
				}
				$_SESSION['anchor_like']=fileatime(BASE_PATH."anchor/anchor_like.log");
			}
		}
		if(isset($_SESSION['anchor_comment']) AND !empty($_SESSION['anchor_comment'])){
			if($_SESSION['anchor_comment']<fileatime(BASE_PATH."anchor/anchor_comment.log")){
				$comment=$db_method->db_all("SELECT * FROM news_comment WHERE news_com_time >'".date("Y-m-d H:i:s",$_SESSION['anchor_comment'])."'");
				if($comment!==false){
					$comment=$db_method->result($comment);
					$counter=count($comment);
					for($i=0;$i<$counter;$i++){
						$data['comment'][$i]['id']=$comment[$i]['news_id'];
						$formed[0]=$comment[$i];
						$data['comment'][$i]['body']=$formed_news->news_comment($formed);
					}
				}
				else{
					$data['error']=$db_method->db_error();
				}
				$_SESSION['anchor_comment']=fileatime(BASE_PATH."anchor/anchor_comment.log");
			}
		}
		$db_method->db_update('user_status',array('status_news'=>date("Y-m-d H:i:s")),"user_id='".$_SESSION['id_user']."'");
		if(isset($data))return $data;
	}

//запись в сессию ID просматриваемого пользователя, защищенная функция
	protected function save_personal_id(){
		if(!isset($_SESSION)) session_start();
		$_SESSION['current_user_id']=$_POST['param1'];
	}
//обработка дружбы на странице пользователя, защищенная функция
	protected function user_friend(){
/*
входные данные
$_POST[datas] - тип отношей между пользователями, возможные значения:
0 - в дружбе отказано
1 - отправлено предложение в дружбе
2 - друзья + подписка
3 - отношения отсутсвуют
*/
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			if(isset($_SESSION['current_user_id']) AND !empty($_SESSION['current_user_id'])){
				$db_method=new db_method();
				switch($_POST['datas']){
					case 0:
						break;
					case 1:
						break;
					case 2:
						$friend=3;
						break;
					case 3:
						$friend=1;
						break;
				}
				if(isset($friend)){
					if($friend==1){
						$friend=$db_method->db_select('friends',array('user_id'=>$_SESSION['id_user'],'friends_user_id'=>$_SESSION['current_user_id']));
						if($friend!==false and count($friend)==0){
							$status=$db_method->db_insert('friends',array('user_id'=>$_SESSION['id_user'],'friends_user_id'=>$_SESSION['current_user_id'],'friends_state'=>1));
							if($status===false)return $db_method->db_error();
							$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],3);
							if($status===false)return $db_method->db_error();
							$data['text']="<div style='float: right; position: relative'><div class='title3' data-title='Oжидание ответа' style='float: right'><img src='../images/дружба предложена и ожидание ответа.png' style='height: 25px;margin-left: 10px;'></div></div>";
							$data['value']=1;
							return $data;
						}
					}
					else{
						$status=$db_method->db_delete('friends',"user_id='".$_SESSION['id_user']."' AND friends_user_id='".$_SESSION['current_user_id']."'");
						if($status===false)return $db_method->db_error();
						$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],4);
						if($status===false)return $db_method->db_error();
						$data['text']="<i class='fa fa-user-plus fa-2x v_subscribe' title='Дружить'></i>";
						$data['value']=1;
						$sub=$db_method->db_select('subscription',array('user_id'=>$_SESSION['current_user_id'],'sub_user_id'=>$_SESSION['id_user']));
						if($sub!==false){
							if(count($sub)>0){
								$data['sub']="<div class='sub' style='display: inline' data='0'><div style='float: right; position: relative'><div class='title3' data-title='Подписаться'><img src='../images/Подписаться.png' style='height: 25px;margin-left: 10px;'></div></div></div>";
							}
							else{
								$data['sub']="<div class='sub' style='display: inline' data='1'><div style='float: right; position: relative'><div class='title3' data-title='Отписаться' style='float: right'><img src='../images/Отписаться.png' style='height: 25px;margin-left: 10px;'></div></div></div>";
							}
						}
						else return $db_method->db_error();
						$data['value']=3;
						return $data;						
					}
				}
			}
		}
	}
//обработка подписки на странице пользователя, защищенная функция
	protected function user_sub(){
/*
входные данные
$_POST[datas] - тип отношей между пользователями, возможные значения:
0 - отношения отсутсвуют
1 - пользователь подписан
*/
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			if(isset($_SESSION['current_user_id']) AND !empty($_SESSION['current_user_id'])){
				$db_method=new db_method();
				if($_POST['datas']!=1){
					$sub=$db_method->db_select('subscription',array('user_id'=>$_SESSION['current_user_id'],'sub_user_id'=>$_SESSION['id_user']));
					if($sub!==false and count($sub)==0){
						$status=$db_method->db_insert('subscription',array('user_id'=>$_SESSION['current_user_id'],'sub_user_id'=>$_SESSION['id_user']));
						if($status===false)return $db_method->db_error();
						$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],5);
						if($status===false)return $db_method->db_error();
						$data['text']="<div style='float: right; position: relative'><div class='title3' data-title='Отписаться' style='float: right'><img src='../images/Отписаться.png' style='height: 25px;margin-left: 10px;'></div></div>";
						$data['value']=1;
						return $data;
					}
				}
				else{
					$status=$db_method->db_delete('subscription',"user_id='".$_SESSION['current_user_id']."' AND sub_user_id='".$_SESSION['id_user']."'");
					if($status===false)return $db_method->db_error();
					$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],6);
					if($status===false)return $db_method->db_error();
					$data['text']="<div style='float: right; position: relative'><div class='title3' data-title='Подписаться'><img src='../images/Подписаться.png' style='height: 25px;margin-left: 10px;'></div></div>";
					$data['value']=0;
					return $data;						
				}
			}
		}
	}
//запись в базу лайка пользователя, защищенная функция
	protected function like_user(){
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			if(isset($_SESSION['current_user_id']) AND !empty($_SESSION['current_user_id'])){
				$db_method=new db_method();
				$user_like_number=$db_method->db_select('user_like',array('user_id'=>$_SESSION['current_user_id'],'user_liked_id'=>$_SESSION['id_user']),'user_like_number');
				$user_rank=$db_method->db_select('user_rank',array('user_id'=>$_SESSION['id_user']),'user_rank');
				$user_power=$db_method->db_select('levels',array('levels'=>$user_rank),'levels_power');
				if($user_like_number!==false and count($user_like_number)==0){
					$user_like_number=$user_power*($_POST['datas']/10);
					$status=$db_method->db_insert('user_like',array('user_id'=>$_SESSION['current_user_id'],'user_liked_id'=>$_SESSION['id_user'],'user_liked_power'=>$_POST['datas'],'user_like_number'=>$user_like_number));
					if($status===false)return $db_method->db_error();
					$db_method->db_user_rank($_SESSION['current_user_id'],$user_like_number);
					$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],7,$user_like_number);
					if($status===false)return $db_method->db_error();
				}
				if($user_like_number!==false and count($user_like_number)>0){
					$user_like_number=$user_like_number*-1;
					$db_method->db_user_rank($_SESSION['current_user_id'],$user_like_number);
					$user_like_number=$user_power*($_POST['datas']/10);
					$status=$db_method->db_update('user_like',array('user_like_number'=>$user_like_number,'user_liked_power'=>$_POST['datas']),"user_id='".$_SESSION['current_user_id']."' AND user_liked_id='".$_SESSION['id_user']."'");
					if($status===false)return $db_method->db_error();
					$db_method->db_user_rank($_SESSION['current_user_id'],$user_like_number);
					$status=$db_method->db_messadge($_SESSION['current_user_id'],$_SESSION['id_user'],8,$user_like_number);
					if($status===false)return $db_method->db_error();					
				}
				if($user_like_number===false) return $db_method->db_error();
			}
		}
	}
//выбор новости по take in
	protected function take_in(){
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			$db_method=new db_method();
			$status=$db_method->db_insert('user_takein',array('user_id'=>$_SESSION['id_user'],'news_id'=>$_POST['param1']));
			if($status===false)return $db_method->db_error();
		}
	}
//удаление книг, закрытая функция
	protected function delete_book(){
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			$db_method=new db_method();
			$file=new file();
			$status=$db_method->db_select('user_book',array('user_book_id'=>$_POST['datas']),'user_book_link,user_book_photo');
			if($status===false) return $db_method->db_error();
			else{
				$path=BASE_PATH."userphoto/".$_SESSION['id_user']."/books/";
				$result=$file->delete_file($path.$status[0]['user_book_link']);
				if($result===false) return "Удаление файла не возможно";
				$result=$file->delete_file($path.$status[0]['user_book_photo']);
				if($result===false) return "Удаление файла не возможно";
				$result=$db_method->db_delete('user_book',"user_book_id='".$_POST['datas']."'");
				if($result===false) return $db_method->db_error();
			}
		}
	}
//подтверждение или отказ дружбы
	protected function friendly(){
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['id_user']) AND !empty($_SESSION['id_user'])){
			$db_method=new db_method();
			$users=$db_method->db_select('note',array('note_id'=>$_POST['datas']),'user_in_id,user_out_id');
			if($users===false) return $db_method->db_error();
			$result=$db_method->db_update('note',array('note_status'=>1),"note_id='".$_POST['datas']."'");
			if($result===false) return $db_method->db_error();
			$result=$db_method->db_update('friends',array('friends_state'=>$_POST['type']),"user_id='".$users[0]['user_out_id']."' AND friends_user_id='".$users[0]['user_in_id']."'");
			if($result===false) return $db_method->db_error();
		}
	}
//открытие диалога
	protected function open_dialog(){
		if(!isset($_SESSION)) session_start();
		$db_method=new db_method();
		$formed_message=new formed_message();
		$mes=$db_method->db_select('messadge',array('mes_id'=>$_POST['param1']),'mes_dialog,user_out_id');
		if($mes===false) return $db_method->db_error();
		$id=$mes[0]['mes_dialog'];
		$_SESSION['mes_user_id']=$mes[0]['user_out_id'];
		if($id>0){
			return $formed_message->message($id);
		}
		else{
			$result=$db_method->db_update('messadge',array('mes_dialog'=>$_POST['param1']),"mes_id='".$_POST['param1']."'");
			if($result===false) return $db_method->db_error();
			$result=$db_method->db_update('messadge',array('mes_status'=>1),"mes_dialog='".$_POST['param1']."'");
			if($result===false) return $db_method->db_error();
			return $formed_message->message($_POST['param1']);
		}
	}
//добавление сообщения в диалог
	protected function send_mes_dialog(){
		if(!isset($_SESSION)) session_start();
		if(!empty($_POST['param3'])){
			$db_method=new db_method();
			$formed_message=new formed_message();
			$data['user_in_id']=$_SESSION['mes_user_id'];
			$data['user_out_id']=$_SESSION['id_user'];
			$data['mes_dialog']=$_POST['param1'];
			$data['mes_status']=0;
			$data['mes_text']=$_POST['param3'];
			$result=$db_method->db_insert('messadge',$data);
			if($result===false) return $db_method->db_error();
			return $formed_message->message($_POST['param1'],date("Y-m-d H:i:s",$_POST['param2']));
		}
	}
}
?>
