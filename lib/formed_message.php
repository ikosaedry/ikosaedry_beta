<?php
class formed_message extends lib
{
	protected $db_method;
	protected $strings;
	protected $user_name;
	protected $user_out_id;
	protected $user_photo;
	protected $my_photo;
	protected $mes_text;
	protected $mes_status;
	protected $mes_dialog;

	
	function __construct(){
		if(!isset($_SESSION)) session_start();
		$this->db_method=new db_method();
		$this->strings=new strings();
	}
	
	public function message($dialog,$time=null){
/*
$dialog - айди диалога
*/
		$where['mes_dialog']=$dialog;
		if(!empty($time))$where="mes_dialog='$dialog' AND mes_time>'$time'";
		$message=$this->db_method->db_select('messadge',$where,null,'mes_time DESC');
		if($message!==false){
			$counter=count($message);
			for($i=0;$i<$counter;$i++){
				$this->data_message($message[$i]);
				@$mes_stek.=$this->creat_message_body();
			}
			if(empty($time)){
				$data="<div class='span6' style='margin-top: 10px; margin-bottom: 10px'>
						<div class='span5' style='margin: 0px'>$this->user_name
							<a href='#' style='margin-left: 20px'><img src='../images/Лента.PNG' class='mini_menu'></a>
							<a href='#'><img src='../images/Сфера.PNG' class='mini_menu'></a>
							<a href='#'><img src='../images/Дом.PNG' class='mini_menu' id=\"go_to_room_mini\"></a>
							<a href='#'><img src='../images/Связи.PNG' class='mini_menu' id=\"show_user_connections_mini\"></a>
							<a href='#'><img src='../images/Добавить.PNG' class='mini_menu'></a>
							<a href='#'><img src='../images/Настройки.PNG' class='mini_menu'></a>
						</div>
						<div class='span1' style='margin: 0px;'><a href='/message.html' style='position: relative'><div class='title5' data-title='Закрыть'><img src='../images/Закрыть.png' id='back' style='height: 10px;' class='pull-right'></div></a></div>
					</div>
					<div class='massenges'>$mes_stek</div>
					<div class='send_massage'>
						<div class='span1 my_photo_on_dialog'>
							<img src='$this->my_photo' class='img-thumbnail' alt='Photo'>
						</div>
						<div class='span7 my_massage'>
							<textarea class='message_field' cols='8'></textarea>
						</div>
						<div class='span7 send_massage_btn' data='".$this->strings->microtime_float()."'>
							<button class='bottom_r send_message' data='$this->mes_dialog'>Отправить</button>
						</div>
					</div>";
			}
			else{
				$data['time']=$this->strings->microtime_float();
				$data['mes']=$mes_stek;
			}
		}
		else{
			$data=$this->db_method->db_error();
		}
		if(isset($data))return $data;
	}

	protected function data_message($message)
	{
		foreach($message as $keys=>$value){
			if($keys=='user_out_id'){
				$this->user_out_id=$value;
				if(empty($this->user_name) AND $_SESSION['id_user']!=$value){
					$user=$this->db_method->db_select('user',array('user_id'=>$value));
					if($user!==false){
						foreach($user[0] as $user_keys=>$user_value){
							if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
								$this->user_name.=" $user_value";
							}
							if($user_keys=='user_photo'){
								if(!empty($user_value)){
									$this->user_photo="/userphoto/$value/$user_value";
								}
								else{
									$this->user_photo="/images/no_photo.png";
								}
							}
						}
					}
					else{
						return $this->db_method->db_error();
					}
				}
			}
			if($keys=='user_in_id'){
				if(empty($this->my_photo) AND $_SESSION['id_user']==$value){
					$user=$this->db_method->db_select('user',array('user_id'=>$value),'user_photo');
					if($user!==false){
						if(!empty($user)){
							$this->my_photo="/userphoto/$value/$user";
						}
						else{
							$this->my_photo="/images/no_photo.png";
						}						
					}
					else{
						return $this->db_method->db_error();
					}
				}
			}
			if($keys=='mes_text'){
				$this->mes_text=$value;
			}
			if($keys=='mes_status'){
				$this->mes_status=$value;
			}
			if($keys=='mes_dialog'){
				if(empty($this->mes_dialog))$this->mes_dialog=$value;
			}
		}		
	}

	protected function creat_message_body(){
		if($this->user_out_id==$_SESSION['id_user']){
			$data="<div class='span12 massage_from_someone'>
						<div class='face'>
						</div>
						<div class='massege_text'>
							$this->mes_text
						</div>
					</div>";
		}
		else{
			$data="<div class='span12 massage_from_me'> 
							<div class='face'>
								<img src='$this->user_photo' class='img-thumbnail' alt='Photo'>
							</div>
							<div class='massege_text'>
								$this->mes_text
							</div>
						</div>";
		}
		return $data;
	}
}