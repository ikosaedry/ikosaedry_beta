<?php
class formed_news extends lib
{
	protected $db_method;
	protected $file;
	protected $user_name;
	protected $user_photo;
	protected $news_type;
	protected $news_rank;
	protected $news_id;
	protected $news;
	protected $news_comment;
	protected $number_news;
	protected $number_news_dislike;
	protected $number_comments;
	protected $user_id;
	protected $count_like;
	
	function __construct(){
		$this->db_method=new db_method();
		$this->file=new file();
	}
	
	public function news($id,$time=null){
		$news=$this->db_method->db_news($id,$time);
		if($news!==false){
			$counter=count($news);
			for($i=0;$i<$counter;$i++){
				$this->data_news($news[$i]);
				@$data['news'].=$this->creat_news_body();
			}
		}
		else{
			$data['messadge']=$this->db_method->db_error();
		}
		if(isset($data))return $data;
	}

	protected function data_news($news)
	{
		foreach($news as $keys=>$value){
			if($keys=='user_id'){
				$this->user_id=$value;
				$user=$this->db_method->db_select('user',array($keys=>$value));
				if($user!==false){
					$this->user_name=null;
					foreach($user[0] as $user_keys=>$user_value){
						if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
							$this->user_name.=" $user_value";
						}
						if($user_keys=='user_photo'){
							if(!empty($user_value)){
								$this->user_photo="/userphoto/$this->user_id/$user_value";
							}
							else{
								$this->user_photo="/images/no_photo.png";
							}
						}
					}
				}
				else{
					$data['messadge']=$this->db_method->db_error();
					return $data;
				}
			}
			if($keys=='news_type'){
				$this->news_type=$value;
			}
			if($keys=='news_id'){
				$this->news_id=$value;
				$this->number_news=count($this->db_method->db_select('news_like',array('news_id'=>$value,'news_like_type'=>1),'news_like_id'));
				$this->number_news_dislike=count($this->db_method->db_select('news_like',array('news_id'=>$value,'news_like_type'=>2),'news_like_id'));
				$this->number_comments=count($this->db_method->db_select('news_comment',array('news_id'=>$value)));
			}
			if($keys=='news_body'){
				switch($this->news_type){
					case 1:
						$this->news="/userphoto/$this->user_id/$value";
						break;
					case 2:
						$this->news="/userphoto/$this->user_id/$value";
						break;
					default:
						$this->news=htmlspecialchars($value);
						break;
				}
			}
			if($keys=='news_comment'){
				$this->news_comment=$value;
			}
			if($keys=='news_rank'){
				$this->news_rank=$value;
			}
		}		
	}

	protected function creat_news_body(){
		$data="<div class='transparent_box'>
					<div class='span4 news_block news_with_comment'>
						<div class='span1 mini_user_photo2' id='feed_photo' data='$this->user_id'>					
							<a href='/personal.html'><img src='$this->user_photo' class='img-thumbnail' alt='Photo'><label class='user_name_on_feed2'>$this->user_name</label></a>	
						</div>					
						<div class='span4 pub_news'>
							<div style='float: right'>";
								if($this->user_id==$_SESSION['id_user']){
									$data.="<button class='select_class'>редактировать</button>";
								}
								$data.="<div class='select_class'>доступ";
		switch($this->news_rank){
			case 1:
				$data.="<img src='../images/Сфера.PNG' class='main_nav_photo' alt='Доступно всем' title='Доступно всем'>";
				break;
			case 2:
				$data.="<img src='../images/Доступно только мне.png' class='main_nav_photo' alt='Доступно мне' title='Доступно мне'>";
				break;
			case 3:
				$data.="<img src='../images/Доступно мне и друзьям.png' class='main_nav_photo' alt='Доступно мне, друзьям' title='Доступно мне, друзьям'>";
				break;
			case 4:
				$data.="<img src='../images/Связи.PNG' class='main_nav_photo' alt='Доступно мне, друзьям и подписчикам' title='Доступно мне, друзьям и подписчикам'>";
				break;
		}
								$data.="</div>
							</div>					
						</div>
						<div class='span4 news_container'>";
		if($this->news_type==1){
			$data.="<div class='span2 news_img'>
								<img src='$this->news' class='img-thumbnail news_img2' alt='Photo'>
								<div class='span2 coment_for_img'>Пользователь $this->user_name обновил фото на аватаре</div>
							</div>";
		}
		if($this->news_type==2){
			$data.="<div class='span2 news_img'>
						<img src='$this->news' class='img-thumbnail news_img2' alt='Photo'>
					</div>";
			if(!empty($this->news_comment)){
				$data.="<div class='span2 coment_for_img'>$this->news_comment</div>";
			}
		}
		if($this->news_type==3){
			$data.="<div class='span2 news_img'>$this->news</div>";
		}
		if($this->news_type==4){
			$data.=$this->creat_shared_news_body($this->news);
		}
		$this->count_like=count($this->db_method->db_select('news_like',array('news_id'=>$this->news_id,'user_id'=>$_SESSION['id_user'])));
		$data.="</div>
						<div class='navbar pub_news'>
						<div style='float: left'>
							<ul class='nav news_id l_d_c_sh' data='$this->news_id'>
								<li><a href='#' class='buttons like";
		if($this->count_like>0 OR $_SESSION['id_user']==$this->user_id) $data.=" disabled";
		$data.="'>Like</a></li>
								<li><a href='#' class='buttons plus_like number_likes thumbs_up'>$this->number_news</a></li>
								<div class='people_likes'>
									<a href='#' class='photo_people_likes'><img src='1.jpg' class='img-thumbnail miniMyPhoto' alt='Photo'></a>
								</div>						
								<li><a href='#' class='buttons dislike";
		if($this->count_like>0 OR $_SESSION['id_user']==$this->user_id) $data.=" disabled";
		$data.="'>Dislike</a></li>
								<li><a href='#' class='buttons plus_like number_dislikes'>$this->number_news_dislike</a></li>
								<li><a href='#' class='buttons comment'>Comment</a></li>
								<li><a href='#' class='buttons share number_comment'>$this->number_comments</a></li>
								<li><a href='#' class='buttons share";
		if($_SESSION['id_user']==$this->user_id OR $this->news_type==4) $data.=" disabled";
		$data.="'>Share</a></li>
								<li><a href='#' class='buttons fa_comment'>0</a></li>
								<div class='people_share'>
									<a href='#' class='photo_people_likes'><img src='1.jpg' class='img-thumbnail miniMyPhoto' alt='Photo'></a>
								</div>
							</ul>
						</div>
						<div style='float: right'>	
								<button class='select_class takein";
		$takein=$this->db_method->db_select('user_takein',array('news_id'=>$this->news_id,'user_id'=>$_SESSION['id_user']));
		if($takein!==false and !empty($takein)) $data.=" disabled";
								$data.="' style='margin-top: 2px'>To take in <i class='fa fa-angle-double-right'></i></button>
						</div>							
						</div>
						<div class='span4 news_container comment_box'>
							<textarea class='textarea_news' name='user_comment' id='user_comment' rows='5' cols='4' placeholder='Ваш коментарий'></textarea>
							<div class='span2 pub_news2'>
								<button class='select_class cancelComment'>Отмена</button>
							</div>
							<div class='span2 pub_news2'>
								<button class='select_class saveComment' name='save_comment' data='$this->news_id'>Сохранить</button>
							</div>
						</div>
						<div class='coment_block'>";
		$comment=$this->db_method->db_select('news_comment',array('news_id'=>$this->news_id));
		$data.=$this->news_comment($comment);
		$data.="</div></div></div>";
		return $data;
	}

	protected function creat_shared_news_body($id){
		$news=$this->db_method->db_select('news',array('news_id'=>$id));
		foreach($news[0] as $keys=>$value){
			if($keys=='user_id'){
				$user_id=$value;
				$user=$this->db_method->db_select('user',array($keys=>$value));
				if($user!==false){
					foreach($user[0] as $user_keys=>$user_value){
						if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
							@$user_name.=" $user_value";
						}
						if($user_keys=='user_photo'){
							if(!empty($user_value)){
								$user_photo="/userphoto/$user_id/$user_value";
							}
							else{
								$user_photo="/images/no_photo.png";
							}
						}
					}
				}
			}
			if($keys=='news_type'){
				$news_type=$value;
			}
			if($keys=='news_id'){
				$news_id=$value;
			}
			if($keys=='news_body'){
				switch($news_type){
					case 1:
						$news="/userphoto/$user_id/$value";
						break;
					case 2:
						$news="/userphoto/$user_id/$value";
						break;
					default:
						$news=htmlspecialchars($value);
						break;
				}
			}
			if($keys=='news_comment'){
				$news_comment=$value;
			}
		}
		$data="<div class='span5 transparent_box3'>
					<div class='span4' style='width: 100%; margin: 0px' data='$user_id'>
						<div style='height: 100%'><a href='/personal.html' style='height: 100%'><div class='span1' style='width: 10%; margin: 0px'><img src='$user_photo' class='img-thumbnail' alt='Photo'></div><div class='span1' style='width: 85%; margin: 0px; height: 100%; padding-left: 5px'><label class='user_name_on_feed' style='position: relative; bottom: 0px'>$user_name</label></div></a></div>
					</div>
					<div class='span4 news_block'>
						<div class='span4 news_container2'>";
		if($news_type==1){
			$data.="<div class='span4 news_img4'>
								<img src='$news' class='img-thumbnail news_img3' alt='Photo'>
						<div class='coment_for_img com'>Пользователь $user_name обновил фото на аватаре</div>
					</div>";
		}
		if($news_type==2){
			$data.="<div class='span4 news_img4'>
						<img src='$news' class='img-thumbnail news_img3' alt='Photo'>";
			if(!empty($news_comment)){
				$data.="<div class='coment_for_img com'>$news_comment</div>";
			}
			$data.="</div>";
		}
		if($news_type==3){
			$data.="<div class='com'>$news</div>";
		}
		$data.="</div></div></div>";
		return $data;		
	}
	
	public function news_comment($comment)
	{
		if($comment!==false){
			$counter=count($comment);
			$data=null;
			for($i=0;$i<$counter;$i++){
				foreach($comment[$i] as $key=>$value){
					if($key=='news_com_body') $user_comment=$value;
					if($key=='news_id') $news_id=$value;
					if($key=='user_id'){
						$user_id=$value;
						$user=$this->db_method->db_select('user',array($key=>$value));
						if($user!==false){
							$users_name=null;
							foreach($user[0] as $user_keys=>$user_value){
								if(($user_keys=='user_name' OR $user_keys=='user_surname') AND !empty($user_value)){
									$users_name.=" $user_value";
								}
								if($user_keys=='user_photo'){
									if(!empty($user_value)){
										$image="/userphoto/$value/$user_value";
									}
									else{
										$image="/images/no_photo.png";
									}
								}
							}
						}
					}
				}
				$data.="<div class='span4' style='width: 100% !important; margin: 0px !important; border-bottom: 1px solid rgba(216,217,217,0.8)'>
					<div class='span1' style='margin: 0px !important; width: 7% !important; padding: 5px;'><img src='$image' class='img-thumbnail' alt='Photo'></div>
					<div class='span3' style='margin: 0px !important; width: 88% !important'>
						<div class='span3 news_comment_description' style=' margin: 0px !important;'><div class='description' id='comment1'>
							<a href='/personal.html' class='name_in_comment uid' data='$user_id'>
								<label class='name_in_comment'>$users_name</label>
							</a>$user_comment
						</div>
					</div>
				<div>
					<div class='navbar' style='float: left; height: 33px; width: 100%; margin-bottom: 0px !important'>
						<ul class='nav l_d_c_sh'>
							<li><a href='#' style='padding-left: 0px'> Like </a></li>
							<li><a href='#'> 0 </a></li>							
							<li><a href='#'> Dislike </a></li>
							<li><a href='#'> 0 </a></li>
						</ul>
					</div>
				</div>
				</div></div>";
			}
		}
		if(isset($data)) return $data;
	}	
}