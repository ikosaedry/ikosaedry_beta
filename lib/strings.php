<?php


class strings{
	public function convert_str($text){
		$text=strtr($text,'«','"');
		$text=strtr($text,'»','"');
		$text=strtr($text,'“','"');
		$text=strtr($text,'”','"');
		$text=strtr($text,"`","'");
		$text=addslashes($text) ;
		return $text;
	}

	public function convert_str_to_date($text){
		return date("Y-m-d",strtotime($text));
	}

	public function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
}
?>
