<?php
class db_method extends lib
{
	private $strings;
	private $link;

	function __construct(){
		$this->strings=new strings();
	}

//ошибки базы данных
	public function db_error(){
		return 'Внимание!<br>Произошла ошибка работы с базой данных<br>'.mysql_errno(). ": " .mysql_error();
	}
//функция выполняющая любой запрос, ей переданный
	public function db_all($query){
		$this->my_link();
		$result=mysql_query($query);
		if(isset($result)) return $result;
	}
//выгрузка данных из базы
	public function db_select($table_name,$where=null,$column=null,$order=null,$limit=null){
/*
$table_name - string - имя таблицы для получения данных (например: member)
$columsn - string - перечень полей и условий записанных с соблюдением синтаксиса MySQL (например: DISTINCT first_name)
$where - array - именованный массив условий для получения выборки записанных (например: id=>'10')
или - string - для сложных запросов, условие записывается пользователем с соблюдением синтаксиса
$order - string - параметры сортировки данных записанных с соблюдением синтаксиса MySQL (например: id DESK)
$limit - string - перечень полей и условий записанных с соблюдением синтаксиса MySQL (например: 0,20)
*/
		$this->my_link();
		if(empty($column)){
			$column='*';
		}
		$query="SELECT $column FROM $table_name";
		if(!empty($where)){
			if(is_array($where)){
				$i=0;
				foreach($where as $key=>$value){
					if($key!='pass'){
						$value=$this->strings->convert_str($value);
					}
					if($i==0){
						$query.=" WHERE $key='$value'";
					}
					else{
						$query.=" AND $key='$value'";
					}
					$i++;
				}
			}
			else{
				$query.=" WHERE $where";
			}
		}
		if(!empty($order)){
			$query.=" ORDER BY $order";
		}
		if(!empty($limit)){
			$query.=" LIMIT $limit";
		}
		$result=mysql_query($query);
		$db_content=$this->result($result);
		if(isset($db_content)) return $db_content;
	}
//загрузка данных в базу
	public function db_insert($tables,$datas){
/*
$tables - string - имя таблицы для получения данных (например: member)
$datas - array - именованный массив данных где ключ массива - имя поля а его значение - значение для записи в базу
*/
	$this->my_link();
	$i=0;
	foreach($datas as $key=>$value){
		if($key!='pass'){
			$value=$this->strings->convert_str($value);
		}
		if(strripos($key,'_birthday') or strripos($key,'_date')){
			$value=$this->strings->convert_str_to_date($value);
		}
		if($i==0){
			$columns=$key;
			$values="'$value'";
		}
		else{
			$columns.=",$key";
			$values.=",'$value'";
		}
		$i++;
	}
	return mysql_query("INSERT INTO $tables($columns) VALUES ($values)");
	}

	public function db_update($tables,$datas,$where){
/*
$tables - string - имя таблицы для получения данных (например: member)
$datas - array - именованный массив данных где ключ массива - имя поля а его значение - значение для записи в базу
$where - string - условия для внесения данных, если отсутсвует будет обновлена вся таблица
*/
	$this->my_link();
	$i=0;
	foreach($datas as $key=>$value){
		if($key!='pass'){
			$value=$this->strings->convert_str($value);
		}
		if(strripos($key,'_birthday') or strripos($key,'_date')){
			$value=$this->strings->convert_str_to_date($value);
		}
		if($i==0){
			$values="$key='$value'";
		}
		else{
			$values.=",$key='$value'";
		}
		$i++;
	}
	return mysql_query("UPDATE $tables SET $values WHERE $where");
	}

	public function db_delete($tables,$where){
/*
$tables - string - имя таблицы для получения данных (например: member)
$where - string - условия для внесения данных, если отсутсвует будет обновлена вся таблица
*/
	$this->my_link();
	return mysql_query("DELETE FROM $tables WHERE $where");
	}

	private function my_link(){
		if(empty($this->link)) $this->link=db_mysql_connect();
	}

	public function result($result){
		$db_content=null;
		if($result!==false){
			$rows=mysql_num_rows($result);
			if(!empty($result)){
				for($i=0;$i<$rows;$i++){
					$row=mysql_fetch_array($result, MYSQL_ASSOC);
					if($rows==1){
						$count_row=count($row);
						if($count_row==1){
							foreach ($row as $key=>$value){
								$db_content=$value;
							}
						}
						else{
							foreach ($row as $key=>$value){
								$db_content[$i][$key]=$value;
							}
						}
					}
					else{
						foreach ($row as $key=>$value){
							$db_content[$i][$key]=$value;
						}
					}
				}
			}
		}
		else{
			$db_content=false;
		}
		return $db_content;
	}

	public function db_news($id,$time=null,$count=null){
		$friends=$this->db_select('friends',array('user_id'=>$id,'friends_state'=>'2'),'friends_user_id');
		$writers=$this->db_select('subscription',array('user_id'=>$id),'sub_user_id');
		$sql="SELECT * FROM news WHERE";
		if(!empty($time)){
			$sql.="(news_date>'$time' AND (";
		}
		$sql.=" news_rank='1' or (news_rank='2' AND user_id='$id') OR (news_rank='3' AND (user_id='$id'";
		if(isset($writers) AND !empty($writers)){
			if(is_array($writers)){
				$counter=count($writers);
				for($a=0;$a<$counter;$a++){
					foreach($writers[$a] as $key=>$value){
							$sql.=" OR user_id='$value'";
					}
				}
			}
			else{
				$sql.=" OR user_id='$writers'";
			}
		}
		$sql.=")) OR (news_rank='4' AND (user_id='$id'";
		if(isset($friends) AND !empty($friends)){
			if(is_array($friends)){
				$counter=count($friends);
				for($a=0;$a<$counter;$a++){
					foreach($friends[$a] as $key=>$value){
						$sql.=" OR user_id='$value'";
					}
				}
			}
			else{
				$sql.=" OR user_id='$friends'";
			}
		}
		$sql.="))";
		if(!empty($time)){
			$sql.="))";
		}
		$sql.=" ORDER BY news_date DESC";
		if(empty($count)){
			$sql.=" LIMIT 0,20";
		}
		$result=mysql_query($sql);
		if($count>0){
			$db_content=mysql_num_rows($result);
		}
		else{
			$db_content=$this->result($result);
		}
		return $db_content;
	}

	public function db_user_rank($user_id,$user_rank,$symbol=null){
		$this->my_link();
		if(!empty($symbol) AND $symbol==2){
			$rank=-1*$user_rank;
		}
		else{
			$rank=$user_rank;
		}
		$user_rank_point=$this->db_select('user_rank',array('user_id'=>$user_id),'user_rank_point');
		if($user_rank_point+$rank>=100000){
			$user_rank_point=100000;
			$rank=100;
		}
		else{
			$user_rank_point=$user_rank_point+$rank;
			$rank=$this->db_select('levels',"levels_point<='$user_rank_point'",'levels','levels DESC','1');
		}
		if($rank===false) return $this->db_error();
		$this->db_update('user_rank',array('user_rank_point'=>$user_rank_point,'user_rank'=>$rank),"user_id='$user_id'");
	}
	
	public function db_messadge($uder_id_in,$user_id_out,$type,$messadge=null){
/*
значения type
1-юзер лайкнул новость
2-юзер дизлайкнул новость
3-предложение дружбы
4-удаление из друзей
5-подписка на пользователя
6-удаление подписки
7-добавление оценки
8-изменение оценки
*/
		$user=$this->db_select('user',array('user_id'=>$_SESSION['id_user']),'user_name,user_surname');
		if($user!==false){
			foreach($user[0] as $key=>$value){
				if($key=='user_name') $name=$value;
				else $name.=" $value";
			}
			switch($type){
				case 1:
					$messadge="$name поставил Вам лайк";
					break;
				case 2:
					$messadge="$name поставил Вам дизлайк";
					break;
				case 3:
					$messadge="$name предлагает Вам дружить";
					break;
				case 4:
					$messadge="$name удалил Вас из друзей";
					break;
				case 5:
					$messadge="$name подписался на Ваши новости";
					break;
				case 6:
					$messadge="$name отписался от Ваших новостей";
					break;
				case 7:
					$messadge="$name поставил Вам оценку $messadge";
					break;
				case 8:
					$messadge="$name изменил Вам оценку на $messadge";
					break;
				case 10:
					$messadge=$this->strings->convert_str($messadge);
					break;
			}
			$data['user_in_id']=$uder_id_in;
			$data['user_out_id']=$user_id_out;
			if($type==10){
				if(isset($_POST['mes_id'])){
					$data['mes_dialog']=$_POST['mes_id'];
				}
				$data['mes_text']=$messadge;
				$this->db_insert('messadge',$data);
			}
			else{ 
				$data['note_type']=$type;
				$data['note_text']=$messadge;
				$this->db_insert('note',$data);
			}
		}
	}
}
?>
