<?php
class Route
{
	static function start()
	{
// контроллер и действие по умолчанию
		$controller_name = 'index';
		$action = 'main';
		$routes = explode('/', $_SERVER['REQUEST_URI']);
// получаем имя контроллера
		if (!empty($routes[1])){
			if(strripos($routes[1],'.')){
				$controller=explode('.',$routes[1]);
			}
			else{
				$controller[0]=$routes[1];
			}
			if($controller[0]=='logout' or $controller[0]=='social' or $controller[0]=='cabinet' or $controller[0]=='personal'){
				$controller_name = $controller[0];
				if(!empty($routes[2])){
					if(strripos($routes[2],'?')){
						$trims=explode('?',$routes[2]);
						$routes[2]=$trims[0];
					}
					if(strripos($routes[2],'.')){
						$trims=explode('.',$routes[2]);
						$routes[2]=$trims[0];
					}
					$action = $routes[2];
				}
			}
			else{
				$action = $controller[0];
			}
		}
		$controller_name = 'controller_'.$controller_name;
// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = "controllers/".$controller_file;
		if(file_exists($controller_path)){
			require_once "controllers/".$controller_file;
		}
		else{
			require_once "controllers/controller_404.php";
			$controller_name = "controller_404";
		}
// создаем контроллер
		new Controller();
		$controller = new $controller_name($action);
		$controller->action_main();
	}
}
?>
