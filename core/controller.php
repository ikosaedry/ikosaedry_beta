<?php
require_once BASE_PATH."model/body.php";
require_once BASE_PATH."main/view.php";
require_once BASE_PATH."main/log.php";

class Controller {

	public $view;
	public $log;

    function __construct()
    {
		$this->view=new View();
		$this->log=new log();
    }
}
