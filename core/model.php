<?php
class Model
{
	public $db_method;
	public $dom_obj;
	public $file;
	public $formed_news;
	public $strings;

    public function db_method()
    {
		$this->db_method=new db_method();
    }
    public function dom_obj()
    {
		$this->dom_obj=new dom_obj();
    }
	public function file()
    {
		$this->file=new file();
    }
	public function formed_news()
    {
		$this->formed_news=new formed_news();
    }
	public function strings()
    {
		$this->strings=new strings();
    }
}
?>
