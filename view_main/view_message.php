<!--script src="/js/center_block.js"></script-->
<script src="/js/sh_message.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top navbar_main">
	<div class="container-fluid">
		<div class="navbar main_navbar">

					<ul class="nav navbar-nav main_nav user_ul">
						<li><div class='logo_block'>
							<img src="../images/Лого.gif" style='height:30px'>
						</div></li>					

					</ul>
			<ul class='nav navbar-nav main_nav logo'>
						<li class="user_inf_rejt"><a href="/cabinet.html"><img src="<?if(isset($data['photo'])) echo $data['photo']; else echo "/images/no_photo.png";?>"  class="img-thumbnail miniMyPhoto" style="float: left; margin-right: 10px" alt="Photo">
							<div style="margin-left: 10px; min-width:200px; height:20px; margin-top: -2px;"><?if(isset($data['main_user_name'])) echo $data['main_user_name']; else echo "User Name";?></div>
							<div style="position: relative; margin-left: 10px;">
								 <div style="float: left"><div style="float: left"><div class="title2" data-title="Уровень пользователя"><?if(isset($data['user_rank'])) echo $data['user_rank']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количество набранных балов"><span>, </span><?if(isset($data['user_rank_point'])) echo $data['user_rank_point']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количестов балов для голосования"><span>, </span><?if(isset($data['user_power'])) echo $data['user_power']; else echo "0";?></div></div></div>
							</div>
							</a>
						</li>
						<li class='open_nav'><a href="#">>></a></li>						
			</ul>					

			<ul class="nav navbar-nav main_nav search">
				<li>
					<div class="form-inline" role="form" method="post">
						<div class="form-group">
							<label for="focusedInput"></label>
								  <div style="position: relative; float: left">
									  <div class="target1"></div>
									  <div class="target2"></div>
										<input id="search_input" class="form-control main_search" placeholder="Search" type="text">
									  <div class="target3"></div>
									  <div class="target4"></div>
								  </div>
							<button class="btn-xs search_btn"><img src="../images/лупа.PNG" class="main_nav_photo"></button>
						</div>
					</div>
				</li>
			</ul>

			<ul class="nav navbar-nav main_nav pull-right menu_ul">
				<li class='close_nav'><a href="#"><<</a></li>			
				<li style="position: relative"><a href="/news.html"><div class="title" data-title="Новостная лента"><img src="../images/Лента.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_news'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_news'];?></div>
					</div>
				<?}?>
				</li>
				<li style="position: relative"><a href="#"><div class="title" data-title="Друзья"><img src="../images/Добавить.PNG" class="main_nav_photo"></div></a>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"></div>
							</div>
				</li>
				<li style="position: relative"><a href="/note.html"><div class="title" data-title="Уведомления"><img src="../images/Служебные сообщения.png" class="main_nav_photo"></div></a>
				<?if(isset($data['count_note'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_note'];?></div>
					</div>
				<?}?>
				</li>				
				<li style="position: relative"><a href="/message.html"><div class="title" data-title="Сообщения"><img src="../images/Сообщение.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_mes'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_mes'];?></div>
					</div>
				<?}?>
				</li>
				<?if(isset($data['log'])) {
					echo $data['log'];
				}
				?>
			</ul>
		</div>
	</div>
</nav>
	<div class="reg_body4 container_2">
		<div class="container-fluid messager pull-right">
			<div class="row">
				<div class="messeger_box"> <!--_______________Блок где выводяться все сообщения__________________---->
					<div class='span6' style='margin: 10px 0px;'><a href='/' class='pull-right' style='position: relative'><div class="title5" data-title="Закрыть"><img src='../images/Закрыть.png' style='height: 10px'></div></a></div>
					<?if(isset($data['messadge'])) echo $data['messadge'];
					if(isset($data['element'])) echo $data['element'];?>
				</div>
				<div class="dialog"> <!--_______________Блок диалога__________________---->
				</div><!--_______________Конец блока диалога__________________---->
			</div>
		</div>
	</div>
