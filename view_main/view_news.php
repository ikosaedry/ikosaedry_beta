<script src="/js/sh_news.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top navbar_main">
	<div class="container-fluid">
		<div class="navbar main_navbar">
					<ul class="nav navbar-nav main_nav user_ul">
						<li><div class='logo_block'>
							<img src="../images/Лого.gif" style='height:30px'>
						</div></li>					

					</ul>
			<ul class='nav navbar-nav main_nav logo2'>
						<li class="user_inf_rejt"><a href="/cabinet.html"><img src="<?if(isset($data['photo'])) echo $data['photo']; else echo "/images/no_photo.png";?>" class="img-thumbnail miniMyPhoto" style="float: left; margin-right: 10px" alt="Photo">
							<div style="margin-left: 10px; min-width:200px; height:20px; margin-top: -2px;"><?if(isset($data['main_user_name'])) echo $data['main_user_name']; else echo "User Name";?></div>
							<div style="position: relative; margin-left: 10px;">
								 <div style="float: left"><div style="float: left"><div class="title2" data-title="Уровень пользователя"><?if(isset($data['user_rank'])) echo $data['user_rank']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количество набранных балов"><span>, </span><?if(isset($data['user_rank_point'])) echo $data['user_rank_point']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количестов балов для голосования"><span>, </span><?if(isset($data['user_power'])) echo $data['user_power']; else echo "0";?></div></div></div>
							</div>
							</a>
						</li>
						<li class='open_nav'><a href="#">>></a></li>						
			</ul>

			<ul class="nav navbar-nav main_nav mainSearch">
				<li>
					<form class="form-inline" role="form" method="post">
						<div class="form-group">
							<label for="focusedInput"></label>
								  <div style="position: relative; float: left">
									  <div class="target1"></div>
									  <div class="target2"></div>
									  <input class="form-control" id='mainSearch' placeholder="Search" type="text">
									  <div class="target3"></div>
									  <div class="target4"></div>
								  </div>
							<button class="btn-xs search_btn"><img src="../images/лупа.PNG" class="main_nav_photo"></button>
						</div>
					</form>
				</li>
			</ul>
			<ul class="nav navbar-nav main_nav pull-right menu_ul">
				<li class='close_nav'><a href="#"><<</a></li>			
				<li style="position: relative"><a href="/news.html"><div class="title" data-title="Новостная лента"><img src="../images/Лента.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_news'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_news'];?></div>
					</div>
				<?}?>
				</li>
				<li style="position: relative"><a href="#"><div class="title" data-title="Друзья"><img src="../images/Добавить.PNG" class="main_nav_photo"></div></a>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"></div>
							</div>
				</li>
				<li style="position: relative"><a href="/note.html"><div class="title" data-title="Уведомления"><img src="../images/Служебные сообщения.png" class="main_nav_photo"></div></a>
				<?if(isset($data['count_note'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_note'];?></div>
					</div>
				<?}?>
				</li>				
				<li style="position: relative"><a href="/message.html"><div class="title" data-title="Сообщения"><img src="../images/Сообщение.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_mes'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_mes'];?></div>
					</div>
				<?}?>
				</li>
				<?if(isset($data['log'])) {
					echo $data['log'];
				}
				?>
			</ul>
		</div>
	</div>
</nav>
<div class="reg_body container_2">
	<div class="news_body reg_body2"> <!--reg_body6--->
		<div class="span7 feed"> <!--reg_body2--->
			<div class="feed_block">
				<div class="transparent_box">
					<div style='position: relative'>
						<div class="span1 mini_user_photo2" id='feed_photo' >
							<div style='position: relative'>
								<img src="<?if(isset($data['photo'])) echo $data['photo']; else echo "/images/no_photo.png";?>" class="img-thumbnail" alt="Photo">
							</div>
						</div>
					</div>
					<div class="span4 news_block">
						<ul class="nav nav-tabs pub_news">
							<?if(isset($data['messadge'])) echo $data['messadge'];?>
							<li class="active add_status pull-right" id="status"><a href="#" class="buttons small_li">сообщение</a></li>
							<li class="photo_video pull-right" id="photoVideo"><a href="#" class="buttons small_li">фото</a></li>
							<li class="photo_alboom pull-right" id="photoAlboom"><a href="#" class="buttons small_li">фотоальбом</a></li>
						</ul>
						<div class="addStatus">
							<form action="" method="post">
								<textarea class="textarea_news textarea" name="chat" id=""  cols="4" placeholder="Чем вы хотите поделиться?"></textarea>
								<div class="span2 pub_news">
									<!--div style="float: left">
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-camera-retro"></i></a></div>
										<div class="newsfeed_buttons"><a href="#"><img src="../images/Добавить1.png" class="main_nav_photo"></a></div>
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-microphone"></i></a></div>
									</div-->
									<div style="float: right">
										<button class="select_class" name="news">Опубликовать</button>
										<?if(isset($data['news_level'])) echo $data['news_level']?>
									</div>
								</div>
							</form>
						</div>
						<div class="addPhotoVideo">
							<form action="" method="post" enctype="multipart/form-data">
								<div class="textarea_news2">
									<input type="file" name="user_photo">
								</div>
								<div>
									<textarea class="textarea_news2 textarea" name="news_comment" placeholder="Комментарий к фото"></textarea>
								</div>
								<div class="span2 pub_news">
									<div style="float: left">
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-camera-retro"></i></a></div>
										<div class="newsfeed_buttons"><a href="#"><img src="../images/Добавить1.png" class="main_nav_photo"></a></div>
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-microphone"></i></a></div>
									</div>
									<div style="float: right">
										<button class="select_class" name="photo">Опубликовать</button>
										<?if(isset($data['news_level'])) echo $data['news_level']?>
									</div>
								</div>
							</form>
						</div>
						<div class="addAlboom">
							<form action="" method="post">
								<div class="textarea_news2">
									<button class="btn pull-left">Выберите папку</button>
								</div>
								<div class="span2 pub_news">
									<div style="float: left">
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-camera-retro"></i></a></div>
										<div class="newsfeed_buttons"><a href="#"><img src="../images/Добавить1.png" class="main_nav_photo"></a></div>
										<div class="newsfeed_buttons"><a href="#"><i class="fa fa-microphone"></i></a></div>
									</div>
									<div style="float: right">
										<button class="select_class" name="album">Опубликовать</button>
										<?if(isset($data['news_level'])) echo $data['news_level']?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--div class="transparent_box">
				<img src="../images/Лого.PNG" style='width:100px;'>
				</div-->
			<?if(isset($data['news']) AND !empty($data['news'])) echo $data['news']?>
			</div>
		</div>
	</div>
</div>
