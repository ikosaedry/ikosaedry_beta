<? 	if(!isset($_SESSION)) session_start();
	if(isset($_SESSION['id_user'])){ ?>
<nav class="navbar navbar-inverse navbar-fixed-top navbar_main">
	<div class="container-fluid container_2">
		<div class="navbar main_navbar">
				<ul class="nav navbar-nav main_nav user_ul">
					<li><div class='logo_block'>
								<img src="../images/Лого.gif" style='height:30px;'>
					</div></li>					

				</ul>
				<ul class='nav navbar-nav main_nav logo'>
						<li class="user_inf_rejt"><a href="/cabinet.html"><img src="<?if(isset($data['photo'])) echo $data['photo']; else echo "/images/no_photo.png";?>" class="img-thumbnail miniMyPhoto" style="float: left; margin-right: 10px" alt="Photo">
							<div style="margin-left: 10px; min-width:200px; height:20px; margin-top: -2px;"><?if(isset($data['main_user_name'])) echo $data['main_user_name']; else echo "User Name";?></div>
							<div style="position: relative; margin-left: 10px;">
								 <div style="float: left"><div style="float: left"><div class="title2" data-title="Уровень пользователя"><?if(isset($data['user_rank'])) echo $data['user_rank']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количество набранных балов"><span>, </span><?if(isset($data['user_rank_point'])) echo $data['user_rank_point']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количестов балов для голосования"><span>, </span><?if(isset($data['user_power'])) echo $data['user_power']; else echo "0";?></div></div></div>
							</div>
							</a>
						</li>
						<li class='open_nav'><a href="#">>></a></li>						
				</ul>
			<ul class="nav navbar-nav main_nav search">
				<li style="margin-right:10px;">
					<div style="position: relative; float: left; height: 28px !important;">
									  <div class="target1"></div>
									  <div class="target2"></div>
				<?if(isset($data['prof'])) echo $data['prof']?>
									  <div class="target3"></div>
									  <div class="target4"></div>
					</div>
				</li>
				<li>
					<div class="form-inline" id="search_form" role="form" method="post" action="/">
						<div class="form-group">
							<label for="focusedInput"></label>
							<div style="position: relative; float: left">
									  <div class="target1"></div>
									  <div class="target2"></div>
							<input id="search_input" class="form-control main_search" placeholder="Search for people, jobs and more.." type="text">
									  <div class="target3"></div>
									  <div class="target4"></div>
								  </div>
							<button class="btn-xs search_btn"><img src="../images/лупа.PNG" class="main_nav_photo"></button>
						</div>
					</div>
				</li>
			</ul>
			<ul class="nav navbar-nav main_nav pull-right menu_ul">
				<li class='close_nav'><a href="#"><<</a></li>			
				<li style="position: relative"><a href="/news.html"><div class="title" data-title="Новостная лента"><img src="../images/Лента.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_news'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_news'];?></div>
					</div>
				<?}?>
				</li>
				<li style="position: relative"><a href="#"><div class="title" data-title="Друзья"><img src="../images/Добавить.PNG" class="main_nav_photo"></div></a>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"></div>
							</div>
				</li>
				<li style="position: relative"><a href="/note.html"><div class="title" data-title="Уведомления"><img src="../images/Служебные сообщения.png" class="main_nav_photo"></div></a>
				<?if(isset($data['count_note'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_note'];?></div>
					</div>
				<?}?>
				</li>				
				<li style="position: relative"><a href="/message.html"><div class="title" data-title="Сообщения"><img src="../images/Сообщение.PNG" class="main_nav_photo"></div></a>
				<?if(isset($data['count_mes'])){?>
					<div class="nav_circle">
						<div class="line"></div>
						<div class="circle"><?echo $data['count_mes'];?></div>
					</div>
				<?}?>
				</li>
				<li id="showConnections" style="opacity: 1 !important; position: relative"><a href="#"><div class="title" data-title="Связи"><img src="../images/Связи.PNG" class="main_nav_photo" alt="switch"></div></a></li>
				<?if(isset($data['log'])) echo $data['log'];?>
				</ul>
		</div>
	</div>
</nav>
<!--div class='main_scroll'>
</div-->
	<?}else{?>
<div class='main_page'>
	<div class='main_logo_block'><img src='../images/Лого4.png' alt='logo' class='main_logo'></div>
	<div class='main_words'>
		<div class='pull-left one_part first_part' style='text-align: right'>..everything should be made as simple as possible,</div>
		<div class='pull-right one_part second_part' style='text-align: left'>but not even one bit simpler. Albert Einstein</div> 
	</div>
	<div class='main_log_registration'>
		<div>
			<?php if(!isset($_SESSION['id_user'])) echo $data['log'];?>
		</div>
	</div>
	<div class='main_lang'><!--a href='#'>eng</a> | <a href='#'>рус</a--></div>
</div>	
	<?}?>
