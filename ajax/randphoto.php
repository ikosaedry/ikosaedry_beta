<?php
error_reporting(0);
function getImgName($path) {
    $pictures = array();
    if ($dir = opendir($path."/"))  {
        while (false !== ($file = readdir($dir))) {
            if ($file == "." || $file == ".." || (is_dir($path."/".$file))) continue;
            $pictures[] = $file;
        }
        closedir($dir);
    }
    return $pictures;
}

echo json_encode(getImgName('../temp_photos'));
?>
