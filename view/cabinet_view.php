<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="<?=BASE_ADDRESS?>favicon.ico" />
	<META http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Spaceford</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<!--- css -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/css/style.css" />
	<link rel="stylesheet" href="/css/style_webgl.css" media="all"/>
	<link rel="stylesheet" href="/css/jquery-ui.min.css" media="all"/>
	<link rel="stylesheet" href="/css/jquery-ui.min.css" media="all"/>
	<link rel="stylesheet" href="/js/jquery-ui/jquery-ui.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Jura:400,600&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

	<!--- js -->
	<script src="/js/jquery-1.11.3.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/jquery.form.js"></script>
	<script src="/js/center_block.js"></script>
	<script src="/js/scriptik.js"></script>	<!--Для работы датвпикера и кнопки Добавить!!-->
	<script src="/js/sh_personal.js"></script>

	<!-- THREE JS SCENE -->
	<script src="/js/three.min.js" async></script>
	<script src="/js/sceneloader.js"></script>
	<!-- -->
	<!-- GMAP -->
	<script src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
	<!-- -->

</head>
<body ontouchstart="">
<div id="sceneloading" style="position: absolute;top: 0;left: 0;width:100%;height:100%;"></div>

<div class="container cont container_2">
	<div class="row">
		<div class="span12">
		<nav class="navbar navbar-inverse navbar-fixed-top navbar_main">
			<div class="container-fluid">
				<div class="navbar main_navbar">
					<ul class="nav navbar-nav main_nav user_ul">
						<li><div class='logo_block'>
							<img src="../images/Лого.gif" style='height:30px;'>
						</div></li>					
					</ul>
					<ul class='nav navbar-nav main_nav logo'>
						<li class="user_inf_rejt"><a href="/cabinet.html"><img src="<?if(isset($data['photo'])) echo $data['photo']; else echo "/images/no_photo.png";?>" class="img-thumbnail miniMyPhoto" style="float: left; margin-right: 10px" alt="Photo">
							<div style="min-width:200px; height:20px; margin-left: 10px; margin-top: -2px;"><?if(isset($data['main_user_name'])) echo $data['main_user_name']; else echo "User Name";?></div>
							<div style="position: relative; margin-left: 10px;">
								<!--div class="target1"></div-->
								<!--div class="target2"></div-->
								 <div style="float: left; text-align: center;"><div style="float: left"><div class="title2" data-title="Уровень пользователя"><?if(isset($data['user_rank'])) echo $data['user_rank']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количество набранных балов"><span>, </span><?if(isset($data['user_rank_point'])) echo $data['user_rank_point']; else echo "0";?></div></div><div style="float: left"><div class="title2" data-title="Количестов балов для голосования"><span>, </span><?if(isset($data['user_power'])) echo $data['user_power']; else echo "0";?></div></div></div>
								<!--div class="target3"></div-->
								<!--div class="target4"></div-->
							</div>
							</a>
						</li>
						<li class='open_nav'><a href="#">>></a></li>							
					</ul>
					<ul class="nav navbar-nav main_nav search">
						<li>
							<div class="form-inline" role="form">
								<div class="form-group">
								  <label for="focusedInput"></label>
								  <div style="position: relative; float: left">
									  <div class="target1"></div>
									  <div class="target2"></div>
									  <input id="search_input" class="form-control main_search" placeholder="Search" type="text">
									  <div class="target3"></div>
									  <div class="target4"></div>
								  </div>
								  <button class="btn-xs search_btn"><img src="../images/лупа.PNG" class="main_nav_photo"></button>
								</div>
							</div>
						</li>						
					</ul>
					<ul class="nav navbar-nav main_nav pull-right menu_ul">
						<li class='close_nav'><a href="#"><<</a></li>					
						<li style="position: relative"><a href="/news.html"><div class="title" data-title="Новостная лента"><img src="../images/Лента.PNG" class="main_nav_photo"></div></a>
						<?if(isset($data['count_news'])){?>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"><?echo $data['count_news'];?></div>
							</div>
						<?}?>
						</li>
						<li><a href='/cabinet/room.php' id="gotoroombtn" style="position: relative;"><div class="title" data-title="Комната"><img src="../images/Дом.PNG" class="main_nav_photo"/>
							<img src="../images/лупа.PNG" style="position: absolute; width: 10px; height: 10px; z-index: 8000; top: 15px; left: 15px;"/></div>
						</a></li>
						<li  style="position: relative"><a href="#"><div class="title" data-title="Друзья"><img src="../images/Добавить.PNG" class="main_nav_photo"></div></a>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"></div>
							</div>
						</li>
						<li style="position: relative"><a href="/note.html"><div class="title" data-title="Уведомления"><img src="../images/Служебные сообщения.png" class="main_nav_photo"></div></a>
						<?if(isset($data['count_note'])){?>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"><?echo $data['count_note'];?></div>
							</div>
						<?}?>
						</li>						
						<li style="position: relative"><a href="/message.html"><div class="title" data-title="Сообщения"><img src="../images/Сообщение.PNG" class="main_nav_photo"></div></a>
						<?if(isset($data['count_mes'])){?>
							<div class="nav_circle">
								<div class="line"></div>
								<div class="circle"><?echo $data['count_mes'];?></div>
							</div>
						<?}?>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><div class="title" data-title="Настройки профия"><img src="../images/Настройки.PNG" class="main_nav_photo"></div></a>
							<ul class="dropdown-menu">
										<li><a href='/cabinet/main.php'>Профиль</a></li>
										<li><a href='/cabinet/education.php'>Учеба</a></li>
										<li><a href='/cabinet/work.php'>Работа</a></li>
										<li><a href='/cabinet/progres.php'>Достижения</a></li>
										<li><a href='/cabinet/books.php'>Книги</a></li>
							</ul>
						</li>
						<?if(isset($data['log'])) echo $data['log'];?>
					</ul>
				</div>
			</div>
		</nav>
			<div class="span12">
				<?php require_once $content_view; ?>
				<div id="dialog" style="display: none;"></div>
			</div>
	</div>
</div>
	<div id="map_canvas" style="display: none;">
		<span id="close_map"></span>
	</div>
</body>
