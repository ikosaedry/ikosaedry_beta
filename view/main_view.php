<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=utf-8">	
	<title>Spaceford</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<!--- css -->
	<link href='https://fonts.googleapis.com/css?family=Jura:400,600&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/css/style.css" />
	<link rel="stylesheet" href="/fonts/font.css" />
	<link rel="stylesheet" href="/css/style_webgl.css" media="all"/>
	<link rel="stylesheet" href="/css/jquery-ui.min.css" media="all"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!--- js -->
	<script src="/js/jquery-1.11.3.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/jquery.form.js"></script>
	<script src="/js/center_block.js"></script>
	<script src="/js/feed.js"></script>
	<script src="/js/sh_personal.js"></script>

	<!-- THREE JS SCENE -->
	<script src="/js/three.min.js" async></script>
	<script src="/js/sceneloader.js"></script>
	<!-- -->
	<!-- GMAP -->
	<script src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
	<!-- -->

</head>
<body ontouchstart="">
<div id="sceneloading" style="position: absolute;top: 0;left: 0;width:100%;height:100%;"></div>

	<?php require_once $content_view; ?>

	<div id="cursor"  style="display:none;position:absolute;top:0px;left:0px;width:100%;height:100%;">
		<img id="cursorImage" src="/images/cursor.png" width="20px" style="z-index: 1;margin:auto;display: block;"/>
		<span id="cursorLabes" style="opacity: 0;"></span>
	</div>

	<div class="panel container_2" id="controlPanel" style="display: none !important;">
		<div class="b_ava">
			<img id="controlImage" src="" class="img-thumbnail ava" alt="Cinque Terre">
			<p id="controlName"></p>
		</div>
		<h4 class="panel_head">Управление</h4>
		<ul id="controlBody">

		</ul>
		<div class="p_bottom">Ctrl + C: Показать/скрыть этот диалог</div>
	</div>

	<div id="zoom_control">
		<div class="zoom_bg" id="zoom_bg">
			<div class="point pintup" id="miny"></div>
			<div class="zoom_control_line"></div>
			<div class="point pintdown" id="maxy"></div>
		</div>
		<div id="zoom_point"></div>
	</div>

	<div id="custom_cursor"  style="display: none;">
		<img id="customcursorImage" src="/images/cursor/hover.png" width="100%"/>
		<span id="customcursorLabes" style="opacity: 0;"></span>
	</div>

	<div id="map_canvas" style="display: none;">
		<span id="close_map"></span>
	</div>



